package com.lia.rohadriver;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.text.Spanned;
import android.util.Log;
import android.widget.RemoteViews;

import androidx.core.app.NotificationCompat;
import androidx.core.text.HtmlCompat;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import org.json.JSONException;
import org.json.JSONObject;

public class FirebaseMessageReceiver extends FirebaseMessagingService{

    Session session;

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        super.onMessageReceived(remoteMessage);
        session = new Session(null,this,null);
       /* Log.d("djhvbd1",""+remoteMessage.getNotification().getTitle());
        Log.d("djhvbd4",""+remoteMessage.getNotification().getBody());
        Log.d("djhvbd7",""+remoteMessage.getData().toString());
        Log.d("djhvbd2",""+remoteMessage.getData().get("data"))*/;
        //handle when receive notification app is open state
        super.onMessageReceived(remoteMessage);
        if (remoteMessage.getData().size() > 0) {
            JSONObject jsonObject = new JSONObject();
            jsonObject = new JSONObject(remoteMessage.getData());
            sendNotification(jsonObject);
        }
        else if(remoteMessage.getNotification() != null){
            JSONObject jsonObject = new JSONObject();
            try {
                jsonObject.put("title",remoteMessage.getNotification().getTitle());
                jsonObject.put("body",remoteMessage.getNotification().getBody());
            } catch (JSONException e) {
                e.printStackTrace();
            }
            sendNotification(jsonObject);
        }
    }

    private RemoteViews getCustomDesign(String title, String message){
        RemoteViews remoteViews=new RemoteViews(getApplicationContext().getPackageName(), R.layout.pushnotification_custom);
        remoteViews.setTextViewText(R.id.title,title);
        remoteViews.setTextViewText(R.id.message,message);
        remoteViews.setImageViewResource(R.id.icon,R.mipmap.ic_launcher);
        return remoteViews;
    }

    private void sendNotification(JSONObject jsonObject) {
        Log.d("Notification Json",""+jsonObject);
        if(jsonObject.optString("data").equalsIgnoreCase("home")){
            Intent intent = new Intent(this, SplashActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            intent.putExtra("fromnotification",true);
            intent.putExtra("page",""+jsonObject.optString("data"));
            PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_ONE_SHOT);
            NotificationCompat.Builder builder = new  NotificationCompat.Builder(this, "Notification")
                    .setSmallIcon(R.mipmap.ic_launcher)
                    .setAutoCancel(true)
                    .setDefaults(Notification.DEFAULT_ALL)
                    .setPriority(Notification.PRIORITY_MAX)
                    .setContentTitle(jsonObject.optString("title"))
                    .setContentText(jsonObject.optString("body"))
                    .setContentIntent(pendingIntent);
            NotificationManager manager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
            manager.notify(0, builder.build());
        }
        else {
            Spanned spanned = HtmlCompat.fromHtml(jsonObject.optString("body") , HtmlCompat.FROM_HTML_MODE_LEGACY);
            Intent intent = new Intent(this, SplashActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_ONE_SHOT);
            NotificationCompat.Builder builder = new  NotificationCompat.Builder(this, "Notification")
                    .setSmallIcon(R.mipmap.ic_launcher)
                    .setAutoCancel(true)
                    .setDefaults(Notification.DEFAULT_ALL)
                    .setPriority(Notification.PRIORITY_MAX)
                    //.setContentTitle(jsonObject.optString("title"))
                    //.setContentText(jsonObject.optString("body"))
                    .setContent(getCustomDesign(jsonObject.optString("title"),spanned.toString()))
                    .setContentIntent(pendingIntent);
            NotificationManager manager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
            manager.notify(0, builder.build());
        }
    }

}
