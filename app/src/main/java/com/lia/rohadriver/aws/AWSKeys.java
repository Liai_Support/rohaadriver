package com.lia.rohadriver.aws;

import com.amazonaws.regions.Regions;
import com.lia.rohadriver.StaticInfo;

public class AWSKeys {
    protected static final String COGNITO_POOL_ID = StaticInfo.POOL_ID;
    protected static final Regions MY_REGION = Regions.ME_SOUTH_1; /*Change Region Here*/
    public static final String BUCKET_NAME = "doveruploads/DriverUploads";

}