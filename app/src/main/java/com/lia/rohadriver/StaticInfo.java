package com.lia.rohadriver;

import static android.content.ContentValues.TAG;

import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;

import androidx.appcompat.app.AppCompatActivity;

import java.text.DecimalFormat;

public class StaticInfo {

 public static final int PERMISSION_REQUEST_CODE = 200;
 public static final int GPS_REQUEST_CODE = 201;
 public static final String googlemapapikey = "AIzaSyAX7o-0VreaS0Gz9jM22oaCzZfF0KiETBU";

 // public static String baseUrl = "https://rohaa.com/";
 public static String baseUrl = "https://doverapp.me/";
// public static String baseUrl = "http://staging.doverapp.me/";
 public static String assigned_orders = baseUrl+"rohaapi/driver/assignedorders";
 public static String driverlogincopy = baseUrl+"rohaapi/driver/driverlogin";
 public static String resetpassword = baseUrl+"rohaapi/driver/resetpassword";
 public static String forgotpassword = baseUrl+"rohaapi/driver/forgotpassword";
 public static String viewprofile = baseUrl+"rohaapi/driver/viewprofile";
 public static String updateprofile = baseUrl+"rohaapi/driver/updateprofile";
 public static String orderstatusupdate = baseUrl+"rohaapi/driver/orderstatusupdate";
 public static String completedorders = baseUrl+"rohaapi/driver/completedorders";
 public static String uploadimage = baseUrl+"rohaapi/driver_ver2/uploadimage";
 public static String tokenupdateUrl = baseUrl+"rohaapi/driver/updatedrivertoken";

 public static String BUCKET_NAME = "doveruploads";
 public static String ENDPOINT = "https://a3naab.s3.amazonaws.com";
 public static String BASE_S3_URL = "https://doveruploads.s3.me-south-1.amazonaws.com/";
 public static String POOL_ID = "me-south-1:60e6134a-a7b7-4584-88c9-4d633e897510";
 public static double milesTokm(double distanceInMiles) {
  distanceInMiles = distanceInMiles*1.60934;
  Log.d(TAG, "milesTokm: static"+distanceInMiles);

  return Double.parseDouble(new DecimalFormat("##.####").format(distanceInMiles));
 }



}

