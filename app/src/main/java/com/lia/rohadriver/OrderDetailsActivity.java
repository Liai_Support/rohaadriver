package com.lia.rohadriver;

import static android.content.ContentValues.TAG;

import androidx.appcompat.app.AppCompatActivity;

import android.content.ComponentName;
import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.net.Uri;
import android.os.Bundle;
import android.telephony.PhoneNumberUtils;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.button.MaterialButton;
import com.google.gson.JsonObject;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;
import java.util.Locale;

public class OrderDetailsActivity extends AppCompatActivity implements View.OnClickListener {

    private TextView orderno,masjid_name,quantity,ordstatus,address,addrkm,cusname,customermobile;
    private ImageView callbtn,wpbtn,locationclick;
    JSONObject obj;
    MaterialButton uploadImage;
    String orderId ="0",latt,lng;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_details);

        String orderData = getIntent().getStringExtra("orderData");
        Log.d(TAG, "onCreate: "+orderData);

        orderno = findViewById(R.id.order_no);
        masjid_name = findViewById(R.id.masjid_name);
        quantity = findViewById(R.id.quantity);
        ordstatus = findViewById(R.id.ordstatus);
        address = findViewById(R.id.address);
        addrkm = findViewById(R.id.addrkm);
        cusname = findViewById(R.id.cusname);
        customermobile = findViewById(R.id.customermobile);
        uploadImage = findViewById(R.id.uploadImage);
        locationclick = findViewById(R.id.locationclick);

        callbtn = findViewById(R.id.callbtn);
        wpbtn = findViewById(R.id.wpbtn);
        callbtn.setOnClickListener(this);
        wpbtn.setOnClickListener(this);
        uploadImage.setOnClickListener(this);
        locationclick.setOnClickListener(this);

        try {
             obj = new JSONObject(orderData);
            if (obj.getString("orderFor") == "water" || obj.getString("orderFor").equals("water")) {
                if (obj.getString("iswish") == "o" || obj.getString("iswish").equals("0")) {

                    orderno.setText("" + getString(R.string.ordernumber) + " : DOV " + obj.getString("order_id")+"-"+obj.getString("Id"));

                    masjid_name.setText("" + obj.getString("yourwish_spec_place"));

                } else {
                    orderno.setText("" + getString(R.string.ordernumber) + " : DOVM " + obj.getString("order_id")+"-"+obj.getString("Id"));
                    masjid_name.setText("" + obj.getString("mosquename"));

                }
                orderId =obj.getString("Id");
                Log.d(TAG, "onCreatexc: "+orderId);
                latt=obj.getString("latitude");
                lng=obj.getString("lng");

                quantity.setText("" + obj.getString("cartcount"));
                ordstatus.setText("" + obj.getString("product_name"));
                ordstatus.setText("" + obj.getString("product_name"));
                address.setText("" + getCompleteAddressString(obj.getDouble("latitude"),obj.getDouble("lng")));
                Log.d(TAG, "onCreate: aaaaaa"+obj.getDouble("distance"));
                addrkm.setText(""+obj.getDouble("distance")+"KM" );
                cusname.setText("" + obj.getString("customer_name"));
                customermobile.setText("" + obj.getString("customer_mno"));
            }else{
                Toast.makeText(this,"home not integrated",Toast.LENGTH_LONG).show();
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }


    }
    private String getCompleteAddressString(double LATITUDE, double LONGITUDE) {
        String strAdd = "";
        Geocoder geocoder = new Geocoder(this, Locale.getDefault());
        try {
            List<Address> addresses = geocoder.getFromLocation(LATITUDE, LONGITUDE, 1);
            if (addresses != null) {
                Address returnedAddress = addresses.get(0);
                StringBuilder strReturnedAddress = new StringBuilder("");

                for (int i = 0; i <= returnedAddress.getMaxAddressLineIndex(); i++) {
                    strReturnedAddress.append(returnedAddress.getAddressLine(i)).append("\n");
                }
                strAdd = strReturnedAddress.toString();
                Log.w("My Current loction address", strReturnedAddress.toString());
            } else {
                Log.w("My Current loction address", "No Address returned!");
            }
        } catch (Exception e) {
            e.printStackTrace();
            Log.w("My Current loction address", "Canont get Address!");
        }
        return strAdd;
    }



    @Override
    public void onClick(View v) {

        switch (v.getId()){
            case R.id.callbtn:

                Intent intent = new Intent(Intent.ACTION_DIAL);
                intent.setData(Uri.parse("tel:+966"+customermobile.getText().toString()));
                startActivity(intent);

                break;
            case R.id.wpbtn:

                try {
//                    number = number.replace(" ", "").replace("+", "");

                    Intent sendIntent = new Intent("android.intent.action.MAIN");
                    sendIntent.setComponent(new ComponentName("com.whatsapp","com.whatsapp.Conversation"));
                    sendIntent.putExtra("jid", "966"+PhoneNumberUtils.stripSeparators(customermobile.getText().toString())+"@s.whatsapp.net");
                    // getApplication().startActivity(sendIntent);

                    startActivity(Intent.createChooser(sendIntent, "Compartir en")
                            .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK));

                } catch(Exception e) {
                    Log.e("WS", "ERROR_OPEN_MESSANGER"+e.toString());
                }

                break;
            case R.id.uploadImage:

                Intent imageintent =new Intent(this,ImageUploadActivity.class);
                imageintent.putExtra("orderId",orderId);
                startActivity(imageintent);

                break;
            case R.id.locationclick:
                Intent mapIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("google.navigation:q="+latt+","+lng+"&mode=c"));
                mapIntent.setPackage("com.google.android.apps.maps");
                startActivity(mapIntent);

                break;
        }



    }
}