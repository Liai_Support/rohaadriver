package com.lia.rohadriver;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.Uri;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.cardview.widget.CardView;
import androidx.core.app.ActivityCompat;
import androidx.recyclerview.widget.RecyclerView;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;
import java.util.Locale;

public class HomeCompletedAdapter extends RecyclerView.Adapter<HomeCompletedAdapter.MyViewHolder> {
    private Context mcontext;
    private List<HomeCompletedDataModel> dataSet;
    private DashboardActivity dashboardActivity;
    private static final int REQUEST_PERMISSIONS = 100;
    private TextView[] pname,pqty;
    public static class MyViewHolder extends RecyclerView.ViewHolder {
        CardView mainview,subcard;
        TextView orderno,map,maphome;
        TextView orderdate,totalcarton,address;
        ImageView imageView1,imageView2,imageView3,imageView4;
        LinearLayout minimize,linear1,linear2,linearimg1,linearimg2,linearimg3,linearimg4,receive;
        TextView suborderno,suborderdate,subtotalcarton,ismecca,mosquename,subtotalcarton2,receivername,receivermno,paytype,amount;
        public TextView completeorder,text;
        LinearLayout linearhome,linearwater,linearpname,linearcqty,linearaddress;

        public MyViewHolder(View itemView) {
            super(itemView);
            this.subcard = (CardView) itemView.findViewById(R.id.sub_card);
            this.mainview = (CardView) itemView.findViewById(R.id.progresscard);
            this.linearhome = (LinearLayout) itemView.findViewById(R.id.linear_home);
            this.linearwater = (LinearLayout) itemView.findViewById(R.id.linear_water);
            this.linearcqty = (LinearLayout) itemView.findViewById(R.id.linear_cartonqty);
            this.linearpname = (LinearLayout) itemView.findViewById(R.id.linear_pname);
            this.linearaddress = (LinearLayout) itemView.findViewById(R.id.linear_address);
            this.address = (TextView) itemView.findViewById(R.id.address);
            this.minimize = (LinearLayout) itemView.findViewById(R.id.minimize);

            this.map = (TextView) itemView.findViewById(R.id.map);
            this.maphome = (TextView) itemView.findViewById(R.id.map_home);
            this.orderno = (TextView) itemView.findViewById(R.id.order_no);
            this.receive =(LinearLayout) itemView.findViewById(R.id.receive);
            this.orderdate = (TextView) itemView.findViewById(R.id.order_date);
            this.totalcarton = (TextView) itemView.findViewById(R.id.total_carton);
            this.suborderno = (TextView) itemView.findViewById(R.id.sub_order_no);
            this.suborderdate = (TextView) itemView.findViewById(R.id.sub_order_date);
            this.subtotalcarton = (TextView) itemView.findViewById(R.id.sub_total_cartons);
            this.ismecca = (TextView) itemView.findViewById(R.id.is_mecca);
            this.mosquename = (TextView) itemView.findViewById(R.id.mosque_name);
            this.subtotalcarton2 = (TextView) itemView.findViewById(R.id.sub_total_cartons2);
            this.receivername = (TextView) itemView.findViewById(R.id.receiver_name);
            this.paytype = (TextView) itemView.findViewById(R.id.paymenttype);
            this.amount = (TextView) itemView.findViewById(R.id.amount);

            this.receivermno = (TextView) itemView.findViewById(R.id.receiver_mno);
            this.linear1 = (LinearLayout) itemView.findViewById(R.id.linear1);
            this.linear2 = (LinearLayout) itemView.findViewById(R.id.linear2);
            linear2.setVisibility(View.GONE);

            linearimg1 = (LinearLayout) itemView.findViewById(R.id.linear_img1);
            linearimg2 = (LinearLayout) itemView.findViewById(R.id.linear_img2);
            linearimg3 = (LinearLayout) itemView.findViewById(R.id.linear_img3);
            linearimg4 = (LinearLayout) itemView.findViewById(R.id.linear_img4);

            this.imageView1 = (ImageView)itemView.findViewById(R.id.imageView1);
            this.imageView2 = (ImageView)itemView.findViewById(R.id.imageView2);
            this.imageView3 = (ImageView)itemView.findViewById(R.id.imageView3);
            this.imageView4 = (ImageView)itemView.findViewById(R.id.imageView4);

            this.completeorder = (TextView) itemView.findViewById(R.id.complete_order);
            this.text = (TextView) itemView.findViewById(R.id.text);
            text.setVisibility(View.GONE);
            linearimg1.setVisibility(View.GONE);
            linearimg2.setVisibility(View.GONE);
            linearimg3.setVisibility(View.GONE);
            linearimg4.setVisibility(View.GONE);
            linearwater.setVisibility(View.GONE);
            linearhome.setVisibility(View.VISIBLE);
            linearaddress.setVisibility(View.VISIBLE);
            completeorder.setVisibility(View.GONE);
            maphome.setVisibility(View.GONE);
            map.setVisibility(View.GONE);
        }
    }

    public HomeCompletedAdapter(Context context, List<HomeCompletedDataModel> data, DashboardActivity dashboardActivity) {
        this.dataSet = data;
        this.mcontext = context;
        this.dashboardActivity = dashboardActivity;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent,
                                           int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.inprogress_card, parent, false);

        MyViewHolder myViewHolder = new MyViewHolder(view);
        return myViewHolder;
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int listPosition) {
        if(dashboardActivity.session.getlang().equals("ar")){
            holder.mosquename.setGravity(View.FOCUS_RIGHT);
            holder.receivername.setGravity(View.FOCUS_RIGHT);
        }
        holder.address.setText(""+dataSet.get(listPosition).getAddress());
        holder.orderno.setText("DOVH"+dataSet.get(listPosition).getOrder_id());
        holder.orderdate.setText(""+dataSet.get(listPosition).getDeliverydate());
        holder.totalcarton.setText(""+dataSet.get(listPosition).getTotal_cartcount()+" "+ dashboardActivity.getString(R.string.pack));
        if(dataSet.get(listPosition).getPayment_type().equalsIgnoreCase("cod")){
            holder.paytype.setText(""+dashboardActivity.getString(R.string.cashondelivery));
        }
        else if(dataSet.get(listPosition).getPayment_type().equalsIgnoreCase("wallet")){
            holder.paytype.setText(""+dashboardActivity.getString(R.string.doverwallet));
        }
        else if(dataSet.get(listPosition).getPayment_type().equalsIgnoreCase("card")){
            holder.paytype.setText(""+dashboardActivity.getString(R.string.onlinecard));
        }
        else if(dataSet.get(listPosition).getPayment_type().equalsIgnoreCase("swipemachine")){
            holder.paytype.setText(""+dashboardActivity.getString(R.string.swipemachine));
        }
        else if(dataSet.get(listPosition).getPayment_type().equalsIgnoreCase("iban")){
            holder.paytype.setText(""+dashboardActivity.getString(R.string.iban));
        }
        else {
            holder.paytype.setText(""+dataSet.get(listPosition).getPayment_type());
        }
        holder.amount.setText(""+dataSet.get(listPosition).getTotalprice()+" SAR");
        holder.receivermno.setText(""+dataSet.get(listPosition).getCustomer_mno());
        holder.receivername.setText(""+dataSet.get(listPosition).getCustomer_name());
        holder.suborderno.setText("DOVH"+dataSet.get(listPosition).getOrder_id());
        holder.suborderdate.setText(""+dataSet.get(listPosition).getOrder_date());
        holder.subtotalcarton.setText(""+dataSet.get(listPosition).getTotal_cartcount()+" "+ dashboardActivity.getString(R.string.pack));
        holder.ismecca.setText(""+dashboardActivity.getString(R.string.homeorder)+"-"+dataSet.get(listPosition).getBranch_name());


        pname = new TextView[dataSet.get(listPosition).getDetails().length()];
        pqty = new TextView[dataSet.get(listPosition).getDetails().length()];
        for (int i=0;i<dataSet.get(listPosition).getDetails().length();i++){
            try {
                JSONObject jsonObject = dataSet.get(listPosition).getDetails().getJSONObject(i);
                holder.mosquename.setText(""+jsonObject.getString("product_name"));
                pname[i] = new TextView(mcontext);
                pname[i].setText(""+""+(i+1)+")"+jsonObject.getString("product_name"));
                pname[i].setBackgroundColor(mcontext.getResources().getColor(R.color.lightyellow1));
                pname[i].setTextColor(Color.BLACK);
                pname[i].setTextAlignment(View.TEXT_ALIGNMENT_VIEW_START);
                pqty[i] = new TextView(mcontext);
                pqty[i].setTextColor(Color.BLACK);
                pqty[i].setBackgroundColor(mcontext.getResources().getColor(R.color.lightyellow1));
                pqty[i].setTypeface(null, Typeface.BOLD);
                pqty[i].setTextAlignment(View.TEXT_ALIGNMENT_VIEW_START);
                pqty[i].setText(""+jsonObject.getString("product_quantity")+" "+ dashboardActivity.getString(R.string.pack));
                LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                params.setMargins(0, 5, 0, 0);
                holder.linearpname.addView(pname[i], params);
                holder.linearcqty.addView(pqty[i],params);
            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
        holder.subtotalcarton2.setText(""+dataSet.get(listPosition).getTotal_cartcount()+" "+ dashboardActivity.getString(R.string.pack));
        holder.subcard.setVisibility(View.GONE);
        holder.linear1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                holder.linear1.setVisibility(View.GONE);
                holder.linear2.setVisibility(View.VISIBLE);
               // holder.mainview.setCardBackgroundColor(mcontext.getResources().getColor(R.color.lightyellow1));
                holder.subcard.setVisibility(View.VISIBLE);
                ActivityCompat.requestPermissions(dashboardActivity,
                        new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE},
                        REQUEST_PERMISSIONS);
            }
        });
        holder.minimize.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                holder.linear2.setVisibility(View.GONE);
                holder.linear1.setVisibility(View.VISIBLE);
               // holder.mainview.setCardBackgroundColor(mcontext.getResources().getColor(R.color.lightyellow1));
                holder.subcard.setVisibility(View.GONE);
                ActivityCompat.requestPermissions(dashboardActivity,
                        new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE},
                        REQUEST_PERMISSIONS);
            }
        });
        holder.maphome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                Double latitude = dataSet.get(listPosition).getLat();
                Double longitude = dataSet.get(listPosition).getLng();
                Log.d("lat long",""+latitude);
                Log.d("lat long",""+longitude);
                String uri = String.format(Locale.ENGLISH, "google.navigation:q="+latitude+","+longitude);
               /* Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(uri));
                homeActivity.startActivity(intent);*/
                Log.d("uri",uri);
                Uri gmmIntentUri = Uri.parse(uri);
                Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
                mapIntent.setPackage("com.google.android.apps.maps");
                dashboardActivity.startActivity(mapIntent);
            }
        });
    }


    @Override
    public int getItemCount() {
        return dataSet.size();
    }
}