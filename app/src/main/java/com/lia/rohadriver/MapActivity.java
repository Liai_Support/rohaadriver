package com.lia.rohadriver;

import android.Manifest;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.location.Location;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;

import com.android.volley.NoConnectionError;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.material.navigation.NavigationView;
import com.google.android.material.snackbar.BaseTransientBottomBar;
import com.google.firebase.messaging.FirebaseMessaging;
import com.google.maps.android.ui.IconGenerator;

import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.directions.route.AbstractRouting;
import com.directions.route.Route;
import com.directions.route.RouteException;
import com.directions.route.Routing;
import com.directions.route.RoutingListener;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.android.gms.tasks.Task;
import com.google.android.material.snackbar.Snackbar;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static android.content.ContentValues.TAG;

public class MapActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener, View.OnClickListener, RoutingListener, GoogleApiClient.OnConnectionFailedListener, OnMapReadyCallback,
        GoogleMap.OnMyLocationClickListener,
        GoogleMap.OnMarkerClickListener,
        GoogleMap.OnMyLocationButtonClickListener {
    boolean doubleBackToExitPressedOnce = false;

    public LatLng latLng;
    private GoogleMap mMap;
    MarkerOptions markerOptions = new MarkerOptions();
    public Session session;

    ArrayList<LatLng> latlnglist = new ArrayList<LatLng>();

    ArrayList<String> list = new ArrayList<String>();
    private FusedLocationProviderClient mFusedLocationProviderClient;
    SupportMapFragment mapFragment;
    String title;

    //current and destination location objects
    protected LatLng start = null;
    protected LatLng end = null;
    TextView titleText;
    //to get location permissions.
    private final static int LOCATION_REQUEST_CODE = 23;
    boolean locationPermission = false;

    //polyline object
    private List<Polyline> polylines = null;
    Location currentLocation;
    private static final int REQUEST_CODE = 101;
    RelativeLayout relativeLayout, mapclick, orderlist;
    public NavigationView navigationView;
    public View headerview;
    public LinearLayout navheader;
    int profilecount = 2;
    TextView update_profile_txt;
    ImageView nav_close;
    TextView editprofile;
    private TextView version;
    public TextView uname, umailid, umobileno;
    View view;
    Bitmap markerBitmap,greenmarkerBitmap;
    // creating array list for adding all our locations.
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_map);
        view = (DrawerLayout) findViewById(R.id.drawer_layout);

        session = new Session(this, this, view);
        relativeLayout = (RelativeLayout) findViewById(R.id.rel);
        mapclick = (RelativeLayout) findViewById(R.id.map_click);
        orderlist = (RelativeLayout) findViewById(R.id.orderlist);
         markerBitmap = BitmapFactory.decodeResource(getResources(), R.drawable.redmosque);
        greenmarkerBitmap = BitmapFactory.decodeResource(getResources(), R.drawable.mosque_icon_green);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        //toolbar.setLogo(R.drawable.ic_burger);
        toolbar.setTitle("");
        toolbar.setSubtitle("");

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
//        toolbar.setNavigationIcon(R.drawable.menu_icon);
//        toolbar.setBackground(getDrawable());




        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        //navigationView.setCheckedItem(R.id.nav_home);
        navigationView.getMenu().findItem(R.id.home_nav).setVisible(false);
        navheader();
        headerview = navigationView.getHeaderView(0);
        navheader = (LinearLayout) headerview.findViewById(R.id.nav_header);

        nav_close = (ImageView) navheader.findViewById(R.id.nav_close);
        nav_close.setOnClickListener(this);
        editprofile = (TextView) navheader.findViewById(R.id.edit_profile_txt);
        editprofile.setOnClickListener(this);
        update_profile_txt = (TextView) navheader.findViewById(R.id.update_profile_txt);
        update_profile_txt.setOnClickListener(this);

        version = (TextView) findViewById(R.id.version);
        version.setOnClickListener(this);
        // version.setText("Version : "+ BuildConfig.VERSION_NAME);
        try {
            version.setText("" + getString(R.string.version) + " " + getPackageManager().getPackageInfo(getPackageName(), 0).versionName);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }

        TextView uname = (TextView) navheader.findViewById(R.id.uname);
        TextView umailid = (TextView) navheader.findViewById(R.id.umailid);
        TextView umobileno = (TextView) navheader.findViewById(R.id.umobileno);
        uname.setText(session.getusername());
        umailid.setText(session.getusermail());
        umobileno.setText(session.getuserphoneno());

        mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        mFusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(this);
        if (!session.gettoken().equals("") && session.isNetworkEnabled() == true) {
            FirebaseMessaging.getInstance().subscribeToTopic("allDevices");
            FirebaseMessaging.getInstance().getToken()
                    .addOnCompleteListener(new OnCompleteListener<String>() {
                        @Override
                        public void onComplete(@NonNull Task<String> task) {
                            if (!task.isSuccessful()) {
                                Log.w(TAG, "Fetching FCM registration token failed", task.getException());
                                return;
                            }
                            // Get new FCM registration token
                            String token = task.getResult();
                            JSONObject requestbody = new JSONObject();
                            try {
                                requestbody.put("driverid", session.getUserId());
                                requestbody.put("token", token);
                                requestbody.put("type", "android");
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            session.progressdialogshow();
                            updatedevicetoken("" + requestbody);
                        }
                    });
        }
        orderlist.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
                Intent intent = new Intent(MapActivity.this, DashboardActivity.class);
                startActivity(intent);
            }
        });

        mapclick.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
                Intent intent = new Intent(MapActivity.this, MapActivity.class);
                startActivity(intent);
            }
        });
    }



    public void navheader() {
        navigationView = (NavigationView) findViewById(R.id.nav_view);
        headerview = navigationView.getHeaderView(0);
        navheader = (LinearLayout) headerview.findViewById(R.id.nav_header);
        //uname = (TextView) navheader.findViewById(R.id.uname);
        //umailid = (TextView) navheader.findViewById(R.id.umailid);
        //umobileno = (TextView) navheader.findViewById(R.id.umobileno);
        JSONObject requestbody = new JSONObject();
        try {
            requestbody.put("id", session.getUserId());
            profileviewrequestJSON(requestbody);
            session.progressdialogshow();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void profileviewrequestJSON(JSONObject response) {
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        final String requestBody = response.toString();
        Log.d("reques34", String.valueOf(response));
        StringRequest stringRequest = new StringRequest(Request.Method.POST, StaticInfo.viewprofile, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                session.progressdialogdismiss();
                Log.d("viewprofileresponse", ">>" + response);
                try {
                    JSONObject obj = new JSONObject(response);
                    if (obj.getBoolean("error") == true) {
                        String msg = obj.getString("message");
                        Log.d("loginerror", "" + msg);
                    } else {
                        uname = (TextView) navheader.findViewById(R.id.uname);
                        umailid = (TextView) navheader.findViewById(R.id.umailid);
                        umobileno = (TextView) navheader.findViewById(R.id.umobileno);
                        String msg = obj.getString("data");
                        JSONObject tok = new JSONObject(msg);
                        session.setusername(tok.getString("name"));
                        session.setusermail(tok.getString("email"));
                        session.setuserphoneno(tok.getString("phone"));
                        session.setDeltotcarton(tok.getInt("total_soldcarton"));
                        session.setRewardedcarton(tok.getInt("settled_carton"));
                        session.setGalaccess(tok.getString("gallery_access"));
                        uname.setText(session.getusername());
                        umailid.setText(session.getusermail());
                        umobileno.setText(session.getuserphoneno());
                        /*Snackbar snackbar;
                        Snackbar.make(mapView, "Profile Updated", Snackbar.LENGTH_SHORT)
                                .setTextColor(ContextCompat.getColor(getApplicationContext(),R.color.design_default_color_error))
                                .setBackgroundTint(ContextCompat.getColor(getApplicationContext(),R.color.white))
                                .setDuration(BaseTransientBottomBar.LENGTH_LONG)
                                .setAnimationMode(BaseTransientBottomBar.ANIMATION_MODE_SLIDE)
                                .show();*/
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("VOLLEY", error.toString());
                session.progressdialogdismiss();
                session.volleyerror(error);
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                int id = session.getUserId();
                Map<String, String> params = new HashMap<String, String>();
                //params.put("Content-Type","application/json" );
                params.put("Authorization", "Bearer " + session.gettoken());
                Log.d("param", "" + params);
                Log.d("id", "" + id);
                return params;
            }

            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }

            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return requestBody == null ? null : requestBody.getBytes("utf-8");
                } catch (UnsupportedEncodingException uee) {
                    VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                    return null;
                }
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(10000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        Log.d("asdsdjkxdk", "" + stringRequest);
        requestQueue.add(stringRequest);
    }

    private void profileupdaterequestJSON(JSONObject response) {
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        final String requestBody = response.toString();
        Log.d("reques234", String.valueOf(response));
        StringRequest stringRequest = new StringRequest(Request.Method.POST, StaticInfo.updateprofile, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d("updateprofileresponse", ">>" + response);
                try {
                    JSONObject obj = new JSONObject(response);
                    if (obj.getBoolean("error") == true) {
                        String msg = obj.getString("message");
                        Log.d("loginerror", "" + msg);
                    } else {
                        navheader();
                        Snackbar snackbar;
                        View view = findViewById(R.id.update_profile_txt);
                        Snackbar.make(view, R.string.profileupdated, Snackbar.LENGTH_SHORT)
                                .setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.design_default_color_error))
                                .setBackgroundTint(ContextCompat.getColor(getApplicationContext(), R.color.white))
                                .setDuration(BaseTransientBottomBar.LENGTH_LONG)
                                .setAnimationMode(BaseTransientBottomBar.ANIMATION_MODE_SLIDE)
                                .show();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("VOLLEY", error.toString());
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                int id = session.getUserId();
                Map<String, String> params = new HashMap<String, String>();
                //params.put("Content-Type","application/json" );
                params.put("Authorization", "Bearer " + session.gettoken());
                Log.d("param", "" + params);
                Log.d("id", "" + id);
                return params;
            }

            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }

            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return requestBody == null ? null : requestBody.getBytes("utf-8");
                } catch (UnsupportedEncodingException uee) {
                    VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                    return null;
                }
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(10000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        Log.d("asdsdjkxdk", "" + stringRequest);
        requestQueue.add(stringRequest);
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        if (!session.isNetworkEnabled() == true) {
            session.noNetworkDialog(this);
        } else {
            int id = item.getItemId();
            //Fragment fragment = null;
            switch (id) {
                case R.id.home_nav:
                    break;
                case R.id.reset:
                    Intent intent1 = new Intent(MapActivity.this, ResetPasswordActivity.class);
                    startActivity(intent1);
                    break;
                case R.id.more:

                    break;
                case R.id.language:
                    Intent n5 = new Intent(MapActivity.this, LanguageActivity.class);
                    startActivity(n5);
                    break;
                case R.id.earnings:
                    Log.d(TAG, "onNavigationItemSelected: clickedearnis");
                    Intent eintent = new Intent(MapActivity.this, EarningsActivity.class);
                    startActivity(eintent);
                    break;
                case R.id.signoff:
                    /*FirebaseAuth.getInstance().signOut();
                     */
                    session.settoken("");
                    session.setusername("");
                    session.setusermail("");
                    session.setUserId(0);
                    session.setuserphoneno("");
                    Intent intent = new Intent(MapActivity.this, LoginActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent);
                    break;
                default:
                    break;
            }
        }
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return false;
    }

    @Override
    public void onClick(View view) {
        if (!session.isNetworkEnabled() == true) {
            session.noNetworkDialog(this);
        } else {
            if (view == editprofile) {
                EditText uname = (EditText) navheader.findViewById(R.id.username);
                EditText umobile = (EditText) navheader.findViewById(R.id.mobile);
                uname.setText(session.getusername());
                umobile.setText(session.getuserphoneno());
                LinearLayout profile = (LinearLayout) navheader.findViewById(R.id.profile);
                LinearLayout profile_update = (LinearLayout) navheader.findViewById(R.id.profile_update);
                if (profilecount % 2 == 0) {
                    profile.setVisibility(View.GONE);
                    profile_update.setVisibility(View.VISIBLE);
                    profilecount++;
                } else {
                    profile.setVisibility(View.VISIBLE);
                    profile_update.setVisibility(View.GONE);
                    profilecount++;
                }
            } else if (view == update_profile_txt) {
                EditText uname = (EditText) navheader.findViewById(R.id.username);
                EditText umobile = (EditText) navheader.findViewById(R.id.mobile);
                String unameStr = String.valueOf(uname.getText().toString());
                String mobileStr = String.valueOf(umobile.getText().toString());
                Log.d("sd4dfg", "" + unameStr);
                if (!unameStr.equals("") && !mobileStr.equals("")) {
                    JSONObject requestbody = new JSONObject();
                    try {
                        requestbody.put("id", session.getUserId());
                        requestbody.put("name", unameStr);
                        requestbody.put("email", session.getusermail());
                        requestbody.put("phone", mobileStr);
                        profileupdaterequestJSON(requestbody);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    LinearLayout profile = (LinearLayout) navheader.findViewById(R.id.profile);
                    LinearLayout profile_update = (LinearLayout) navheader.findViewById(R.id.profile_update);
                    profile_update.setVisibility(View.GONE);
                    profile.setVisibility(View.VISIBLE);
                } else {
                    Snackbar snackbar;
                    Snackbar.make(update_profile_txt, R.string.enterallfields, Snackbar.LENGTH_SHORT)
                            .setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.design_default_color_error))
                            .setBackgroundTint(ContextCompat.getColor(getApplicationContext(), R.color.white))
                            .setDuration(BaseTransientBottomBar.LENGTH_LONG)
                            .setAnimationMode(BaseTransientBottomBar.ANIMATION_MODE_SLIDE)
                            .show();
                }
            } else if (view == nav_close) {
                DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
                drawer.closeDrawer(GravityCompat.START);
            } else if (view == version) {
                session.version();
            }
        }
    }

    private void updatedevicetoken(String response) {
        try {
            RequestQueue requestQueue = Volley.newRequestQueue(this);
            JSONObject jsonBody = new JSONObject(response);
            final String requestcartBody = jsonBody.toString();
            Log.d("requesttoken", "" + requestcartBody);
            StringRequest stringRequest = new StringRequest(Request.Method.POST, StaticInfo.tokenupdateUrl, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    session.progressdialogdismiss();
                    Log.d("requesttokenresponse", "" + response);
                    try {
                        JSONObject obj = new JSONObject(response);
                        JSONObject tok = new JSONObject(obj.getString("data"));
                        if (tok.getString("status").equalsIgnoreCase("inactive")) {
                            Snackbar.make(relativeLayout, getString(R.string.youaredisablefromadminpleasecontactadmin), Snackbar.LENGTH_LONG)
                                    .setTextColor(ContextCompat.getColor(MapActivity.this, R.color.design_default_color_error))
                                    .setBackgroundTint(ContextCompat.getColor(MapActivity.this, R.color.white))
                                    .setDuration(BaseTransientBottomBar.LENGTH_LONG)
                                    .setAnimationMode(BaseTransientBottomBar.ANIMATION_MODE_SLIDE)
                                    .setAction(getString(R.string.ok), new View.OnClickListener() {
                                        @Override
                                        public void onClick(View view) {
                                            session.settoken("");
                                            session.setusername("");
                                            session.setusermail("");
                                            session.setUserId(0);
                                            session.setuserphoneno("");
                                            finish();
                                            Intent intent = new Intent(MapActivity.this, LoginActivity.class);
                                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                            startActivity(intent);
                                        }
                                    })
                                    .show();
                            session.settoken("");
                            session.setusername("");
                            session.setusermail("");
                            session.setUserId(0);
                            session.setuserphoneno("");
                            finish();
                            Intent intent = new Intent(MapActivity.this, LoginActivity.class);
                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            startActivity(intent);
                        } else {

                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    session.progressdialogdismiss();
                    Log.e("VOLLEY", error.toString());
                    if (error instanceof NoConnectionError) {
                        session.slownetwork(MapActivity.this);
                    }
                }
            }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    int id = session.getUserId();
                    Map<String, String> params = new HashMap<String, String>();
                    //params.put("Content-Type","application/json" );
                    params.put("Authorization", "Bearer " + session.gettoken());
                    Log.d("param", "" + params);
                    Log.d("id", "" + id);
                    return params;
                }

                @Override
                public String getBodyContentType() {
                    return "application/json; charset=utf-8";
                }

                @Override
                public byte[] getBody() throws AuthFailureError {
                    try {
                        return requestcartBody == null ? null : requestcartBody.getBytes("utf-8");
                    } catch (UnsupportedEncodingException uee) {
                        VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestcartBody, "utf-8");
                        return null;
                    }
                }
            };
            requestQueue.add(stringRequest);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void assignedorderrequestJSON(JSONObject response) {
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        final String requestBody = response.toString();
        Log.d("request234", String.valueOf(response));
        StringRequest stringRequest = new StringRequest(Request.Method.POST, StaticInfo.assigned_orders, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                session.progressdialogdismiss();
                Log.d("assignedorderresponse", ">>" + response);
                try {
                    JSONObject obj = new JSONObject(response);
                    Log.d("asdfcgvhbnjm", "" + obj);


                    if (obj.getBoolean("error") == false) {
                        JSONObject json5 = obj.getJSONObject("data");
                        JSONArray obj5 = new JSONArray(json5.getString("water"));
                        JSONArray obj6 = new JSONArray(json5.getString("home"));
                        if (obj5.length() == 0 && obj6.length() == 0) {
                            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(MapActivity.this);
                            alertDialogBuilder.setCancelable(false);
                            alertDialogBuilder.setMessage(getString(R.string.pleaseclickyesfordashboard));
                            alertDialogBuilder.setPositiveButton(getString(R.string.yes),
                                    new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface arg0, int arg1) {
                                            arg0.dismiss();
                                            Intent intent = new Intent(MapActivity.this, DashboardActivity.class);
                                            startActivity(intent);
                                        }
                                    });

                            alertDialogBuilder.setNegativeButton(getString(R.string.no), new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.cancel();
                                    /*Intent intent = new Intent(MapActivity.this, HomeActivity.class);
                                    startActivity(intent);*/
                                }
                            });

                            AlertDialog alertDialog = alertDialogBuilder.create();
                            // alertDialog.getWindow().setLayout(600,600);
                            alertDialog.setTitle(getString(R.string.ordersnotavailable));
                            alertDialog.show();
                            Button buttonbackground = alertDialog.getButton(DialogInterface.BUTTON_NEGATIVE);
                            buttonbackground.setTextColor(Color.BLUE);
                            Button buttonbackground1 = alertDialog.getButton(DialogInterface.BUTTON_POSITIVE);
                            buttonbackground1.setTextColor(Color.BLUE);
                        } else {
                            JSONObject json = obj.getJSONObject("data");
                            JSONArray obj1 = new JSONArray(json.getString("water"));
                            JSONArray obj2 = new JSONArray(json.getString("home"));
                            for (int i = 0; i < obj1.length(); i++) {
                                JSONObject jsonObject = obj1.getJSONObject(i);
                                Double d1 = Double.parseDouble(jsonObject.getString("latitude"));
                                Double d2 = Double.parseDouble(jsonObject.getString("lng"));
                                title = String.valueOf(jsonObject);
                                Log.d("titleeee", "" + title);
                                latLng = new LatLng(d1, d2);
                                Log.d("kkkkkkkkk", "" + latLng);
                                latlnglist.add(latLng);
                                list.add(title);
                            }
                            for (int j = 0; j < obj2.length(); j++) {
                                JSONObject jsonObject2 = obj2.getJSONObject(j);
                                Double d1 = Double.parseDouble(jsonObject2.getString("lat"));
                                Double d2 = Double.parseDouble(jsonObject2.getString("lng"));
                                title = String.valueOf(jsonObject2);
                                Log.d("titleeee", "" + title);
                                latLng = new LatLng(d1, d2);
                                Log.d("kkkkkkkkk", "" + latLng);
                                latlnglist.add(latLng);
                                list.add(title);
                            }
                            Log.d("sdhgfcsdh", "" + latlnglist);
                            for (int i = 0; i < latlnglist.size(); i++) {
                                // multiple marker with titles ...
                                IconGenerator iconGenerator = new IconGenerator(MapActivity.this);
                                //iconGenerator.setBackground(MainActivity2.this.getDrawable(R.drawable.black_marker));
                                View inflatedView = View.inflate(MapActivity.this, R.layout.custom_marker, null);
                                titleText = inflatedView.findViewById(R.id.titletext1);
                                // titleText.setText(list.get(i));
                                int k = i + 1;
                                titleText.setText("" + k);
                                iconGenerator.setContentView(inflatedView);
                                MarkerOptions b = markerOptions.position(latlnglist.get(i)).title(list.get(i)).icon(BitmapDescriptorFactory.fromBitmap(greenmarkerBitmap));
                                mMap.addMarker(b);
                                Log.d(TAG, "onResponse: "+b.getTitle());


                                // mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latlnglist.get(0), 17));
                                // Findroutes(start, latlnglist.get(i));
                            }
                            //Findroutes(start, latlnglist.get(0));
                        }
                    } else {

                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                // progressDialog.dismiss();
                Log.e("VOLLEY", error.toString());
                session.progressdialogdismiss();
                session.volleyerror(error);
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                int id = session.getUserId();
                Map<String, String> params = new HashMap<String, String>();
                //params.put("Content-Type","application/json" );
                params.put("Authorization", "Bearer " + session.gettoken());
                Log.d("param", "" + params);
                Log.d("id", "" + id);
                return params;
            }

            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }

            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return requestBody == null ? null : requestBody.getBytes("utf-8");
                } catch (UnsupportedEncodingException uee) {
                    VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                    return null;
                }
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(10000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        Log.d("asdsdjkxdk", "" + stringRequest);
        requestQueue.add(stringRequest);

    }


  /*  private void assignedorderrequestJSON(JSONObject response) {
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        final String requestBody = response.toString();
        Log.d("request234", String.valueOf(response));
        StringRequest stringRequest = new StringRequest(Request.Method.POST, StaticInfo.assigned_orders, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                session.progressdialogdismiss();
                Log.d("assignedorderresponse", ">>" + response);
                try {
                    JSONObject obj = new JSONObject(response);
                    Log.d("asdfcgvhbnjm", "" + obj);


                    if (obj.getBoolean("error") == false) {
                        JSONObject json5 = obj.getJSONObject("data");
                        JSONArray obj5 = new JSONArray(json5.getString("water"));
                        JSONArray obj6 = new JSONArray(json5.getString("home"));
                        if (obj5.length() == 0 && obj6.length() == 0) {
                            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(MapActivity.this);
                            alertDialogBuilder.setCancelable(false);
                            alertDialogBuilder.setMessage(getString(R.string.pleaseclickyesfordashboard));
                            alertDialogBuilder.setPositiveButton(getString(R.string.yes),
                                    new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface arg0, int arg1) {
                                            arg0.dismiss();
                                            Intent intent = new Intent(MapActivity.this, DashboardActivity.class);
                                            startActivity(intent);
                                        }
                                    });

                            alertDialogBuilder.setNegativeButton(getString(R.string.no), new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.cancel();
                                    *//*Intent intent = new Intent(MapActivity.this, HomeActivity.class);
                                    startActivity(intent);*//*
                                }
                            });

                            AlertDialog alertDialog = alertDialogBuilder.create();
                            // alertDialog.getWindow().setLayout(600,600);
                            alertDialog.setTitle(getString(R.string.ordersnotavailable));
                            alertDialog.show();
                            Button buttonbackground = alertDialog.getButton(DialogInterface.BUTTON_NEGATIVE);
                            buttonbackground.setTextColor(Color.BLUE);
                            Button buttonbackground1 = alertDialog.getButton(DialogInterface.BUTTON_POSITIVE);
                            buttonbackground1.setTextColor(Color.BLUE);
                        } else {
                            JSONObject json = obj.getJSONObject("data");
                            JSONArray obj1 = new JSONArray(json.getString("water"));
                            JSONArray obj2 = new JSONArray(json.getString("home"));
                            for (int i = 0; i < obj1.length(); i++) {
                                JSONObject jsonObject = obj1.getJSONObject(i);
                                Double d1 = Double.parseDouble(jsonObject.getString("latitude"));
                                Double d2 = Double.parseDouble(jsonObject.getString("lng"));
                                title = jsonObject.getString("mosquename");
                                Log.d("titleeee", "" + title);
                                latLng = new LatLng(d1, d2);
                                Log.d("kkkkkkkkk", "" + latLng);
                                latlnglist.add(latLng);
                                list.add(title);
                            }
                            for (int j = 0; j < obj2.length(); j++) {
                                JSONObject jsonObject2 = obj2.getJSONObject(j);
                                Double d1 = Double.parseDouble(jsonObject2.getString("lat"));
                                Double d2 = Double.parseDouble(jsonObject2.getString("lng"));
                                title = jsonObject2.getString("address");
                                Log.d("titleeee", "" + title);
                                latLng = new LatLng(d1, d2);
                                Log.d("kkkkkkkkk", "" + latLng);
                                latlnglist.add(latLng);
                                list.add(title);
                            }
                            Log.d("sdhgfcsdh", "" + latlnglist);
                            for (int i = 0; i < latlnglist.size(); i++) {
                                // multiple marker with titles ...
                                IconGenerator iconGenerator = new IconGenerator(MapActivity.this);
                                //iconGenerator.setBackground(MainActivity2.this.getDrawable(R.drawable.black_marker));
                                View inflatedView = View.inflate(MapActivity.this, R.layout.custom_marker, null);
                                titleText = inflatedView.findViewById(R.id.titletext1);
                                // titleText.setText(list.get(i));
                                int k = i + 1;
                                titleText.setText("" + k);
                                iconGenerator.setContentView(inflatedView);
                                MarkerOptions b = markerOptions.position(latlnglist.get(i)).title("Marker").icon(BitmapDescriptorFactory.fromBitmap(greenmarkerBitmap));
                                mMap.addMarker(b);

                                // mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latlnglist.get(0), 17));
                                // Findroutes(start, latlnglist.get(i));
                            }
                            //Findroutes(start, latlnglist.get(0));
                        }
                    } else {

                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                // progressDialog.dismiss();
                Log.e("VOLLEY", error.toString());
                session.progressdialogdismiss();
                session.volleyerror(error);
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                int id = session.getUserId();
                Map<String, String> params = new HashMap<String, String>();
                //params.put("Content-Type","application/json" );
                params.put("Authorization", "Bearer " + session.gettoken());
                Log.d("param", "" + params);
                Log.d("id", "" + id);
                return params;
            }

            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }

            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return requestBody == null ? null : requestBody.getBytes("utf-8");
                } catch (UnsupportedEncodingException uee) {
                    VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                    return null;
                }
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(10000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        Log.d("asdsdjkxdk", "" + stringRequest);
        requestQueue.add(stringRequest);

    }*/


    @Override
    public void onBackPressed() {
        //super.onBackPressed();
        if (doubleBackToExitPressedOnce) {
            Intent intent = new Intent(Intent.ACTION_MAIN);
            intent.addCategory(Intent.CATEGORY_HOME);
            startActivity(intent);
            return;
        }

        doubleBackToExitPressedOnce = true;
        Toast.makeText(this, "Press again back button to exit", Toast.LENGTH_SHORT).show();

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                doubleBackToExitPressedOnce = false;
            }
        }, 2000);


    }

    public void getMarker() {
        Log.d("sdcdx4", "dv");
        JSONObject requestbody1 = new JSONObject();
        try {
            requestbody1.put("id", session.getUserId());
            requestbody1.put("lat", start.latitude);
            requestbody1.put("lng", start.longitude);
            assignedorderrequestJSON(requestbody1);
            session.progressdialogshow();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }





    // function to find Routes.
    public void Findroutes(LatLng Start, LatLng End) {
        if (Start == null || End == null) {
            Toast.makeText(MapActivity.this, getString(R.string.unabletogetthelocation), Toast.LENGTH_LONG).show();
        } else {
            Routing routing = new Routing.Builder()
                    .travelMode(AbstractRouting.TravelMode.WALKING)
                    .withListener(this)
                    .alternativeRoutes(true)
                    .waypoints(Start, End)
                    .key(StaticInfo.googlemapapikey)  //also define your api key here.
                    .build();
            routing.execute();
        }
    }

    //Routing call back functions.
    @Override
    public void onRoutingFailure(RouteException e) {
        View parentLayout = findViewById(android.R.id.content);
        Snackbar snackbar = Snackbar.make(parentLayout, e.toString(), Snackbar.LENGTH_LONG);
        snackbar.show();
//        Findroutes(start,end);
    }

    @Override
    public void onRoutingStart() {
        Toast.makeText(MapActivity.this, "Finding " +
                "Route...", Toast.LENGTH_LONG).show();
    }

    //If Route finding success..
    @Override
    public void onRoutingSuccess(ArrayList<Route> route, int shortestRouteIndex) {

        CameraUpdate center = CameraUpdateFactory.newLatLng(start);
        CameraUpdate zoom = CameraUpdateFactory.zoomTo(16);

        PolylineOptions polyOptions = new PolylineOptions();
        LatLng polylineStartLatLng = null;
        LatLng polylineEndLatLng = null;
        polylines = new ArrayList<>();


        if (polylines != null) {
            polylines.clear();
            // polylines.remove(route);
            // polylines.remove(shortestRouteIndex);
            // polylines.remove(ma);
            Log.d("polyline", "sdfghj");
        }
        //  polylines.clear();

        //add route(s) to the map using polyline

        //   polylines.add(mMap.addPolyline(new PolylineOptions().add(start,latlnglist.get(0)).color(Color.RED).width(7)));

        for (int i = 0; i < route.size(); i++) {

         /*   if (i == shortestRouteIndex) {
                polyOptions.color(Color.RED);
                polyOptions.width(7);
                polyOptions.addAll(route.get(shortestRouteIndex).getPoints());
                Polyline polyline = mMap.addPolyline(polyOptions);
                polylineStartLatLng = polyline.getPoints().get(0);
                int k = polyline.getPoints().size();
                polylineEndLatLng = polyline.getPoints().get(k - 1);
                polylines.add(polyline);

            }*/
            if (i == shortestRouteIndex) {
                polyOptions.color(Color.RED);
                polyOptions.width(7);
                polyOptions.addAll(route.get(shortestRouteIndex).getPoints());
                Polyline polyline = mMap.addPolyline(polyOptions);
                //  polylineStartLatLng = polyline.getPoints().get(0);
                polylineStartLatLng = start;
                int k = polyline.getPoints().size();
                polylineEndLatLng = polyline.getPoints().get(0);
                polylines.add(polyline);

            } else {

            }

        }
        // polyOptions.add(start,latlnglist.get(0)).color(Color.RED);


        //Add Marker on route starting position
       /* MarkerOptions startMarker = new MarkerOptions();
        startMarker.position(polylineStartLatLng);
        startMarker.title("My Location");
        mMap.addMarker(startMarker);

        //Add Marker on route ending position
        MarkerOptions endMarker = new MarkerOptions();
        endMarker.position(polylineEndLatLng);
        endMarker.title("Destination");
        mMap.addMarker(endMarker);*/
    }

    @Override
    public void onRoutingCancelled() {
        Findroutes(start, end);

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Findroutes(start, end);

    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        Log.d("sdcdx", "dv");
        mMap = googleMap;
        if (ActivityCompat.checkSelfPermission(
                this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
                this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, REQUEST_CODE);
            return;
        }
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        mMap.setMyLocationEnabled(true);
        mMap.setOnMyLocationClickListener(this);
        mMap.setOnMyLocationButtonClickListener(this);
        mMap.getUiSettings().setMyLocationButtonEnabled(false);
        mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        mMap.setOnMarkerClickListener(this);
        fetchLocation();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case REQUEST_CODE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    fetchLocation();
                }
                break;
        }
    }


    private void fetchLocation() {
        if (session.isGpsEnabled() == false || session.isPermissionsEnabled() == false) {
            session.gpsEnableRequest();
            session.permissionsEnableRequest();
        } else {
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                // TODO: Consider calling
                //    ActivityCompat#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for ActivityCompat#requestPermissions for more details.
                return;
            }
            Task<Location> task = mFusedLocationProviderClient.getLastLocation();
            if(task != null){
                task.addOnCompleteListener(new OnCompleteListener<Location>() {
                    @Override
                    public void onComplete(@NonNull Task<Location> task) {
                        if (task!=null && task.isSuccessful()) {
                            Log.d("TAG", "onComplete: found task!");
                            Location location = (Location) task.getResult();
                            if (location != null) {
                                currentLocation = location;
                                // currrentlatLng = new LatLng(21.3891, 39.8579);
                                //start = currrentlatLng;
                                start = new LatLng(location.getLatitude(), location.getLongitude());
                                session.setlat((float) start.latitude);
                                session.setlng((float) start.longitude);
                                Log.d("dcxd", "" + session.getlat() + "df" + session.getlng());
                                MarkerOptions marker = new MarkerOptions().icon(BitmapDescriptorFactory.fromBitmap(markerBitmap)).position(start).infoWindowAnchor(1.95f,0.0f);
                                ;

                                mMap.addMarker(marker);
                                mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(start, 14));
                                getMarker();
                                //MarkerOptions markerOptions = new MarkerOptions().position(currrentlatLng).title("I am here!");
                                //   mMap.animateCamera(CameraUpdateFactory.newLatLng(currrentlatLng));
                                // mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(currrentlatLng, 20));
                                // mMap.addMarker(markerOptions);
                            }
                            else {
                                Log.d("TAG", "onComplete: current task is null");
                            }
                        } else {
                            Log.d("TAG", "onComplete: current task is null");
                            //Toast.makeText(getActivity(), "unable to get current task", Toast.LENGTH_SHORT).show();
                        }
                    }
                });
            }
            else {
                Log.d("TAG", "onComplete: current task is null");
                //Toast.makeText(getActivity(), "unable to get current task", Toast.LENGTH_SHORT).show();
            }
        }
    }

    @Override
    public boolean onMarkerClick(Marker marker) {

        if (marker.getPosition().equals(start)) {

            marker.setTitle(getString(R.string.youarehere));
            marker.setTag(getString(R.string.youarehere));
            marker.setIcon(BitmapDescriptorFactory.fromBitmap(markerBitmap));
        } else {

            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
            alertDialogBuilder.setMessage(getString(R.string.showorderdetails));
            alertDialogBuilder.setPositiveButton(R.string.yes,
                    new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface arg0, int arg1) {
                            Intent intent = new Intent(MapActivity.this, OrderDetailsActivity.class);
                            intent.putExtra("orderData",marker.getTitle());
                            startActivity(intent);
                        }
                    });

            alertDialogBuilder.setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {

                    dialog.cancel();


                }
            });

            AlertDialog alertDialog = alertDialogBuilder.create();
            // alertDialog.getWindow().setLayout(600,600);
            alertDialog.show();

            Button buttonbackground = alertDialog.getButton(DialogInterface.BUTTON_NEGATIVE);
            buttonbackground.setTextColor(Color.BLUE);

            Button buttonbackground1 = alertDialog.getButton(DialogInterface.BUTTON_POSITIVE);
            buttonbackground1.setTextColor(Color.BLUE);

        }

        /*orderdetails.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity2.this, HomeActivity.class);
                startActivity(intent);
            }
        });*/
      /*  alertD.dismiss();
        alertD.setView(promptView);
        alertD.setCancelable(true);
        alertD.show();
        alertD.getWindow().setGravity(Gravity.CENTER);
        alertD.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        WindowManager.LayoutParams layoutParams = new WindowManager.LayoutParams();
        layoutParams.copyFrom(alertD.getWindow().getAttributes());
        layoutParams.width=600;
        layoutParams.height=600;
        // layoutParams.horizontalMargin=20;
        alertD.getWindow().setAttributes(layoutParams);

*/
      /*  Log.d("dfvdf",""+marker.getPosition()+"equal"+""+start);
        if(marker.getPosition().equals(start)){
        *//*    marker.setTitle("Your Location");
            marker.setTag("Your Location");
            marker.setIcon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_CYAN));
     *//*   }
        else {
         //   mMap.clear();
            Findroutes(start,marker.getPosition());
        }*/
        return true;
    }

    @Override
    public void onMyLocationClick(@NonNull Location location) {

    }

    @Override
    public boolean onMyLocationButtonClick() {
        return true;
    }
    private BitmapDescriptor BitmapFromVector(Context context, int vectorResId) {
        // below line is use to generate a drawable.
        Drawable vectorDrawable = ContextCompat.getDrawable(context, vectorResId);

        // below line is use to set bounds to our vector drawable.
        vectorDrawable.setBounds(0, 0, vectorDrawable.getIntrinsicWidth(), vectorDrawable.getIntrinsicHeight());

        // below line is use to create a bitmap for our
        // drawable which we have added.
        Bitmap bitmap = Bitmap.createBitmap(vectorDrawable.getIntrinsicWidth(), vectorDrawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);

        // below line is use to add bitmap in our canvas.
        Canvas canvas = new Canvas(bitmap);

        // below line is use to draw our
        // vector drawable in canvas.
        vectorDrawable.draw(canvas);

        // after generating our bitmap we are returning our bitmap.
        return BitmapDescriptorFactory.fromBitmap(bitmap);
    }


}


