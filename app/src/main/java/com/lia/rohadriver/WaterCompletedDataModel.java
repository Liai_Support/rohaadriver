package com.lia.rohadriver;

import com.android.volley.toolbox.StringRequest;

public class WaterCompletedDataModel {

    String name;
    String version;
    int id,M_oid;
    int image;
    int cartoncount;
    String orderdate,orderfor,cname,cmno,pname,deliverydate;
    int ismecca,iswish;
    String mosquename,receivername,receivermno,receiverdesc,place,paytype,amount,yourwishplace,branchname;
    String imgpath1,imgpath2,imgpath3,imgpath4;
    Double lat,lng;
    String productname;

    public WaterCompletedDataModel(String deliverydate,String brachname,String yourwishplace,String receiverdesc,String pname,String cname,String cmno,String amount, String paytype, String orderfor, int id,int M_oid, Double lat, Double lng, int cartoncount, String orderdate, int ismecca, int iswish, String place, String mosquename, String receivername, String receivermno, String imgpath1, String imgpath2, String imgpath3, String imgpath4,String productname) {
        this.branchname = brachname;
        this.deliverydate = deliverydate;
        this.yourwishplace = yourwishplace;
        this.receiverdesc = receiverdesc;
        this.cname = cname;
        this.cmno = cmno;
        this.pname = pname;
        this.id = id;
        this.M_oid = M_oid;
        this.paytype = paytype;
        this.amount = amount;
        this.orderfor = orderfor;
        this.lat = lat;
        this.lng = lng;
        this.cartoncount = cartoncount;
        this.orderdate = orderdate;
        this.ismecca = ismecca;
        this.iswish = iswish;
        this.place = place;
        this.mosquename = mosquename;
        this.receivername = receivername;
        this.receivermno = receivermno;
        this.imgpath1 = imgpath1;
        this.imgpath2 = imgpath2;
        this.imgpath3 = imgpath3;
        this.imgpath4 = imgpath4;
        this.productname = productname;
    }

    public int getM_oid(){
        return M_oid;
    }

    public String getDeliverydate(){
        return deliverydate;
    }

    public String getCname(){
        return cname;
    }

    public String getCmno(){
        return cmno;
    }

    public String getPname(){
        return pname;
    }

    public String getReceiverdesc(){
        return receiverdesc;
    }

    public String getYourwishplace(){
        return yourwishplace;
    }

    public String getBranchname(){
        return branchname;
    }

    public String getOrderfor(){
        return orderfor;
    }
    public String getPaytype(){
        return paytype;
    }
    public String getAmount(){
        return amount;
    }
    public String getName() {
        return name;
    }

    public String getImgpath1() {
        return imgpath1;
    }

    public String getImgpath2() {
        return imgpath2;
    }
    public String getImgpath3() {
        return imgpath3;
    }
    public String getImgpath4() {
        return imgpath4;
    }

    public String getVersion() {
        return version;
    }

    public int getImage() {
        return image;
    }

    public int getOrderId() {
        return id;
    }

    public Double getLat(){
        return lat;
    }

    public Double getLng(){
        return lng;
    }

    public int getCartoncount(){
        return cartoncount;
    }

    public String getOrderdate(){
        return orderdate;
    }

    public String getIsmecca(){
        if(ismecca == 1){
            return "Mecca";
        }
        else if(ismecca == 2){
            return "Medina";
        }
        else if(ismecca == 9){
            return "Riyadh";
        }
        else {
            return "Jeddah";
        }
    }

    public String getPlace(){
        return place;
    }
    public int getIswish(){
        return iswish;
    }

    public String getMosquename(){
        return mosquename;
    }

    public String getReceivername(){
        return receivername;
    }

    public String getReceivermno(){
        return receivermno;
    }
    public String getProductname(){
        return productname;
    }
}
