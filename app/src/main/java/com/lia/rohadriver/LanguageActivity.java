package com.lia.rohadriver;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Build;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import java.util.Locale;

import static android.Manifest.permission.ACCESS_FINE_LOCATION;
import static android.Manifest.permission.CAMERA;
import static android.Manifest.permission.WRITE_EXTERNAL_STORAGE;

public class LanguageActivity extends AppCompatActivity {

    private Session session;
    String langcode="";
    private Button arabic,english;
    RelativeLayout relativeLayout;
    View view;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_language);
        view = (RelativeLayout) findViewById(R.id.main_lang);
        session = new Session(this,this,view);
        isCheck();
        if (!session.isNetworkEnabled() == true) {
            session.noNetworkDialog(this);
        }
        relativeLayout = (RelativeLayout) findViewById(R.id.main_lang);
        arabic = (Button) findViewById(R.id.arabic);
        english = (Button) findViewById(R.id.english);

        arabic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!session.isNetworkEnabled() == true) {
                    session.noNetworkDialog(LanguageActivity.this);
                }
                else {
                    if(isCheck() == true){
                        session.setAppLocale("ar");
                        Intent intent = new Intent(LanguageActivity.this, LoginActivity.class);
                        startActivity(intent);
                        finish();
                    }
                }
            }
        });

        english.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!session.isNetworkEnabled() == true) {
                    session.noNetworkDialog(LanguageActivity.this);
                }
                else {
                    if(isCheck() == true){
                        session.setAppLocale("en");
                        Intent intent = new Intent(LanguageActivity.this, LoginActivity.class);
                        startActivity(intent);
                        finish();
                    }
                }
            }
        });
    }

    public Boolean isCheck(){
        if(!session.isPermissionsEnabled()){
            session.permissionsEnableRequest();
            return false;
        }
        else if(!session.isGpsEnabled()){
            session.gpsEnableRequest();
            return false;
        }
        return true;
    }

    @Override
    protected void onResume() {
        super.onResume();
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case StaticInfo.PERMISSION_REQUEST_CODE:
                if (grantResults.length > 0) {
                    boolean locationAccepted = grantResults[0] == PackageManager.PERMISSION_GRANTED;
                    boolean cameraAccepted = grantResults[1] == PackageManager.PERMISSION_GRANTED;
                    boolean storageAccepted = grantResults[2] == PackageManager.PERMISSION_GRANTED;
                    if (locationAccepted && storageAccepted && cameraAccepted) {
                        //all permissions granted
                        if(!session.isGpsEnabled()){
                            session.gpsEnableRequest();
                        }
                        /*showMessageOKCancel("You need to allow access to both the permissions",
                                        new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialog, int which) {
                                                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                                                    requestPermissions(new String[]{ACCESS_FINE_LOCATION, WRITE_EXTERNAL_STORAGE,READ_EXTERNAL_STORAGE},
                                                            PERMISSION_REQUEST_CODE);

                                                }
                                            }
                                        });*/
                    }
                    else {
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                            if (shouldShowRequestPermissionRationale(ACCESS_FINE_LOCATION) || shouldShowRequestPermissionRationale(CAMERA) || shouldShowRequestPermissionRationale(WRITE_EXTERNAL_STORAGE)) {
                                session.permissionsEnableRequest();
                                return;
                            }
                            else {
                                session.displayManuallyEnablePermissionsDialog();
                            }
                        }
                    }
                }
                break;
        }
    }

    @Override
    public void onActivityResult(int requestCode, final int resultCode, Intent intent) {
        super.onActivityResult(requestCode, resultCode, intent);

        switch (requestCode) {
            case StaticInfo.GPS_REQUEST_CODE:
                if(resultCode != RESULT_OK){
                    //gps enabled succefully
                }
                break;
        }
    }

}