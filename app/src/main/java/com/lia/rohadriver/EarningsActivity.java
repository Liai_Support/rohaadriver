package com.lia.rohadriver;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

public class EarningsActivity extends AppCompatActivity {

    TextView totdelcarton,rewardedcarton,balcarton;
    Session session;
    View view;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_earnings);
        totdelcarton =(TextView) findViewById(R.id.tcarton);
        rewardedcarton =(TextView) findViewById(R.id.arcarton);
        balcarton =(TextView) findViewById(R.id.balcarton);
        session = new Session(this, this, totdelcarton);


        totdelcarton.setText(""+session.getDeltotalcarton());
        rewardedcarton.setText(""+session.getRewardedcarton());
        balcarton.setText(""+(session.getDeltotalcarton()-session.getRewardedcarton()));


    }
}