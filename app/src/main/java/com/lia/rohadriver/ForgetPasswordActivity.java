package com.lia.rohadriver;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.snackbar.BaseTransientBottomBar;
import com.google.android.material.snackbar.Snackbar;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;

public class ForgetPasswordActivity extends AppCompatActivity implements View.OnClickListener {
    EditText mail;
    private MaterialButton submit;
    ImageView back;
    private Session session;
    View view;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forget_password);
        view = (RelativeLayout) findViewById(R.id.root);

        session = new Session(this,this,view);
        if (!session.isNetworkEnabled() == true){
            session.noNetworkDialog(this);
        }
        mail=(EditText) findViewById(R.id.mail);
        submit=findViewById(R.id.submit);
        submit.setOnClickListener(this);
        back = (ImageView) findViewById(R.id.back);
        back.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        if (!session.isNetworkEnabled() == true){
            session.noNetworkDialog(this);
        }
        else {
            if (view == submit) {
                String mailStr = mail.getText().toString();
                if (mail.getText().toString().trim().length() == 0) {
                    mail.setError(getString(R.string.enterallfields));
                    snackbarToast(getString(R.string.enterallfields));
                } else {
                    JSONObject requestbody = new JSONObject();
                    try {
                        requestbody.put("email", mailStr);
                        phonerequestJSON(requestbody);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            } else if (view == back) {
                Intent intent = new Intent(ForgetPasswordActivity.this, LoginActivity.class);
                startActivity(intent);
            }
        }
    }

    private void phonerequestJSON(JSONObject response) {
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        final String requestBody = response.toString();
        Log.d("request",String.valueOf(response));
        StringRequest stringRequest = new StringRequest(Request.Method.POST, StaticInfo.forgotpassword, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d("passwordforgetresponse", ">>" + response);
                try{
                    JSONObject obj = new JSONObject(response);
                    if(obj.getBoolean("error") == true){
                        String msg = obj.getString("message");
                        snackbarToast(""+msg);
                    }
                    else {
                        String msg = obj.getString("message");
                       // snackbarToast(""+msg);
                        View view = findViewById(R.id.submit);
                        Snackbar.make(view, msg, Snackbar.LENGTH_INDEFINITE)
                                .setTextColor(ContextCompat.getColor(ForgetPasswordActivity.this,R.color.design_default_color_error))
                                .setBackgroundTint(ContextCompat.getColor(ForgetPasswordActivity.this,R.color.white))
                                .setDuration(BaseTransientBottomBar.LENGTH_LONG)
                                .setAnimationMode(BaseTransientBottomBar.ANIMATION_MODE_SLIDE)
                                .setAction(R.string.signin, new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                        Intent intent = new Intent(ForgetPasswordActivity.this, LoginActivity.class);
                                        startActivity(intent);
                                    }
                                })
                                .setActionTextColor(Color.BLACK)
                                .show();
                       /* Intent intent = new Intent(ForgetPasswordActivity.this, LoginActivity.class);
                        startActivity(intent);*/
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("VOLLEY", error.toString());
            }
        }){
            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }

            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return requestBody == null ? null : requestBody.getBytes("utf-8");
                } catch (UnsupportedEncodingException uee) {
                    VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                    return null;
                }
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(10000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        Log.d("asdsdjkxdk",""+stringRequest);
        requestQueue.add(stringRequest);
    }

    private void snackbarToast(String msg){
        Snackbar snackbar ;
        //contextView = findViewById(R.id.contextview);
        View view = findViewById(R.id.submit);
        Snackbar.make(view, msg, Snackbar.LENGTH_SHORT)
                .setTextColor(ContextCompat.getColor(ForgetPasswordActivity.this,R.color.design_default_color_error))
                .setBackgroundTint(ContextCompat.getColor(ForgetPasswordActivity.this,R.color.white))
                .setDuration(BaseTransientBottomBar.LENGTH_LONG)
                .setAnimationMode(BaseTransientBottomBar.ANIMATION_MODE_SLIDE)
                .show();
    }
}