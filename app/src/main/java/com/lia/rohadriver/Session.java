package com.lia.rohadriver;

import static android.Manifest.permission.ACCESS_FINE_LOCATION;
import static android.Manifest.permission.CAMERA;
import static android.Manifest.permission.READ_EXTERNAL_STORAGE;
import static android.Manifest.permission.WRITE_EXTERNAL_STORAGE;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentSender;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.provider.Settings;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.google.android.gms.common.api.ResolvableApiException;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResponse;
import com.google.android.gms.location.SettingsClient;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.snackbar.BaseTransientBottomBar;
import com.google.android.material.snackbar.Snackbar;

import org.jsoup.Jsoup;

import java.io.IOException;
import java.util.Locale;

public class Session {

    private SharedPreferences pref;

    private static final String PREF_NAME = "RohaPref";

    SharedPreferences.Editor editor;
    // Editor for Shared preferences
    // Context
    Context context;
    View view;
    Activity activity;
    ProgressDialog progressDialog;

    // Shared pref mode
    int PRIVATE_MODE = 0;
    private static final int REQUEST_PERMISSIONS = 100;

    private static final String IS_LOGIN = "IsLoggedIn";
    private static final String IS_NETWORK = "IsNetwork";
    // User name (make variable public to access from outside)
    public static final String KEY_NAME = "userId";
    private static final String IS_VERSION = "IsVersion";
    private static final String IS_GPS = "IsGps";
    public LocationManager locationManager;

    String currentVersion,onlinever;


    public Session(Activity activity,Context context,View view) {
        this.context = context;
        this.view = view;
        this.activity = activity;
        pref = context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        editor = pref.edit();    }

    /**
     * Quick check for login
     * **/
    // Get Login State
    public void setIsLogin(boolean isLogin){
        editor.putBoolean(IS_LOGIN, isLogin);
        editor.commit();
    }
    public boolean isNetworkEnabled(){
        boolean connected = false;
        try {
            ConnectivityManager cm = (ConnectivityManager) context.getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo nInfo = cm.getActiveNetworkInfo();
            connected = nInfo != null && nInfo.isAvailable() && nInfo.isConnected();
            return pref.getBoolean(IS_NETWORK,connected);
        }  catch (Exception e) {
            Log.e("Connectivity Exception", e.getMessage());
        }
        return pref.getBoolean(IS_NETWORK,connected);
    }

      /*private boolean haveNetworkWithwifi(){
        boolean have_WIFI= false;
        boolean have_MobileData = false;
        ConnectivityManager connectivityManager = (ConnectivityManager)getSystemService(CONNECTIVITY_SERVICE);
        NetworkInfo[] networkInfos = connectivityManager.getAllNetworkInfo();
        for(NetworkInfo info:networkInfos){
            if (info.getTypeName().equalsIgnoreCase("WIFI"))if (info.isConnected())have_WIFI=true;
            if (info.getTypeName().equalsIgnoreCase("MOBILE DATA"))if (info.isConnected())have_MobileData=true;
        }
        return have_WIFI||have_MobileData;
    }*/

    public boolean isGpsEnabled(){
        locationManager = (LocationManager) context.getApplicationContext().getSystemService(Context.LOCATION_SERVICE);
        if (!locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            return false;
        }
        else {
            return true;
        }
    }

    public void gpsEnableRequest() {
        LocationRequest locationRequest = LocationRequest.create();
        locationRequest.setInterval(10000);
        locationRequest.setFastestInterval(5000);
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder().addLocationRequest(locationRequest);
        SettingsClient settingsClient = LocationServices.getSettingsClient(activity);
        Task<LocationSettingsResponse> task = settingsClient.checkLocationSettings(builder.build());
        task.addOnSuccessListener(activity, new OnSuccessListener<LocationSettingsResponse>() {
            @Override
            public void onSuccess(LocationSettingsResponse locationSettingsResponse) {
                // Add polylines to the map.
                // Polylines are useful to show a route or some other connection between points.
                // Position the map's camera near Alice Springs in the center of Australia,
                // and set the zoom factor so most of Australia shows on the screen.
                setIsGps(true);
            }
        });
        task.addOnFailureListener(activity, new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                if (e instanceof ResolvableApiException) {
                    ResolvableApiException resolvable = (ResolvableApiException) e;
                    try {
                        setIsGps(false);
                       /* if(isgps1() == false){
                            nogps1(activity);
                        }*/
                        resolvable.startResolutionForResult(activity, StaticInfo.GPS_REQUEST_CODE);
                    } catch (IntentSender.SendIntentException e1) {
                        e1.printStackTrace();
                    }
                }
            }
        });
    }

    public void setIsGps(boolean isGps){
        editor.putBoolean(IS_GPS, isGps);
        editor.commit();
    }

    public boolean getIsGps(){
        boolean gps = pref.getBoolean(IS_GPS,true);
        return gps;
    }

    public void displayManuallyEnableGpsDialog() {
        Intent intent = new Intent(
                Settings.ACTION_LOCATION_SOURCE_SETTINGS);
        context.startActivity(intent);
        android.app.AlertDialog.Builder alertDialog = new android.app.AlertDialog.Builder(
                context);
        alertDialog.setTitle("SETTINGS");
        alertDialog.setMessage("Enable Location Provider! Go to settings menu?");
        alertDialog.setPositiveButton("Settings",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        Intent intent = new Intent(
                                Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                        context.startActivity(intent);
                    }
                });
        alertDialog.setNegativeButton("Cancel",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });
        alertDialog.show();
    }

    public boolean isPermissionsEnabled() {
        int result1 = ContextCompat.checkSelfPermission(context, ACCESS_FINE_LOCATION);
        int result2 = ContextCompat.checkSelfPermission(context, WRITE_EXTERNAL_STORAGE);
        int result3 = ContextCompat.checkSelfPermission(context, CAMERA);
        return result1 == PackageManager.PERMISSION_GRANTED && result2 == PackageManager.PERMISSION_GRANTED && result3 == PackageManager.PERMISSION_GRANTED;
    }

    public void permissionsEnableRequest() {
        ActivityCompat.requestPermissions(activity, new String[]{ACCESS_FINE_LOCATION,CAMERA,WRITE_EXTERNAL_STORAGE,READ_EXTERNAL_STORAGE}, StaticInfo.PERMISSION_REQUEST_CODE);
    }

    public void displayManuallyEnablePermissionsDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setMessage("We need to access Location for performing necessary task. Please permit the permission through "
                + "Settings screen.\n\nSelect App Permissions -> Enable permission(Location,Storage)");
        builder.setCancelable(false);
        builder.setPositiveButton(context.getString(R.string.permitmanually), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                Intent intent = new Intent();
                intent.setAction(android.provider.Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                Uri uri = Uri.fromParts("package", context.getPackageName(), null);
                intent.setData(uri);
                context.startActivity(intent);
            }
        });
        builder.setNegativeButton(context.getString(R.string.cancel), null);
        builder.show();
    }


    public void settoken(String token) {
        pref.edit().putString("token", token).commit();
    }


    public String gettoken() {
        String token = pref.getString("token","");
        return token;
    }

    public void setlat(float lat){
        pref.edit().putFloat("lat",lat).commit();
    }

    public float getlat(){
        float lat = pref.getFloat("lat",0);
        return lat;
    }

    public void setlng(float lng){
        pref.edit().putFloat("lng",lng).commit();
    }

    public float getlng(){
        float lng = pref.getFloat("lng",0);
        return lng;
    }


    public void setusername(String username) {
        pref.edit().putString("username", username).commit();
    }

    public String getusername() {
        String username = pref.getString("username","");
        return username;
    }



    public void setuserphoneno(String userphoneno) {
        pref.edit().putString("userphoneno", userphoneno).commit();
    }

    public String getuserphoneno() {
        String userphoneno = pref.getString("userphoneno","");
        return userphoneno;
    }

    public void setusermail(String usermail) {
        pref.edit().putString("usermail", usermail).commit();
    }

    public String getusermail() {
        String usermail = pref.getString("usermail","");
        return usermail;
    }
    public void setUserId(int userId) {
        pref.edit().putInt("userId", userId).commit();
    }

    public int getUserId() {
        int userId = pref.getInt("userId",0);
        return userId;
    }

    public void setDeltotcarton(int totdelCarton) {
        pref.edit().putInt("totdelcarton", totdelCarton).commit();
    }

    public int getDeltotalcarton() {
        int totdelCarton = pref.getInt("totdelcarton",0);
        return totdelCarton;
    }

    public void setRewardedcarton(int rewardedcarton) {
        pref.edit().putInt("rewardedcarton", rewardedcarton).commit();
    }

    public int getRewardedcarton() {
        int rewardedcarton = pref.getInt("rewardedcarton",0);
        return rewardedcarton;
    }

    public void setGalaccess(String gaccess) {
        pref.edit().putString("gaccess", gaccess).commit();
    }

    public String getGalaccess() {
        String gaccess = pref.getString("gaccess","I");
        return gaccess;
    }

    public void setlang(String Lang) {
        pref.edit().putString("Lang", Lang).commit();
    }

    public String getlang() {
        String Lang = pref.getString("Lang","en");
        return Lang;
    }

    public void noNetworkDialog(final Activity activity){
        AlertDialog.Builder alertDialog3 = new AlertDialog.Builder(context);
       // LayoutInflater inflater = getLayoutInflater();
        LayoutInflater inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View dialoglayout = inflater.inflate(R.layout.no_network, null);
        dialoglayout.setMinimumHeight(100);
        dialoglayout.setMinimumWidth(100);
        dialoglayout.setBackgroundDrawable(null);
        alertDialog3.setView(R.layout.no_network);
        final AlertDialog alertDialog = alertDialog3.create();
        alertDialog.show();
        alertDialog.setCancelable(true);
        alertDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                // dialog dismiss without button press
               /* if (isNetwork() == true){
                    alertDialog.dismiss();
                }
                else {
                    alertDialog.show();
                }*/
            }
        });
        WindowManager.LayoutParams layoutParams = new WindowManager.LayoutParams();
        layoutParams.copyFrom(alertDialog.getWindow().getAttributes());
        layoutParams.width = 500;
        layoutParams.height = 500;
        alertDialog.getWindow().setBackgroundDrawable(null);
        alertDialog.getWindow().setAttributes(layoutParams);
        ImageView btnClose = (ImageView)alertDialog.findViewById(R.id.btnIvClose);
        btnClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                if (isNetworkEnabled() == true){
                    alertDialog.dismiss();
                    Intent intent = new Intent(context, activity.getClass());
                    activity.startActivity(intent);
                }
                else {
                    alertDialog.show();
                }
            }
        });
    }

    public void slownetwork(final Activity activity){
        AlertDialog.Builder alertDialog3 = new AlertDialog.Builder(context);
        // LayoutInflater inflater = getLayoutInflater();
        LayoutInflater inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View dialoglayout = inflater.inflate(R.layout.slow_network, null);
        dialoglayout.setMinimumHeight(100);
        dialoglayout.setMinimumWidth(100);
        dialoglayout.setBackgroundDrawable(null);
        alertDialog3.setView(R.layout.slow_network);
        final AlertDialog alertDialog = alertDialog3.create();
        alertDialog.show();
        alertDialog.setCancelable(true);
        alertDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                // dialog dismiss without button press
               /* if (isNetwork() == true){
                    alertDialog.dismiss();
                }
                else {
                    alertDialog.show();
                }*/
            }
        });
        WindowManager.LayoutParams layoutParams = new WindowManager.LayoutParams();
        layoutParams.copyFrom(alertDialog.getWindow().getAttributes());
        layoutParams.width = 500;
        layoutParams.height = 500;
        alertDialog.getWindow().setBackgroundDrawable(null);
        alertDialog.getWindow().setAttributes(layoutParams);
        ImageView btnClose = (ImageView)alertDialog.findViewById(R.id.btnIvClose);
        btnClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                if (isNetworkEnabled() == true){
                    alertDialog.dismiss();
                    Intent intent = new Intent(context,activity.getClass());
                    context.startActivity(intent);
                }
                else {
                    alertDialog.show();
                }
            }
        });
    }

    public void setIsVersion(boolean isVersion){
        editor.putBoolean(IS_VERSION, isVersion);
        editor.commit();
    }

    public boolean isVersion(){
        return pref.getBoolean(IS_VERSION,false);
    }

    public void version(){
        progressdialogshow();
        try {
            currentVersion = context.getPackageManager().getPackageInfo(context.getPackageName(), 0).versionName;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        new GetVersionCode().execute();
    }

    class GetVersionCode extends AsyncTask<Void, String, String> {

        @Override

        protected String doInBackground(Void... voids) {
            String newVersion = "";
            try {
                // another way
              /*  Document document = Jsoup.connect("https://play.google.com/store/apps/details?id=" + context.getPackageName() + "&hl=en")
                        .timeout(30000)
                        .userAgent("Mozilla/5.0 (Windows; U; WindowsNT 5.1; en-US; rv1.8.1.6) Gecko/20070725 Firefox/2.0.0.6")
                        .referrer("http://www.google.com")
                        .get();
                if (document != null) {
                    Elements element = document.getElementsContainingOwnText("Current Version");
                    for (Element ele : element) {
                        if (ele.siblingElements() != null) {
                            Elements sibElemets = ele.siblingElements();
                            for (Element sibElemet : sibElemets) {
                                return newVersion = sibElemet.text();
                            }
                        }
                    }
                }*/
                return newVersion = Jsoup.connect("https://play.google.com/store/apps/details?id=" +  context.getPackageName() + "&hl=en")
                        .timeout(30000)
                        .userAgent("Mozilla/5.0 (Windows; U; WindowsNT 5.1; en-US; rv1.8.1.6) Gecko/20070725 Firefox/2.0.0.6")
                        .referrer("http://www.google.com")
                        .get()
                        .select("div.hAyfc:nth-child(4) > span:nth-child(2) > div:nth-child(1) > span:nth-child(1)")
                        .first()
                        .ownText();
            } catch (IOException e) {
                progressdialogdismiss();
                e.printStackTrace();
            }
            return newVersion;
        }

        @Override
        protected void onPostExecute(String onlineVersion) {
            super.onPostExecute(onlineVersion);
            progressdialogdismiss();
            if (onlineVersion != null && !onlineVersion.isEmpty()) {
                if (Float.valueOf(currentVersion) < Float.valueOf(onlineVersion)) {
                    setIsVersion(true);
                    versiondialog();
                }
                else {
                    setIsVersion(false);
                }
                Log.d("update", "Current version " + currentVersion + "playstore version " + onlineVersion);
            }
        }
    }


    public void versiondialog(){
        //show anything
        //Uncomment the below code to Set the message and title from the strings.xml file
        AlertDialog.Builder builder = new AlertDialog.Builder(context);

        //Setting message manually and performing action on button click
        builder.setMessage(context.getString(R.string.newversionisavailable))
                .setCancelable(false)
                .setPositiveButton(context.getString(R.string.update), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        //finish();
                        dialog.cancel();
                        Intent myWebLink = new Intent(android.content.Intent.ACTION_VIEW);
                        myWebLink.setData(Uri.parse("https://play.google.com/store/apps/details?id=com.lia.yello.roha"));
                        context.startActivity(myWebLink);
                    }
                })
                .setNegativeButton(context.getString(R.string.cancel), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        //  Action for 'NO' Button
                        dialog.cancel();
                    }
                });
        //Creating dialog box
        AlertDialog alert = builder.create();
        //Setting the title manually
        alert.setTitle("");
        alert.show();
    }

    public void progressdialogshow(){
        if(progressDialog != null){
            if(progressDialog.isShowing()){
                progressDialog.dismiss();
            }
        }

        progressDialog = new ProgressDialog(context,R.style.MyDialogstyle);
        progressDialog.setTitle("");
        progressDialog.setMessage(context.getString(R.string.pleasewait));
        progressDialog.setCancelable(false);

        if(progressDialog.isShowing() == false){
            Log.d("dopencountt","1");
            progressDialog.show();
        }
    }

    public void progressdialogdismiss() {
        if (progressDialog != null) {
            if (progressDialog.isShowing() == true) {
                Log.d("dclosecountt", "1");
                progressDialog.dismiss();
            }
        }
    }

    public void snackbarToast(String msg,View view){
        Snackbar.make(view, msg, Snackbar.LENGTH_SHORT)
                .setTextColor(ContextCompat.getColor(context,R.color.design_default_color_error))
                .setBackgroundTint(ContextCompat.getColor(context,R.color.white))
                .setDuration(BaseTransientBottomBar.LENGTH_LONG)
                .setAnimationMode(BaseTransientBottomBar.ANIMATION_MODE_SLIDE)
                .show();
    }

    public void volleyerror(VolleyError error){
        if (error instanceof TimeoutError || error instanceof NoConnectionError) {
            //This indicates that the reuest has either time out or there is no connection
            snackbarToast(context.getString(R.string.timeouterror),view);
        } else if (error instanceof AuthFailureError) {
            // Error indicating that there was an Authentication Failure while performing the request
            snackbarToast(context.getString(R.string.authfailureerror),view);
        } else if (error instanceof ServerError) {
            //Indicates that the server responded with a error response
            snackbarToast(context.getString(R.string.servererror),view);
        } else if (error instanceof NetworkError) {
            //Indicates that there was network error while performing the request
            snackbarToast(context.getString(R.string.networkerror),view);
        } else if (error instanceof ParseError) {
            // Indicates that the server response could not be parsed
            snackbarToast(context.getString(R.string.parseerror),view);
        }
    }

    public void setAppLocale(String localeCode) {
        Resources resources = activity.getResources();
        DisplayMetrics displayMetrics = resources.getDisplayMetrics();
        Configuration configuration = resources.getConfiguration();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
            configuration.setLocale(new Locale(localeCode.toLowerCase()));
        }
        else {
            configuration.locale = new Locale(localeCode.toLowerCase());
        }
        resources.updateConfiguration(configuration, displayMetrics);
        setlang(localeCode);
    }

}

