package com.lia.rohadriver.ImagePicker1;

import androidx.activity.result.ActivityResult;
import androidx.activity.result.ActivityResultCallback;
import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.FileProvider;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import com.google.android.material.imageview.ShapeableImageView;
import com.lia.rohadriver.R;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class MainActivity extends AppCompatActivity {


    ActivityResultLauncher<Intent> someActivityResultLauncher;
    String currentPhotoPath =  null;
    File currentPhotoFile = null;
    Uri currentPhotoPathFileUri = null;
    Bitmap bitmap;
    ShapeableImageView shapeableImageView;
    public int CAMERA = 2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        shapeableImageView = (ShapeableImageView) findViewById(R.id.imageViewProfile);
        Button button = (Button) findViewById(R.id.buttonCamera2);

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                takePhotoFromCamera();
            }
        });

        someActivityResultLauncher = registerForActivityResult(
                new ActivityResultContracts.StartActivityForResult(),
                new ActivityResultCallback<ActivityResult>() {
                    @Override
                    public void onActivityResult(ActivityResult result) {
                        Log.d("jgvdv",""+result.getResultCode()+"dv"+result.getData());
                        if (result.getResultCode() == Activity.RESULT_OK) {
                            // There are no request codes
                            if(result.getData() != null){
                                // Get Extra from the intent
                                Bundle extras = result.getData().getExtras();
                                // Get the returned image from extra
                                bitmap = (Bitmap) extras.get("data");
                                shapeableImageView.setImageBitmap(bitmap);
                                /*long imagename = System.currentTimeMillis();
                                imagedata = new VolleyMultipartRequest.DataPart(imagename + ".jpg", getFileDataFromDrawable(bitmap));
                                if (imagedata != null) {
                                    session.progressdialogshow();
                                    uploadBitmap(img, bitmap);
                                }*/
                            }
                        }
                    }
                });
    }

    private void takePhotoFromCamera() {
       /* Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        someActivityResultLauncher.launch(intent);*/

        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (intent.resolveActivity(getPackageManager()) != null) {
            // Create the File where the photo should go
            Uri outputFileUri = null;
            try {
                outputFileUri = createImageFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
            // Continue only if the File was successfully created
            if (outputFileUri != null) {
                intent.putExtra(MediaStore.EXTRA_OUTPUT, outputFileUri);
                startActivityForResult(intent, CAMERA);
                //someActivityResultLauncher.launch(intent);
            }
        }
    }

    private Uri createImageFile() throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );

        // Save a file: path for use with ACTION_VIEW intents
        if(image != null){
            currentPhotoFile = image;
            currentPhotoPath = image.getAbsolutePath();
            currentPhotoPathFileUri = FileProvider.getUriForFile(this,
                    "com.lia.rohadriver.android.fileprovider",
                    image);
        }
        return currentPhotoPathFileUri;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.d("requestcode",""+requestCode+".."+resultCode+".."+data);
        if (requestCode == CAMERA  && resultCode == RESULT_OK) {
            String filePath = getImageFilePath(data);
            if (filePath != null) {
                Log.d("filePath", String.valueOf(filePath));
                bitmap = BitmapFactory.decodeFile(filePath);
                shapeableImageView.setImageBitmap(bitmap);
                /*long imagename = System.currentTimeMillis();
                imagedata = new VolleyMultipartRequest.DataPart(imagename+".jpg", getFileDataFromDrawable(bitmap));
                if(imagedata != null){
                    session.progressdialogshow();
                    uploadBitmap(img,bitmap);
                }*/
            }
        }
    }

    private String getImageFilePath(Intent data) {
        boolean isCamera = data == null || data.getData() == null;
        if (isCamera) return currentPhotoPath;
        else  return null;
    }



}