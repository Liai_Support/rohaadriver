package com.lia.rohadriver;

import static android.content.ContentValues.TAG;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.appcompat.app.AlertDialog;
import androidx.cardview.widget.CardView;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.content.FileProvider;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.material.snackbar.BaseTransientBottomBar;
import com.google.android.material.snackbar.Snackbar;
import com.pedromassango.doubleclick.DoubleClick;
import com.pedromassango.doubleclick.DoubleClickListener;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

public class WaterInprogressAdapter extends RecyclerView.Adapter<WaterInprogressAdapter.MyViewHolder>{
    private Context mcontext;
    private List<WaterInprogressDataModel> dataSet;
    private DashboardActivity dashboardActivity;
    private static final int REQUEST_PERMISSIONS = 100;
    private static final int PICK_IMAGE_REQUEST =1 ;
    public static class MyViewHolder extends RecyclerView.ViewHolder {
        CardView mainview,subcard,progresscard;
        TextView orderno,map;
        TextView orderdate,totalcarton,nametxt,quantitytext,productnametxt,orernotxt,kmtxt;
        ImageView imageView1,imageView2,imageView3,imageView4;
        LinearLayout minimize,linear1,linear2,linearimg1,linearimg2,linearimg3,linearimg4,receive,linearhome,linearwater,linearaddress;
        TextView phone,suborderno,suborderdate,subtotalcarton,ismecca,mosquename,subtotalcarton2,receivername,receivermno,paytype,amount;
        public TextView completeorder;

        public MyViewHolder(View itemView) {
            super(itemView);
            this.nametxt =itemView.findViewById(R.id.nametxt);
            this.quantitytext = itemView.findViewById(R.id.quantitytext);
            this.productnametxt = itemView.findViewById(R.id.productnametxt);
            this.orernotxt = itemView.findViewById(R.id.orernotxt);
            this.kmtxt = itemView.findViewById(R.id.kmtxt);

            this.subcard = (CardView) itemView.findViewById(R.id.sub_card);
            this.mainview = (CardView) itemView.findViewById(R.id.progresscard);
            this.linearhome = (LinearLayout) itemView.findViewById(R.id.linear_home);
            this.linearwater = (LinearLayout) itemView.findViewById(R.id.linear_water);
            this.linearaddress = (LinearLayout) itemView.findViewById(R.id.linear_address);
            this.map = (TextView) itemView.findViewById(R.id.map);
            this.minimize = (LinearLayout) itemView.findViewById(R.id.minimize);
            this.orderno = (TextView) itemView.findViewById(R.id.order_no);
            this.receive =(LinearLayout) itemView.findViewById(R.id.receive);
            this.orderdate = (TextView) itemView.findViewById(R.id.order_date);
            this.totalcarton = (TextView) itemView.findViewById(R.id.total_carton);
            this.suborderno = (TextView) itemView.findViewById(R.id.sub_order_no);
            this.suborderdate = (TextView) itemView.findViewById(R.id.sub_order_date);
            this.subtotalcarton = (TextView) itemView.findViewById(R.id.sub_total_cartons);
            this.ismecca = (TextView) itemView.findViewById(R.id.is_mecca);
            this.mosquename = (TextView) itemView.findViewById(R.id.mosque_name);
            this.subtotalcarton2 = (TextView) itemView.findViewById(R.id.sub_total_cartons2);
            this.receivername = (TextView) itemView.findViewById(R.id.receiver_name);
            this.paytype = (TextView) itemView.findViewById(R.id.paymenttype);
            this.amount = (TextView) itemView.findViewById(R.id.amount);

            this.receivermno = (TextView) itemView.findViewById(R.id.receiver_mno);
            this.phone = (TextView) itemView.findViewById(R.id.phone_txt);
            this.linear1 = (LinearLayout) itemView.findViewById(R.id.linear1);
            this.linear2 = (LinearLayout) itemView.findViewById(R.id.linear2);
            linear2.setVisibility(View.GONE);

            linearimg1 = (LinearLayout) itemView.findViewById(R.id.linear_img1);
            linearimg2 = (LinearLayout) itemView.findViewById(R.id.linear_img2);
            linearimg3 = (LinearLayout) itemView.findViewById(R.id.linear_img3);
            linearimg4 = (LinearLayout) itemView.findViewById(R.id.linear_img4);

            this.imageView1 = (ImageView)itemView.findViewById(R.id.imageView1);
            this.imageView2 = (ImageView)itemView.findViewById(R.id.imageView2);
            this.imageView3 = (ImageView)itemView.findViewById(R.id.imageView3);
            this.imageView4 = (ImageView)itemView.findViewById(R.id.imageView4);

            this.completeorder = (TextView) itemView.findViewById(R.id.complete_order);

            this.progresscard = (CardView) itemView.findViewById(R.id.progresscard);

        }
    }

    public WaterInprogressAdapter(Context context, List<WaterInprogressDataModel> data, DashboardActivity dashboardActivity) {
        this.dataSet = data;
        this.mcontext = context;
        this.dashboardActivity = dashboardActivity;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent,
                                           int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.inprogress_card, parent, false);

        MyViewHolder myViewHolder = new MyViewHolder(view);
        return myViewHolder;
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder,  final int listPosition) {
        if(dashboardActivity.session.getlang().equals("ar")){
            holder.mosquename.setGravity(View.FOCUS_RIGHT);
            holder.receivername.setGravity(View.FOCUS_RIGHT);
        }
        Log.d(TAG, "onBindViewHoldersssdd: "+dataSet.get(listPosition).getData());
        if (dataSet.get(listPosition).getIswish() == 0 ) {
            holder.nametxt.setText(""+dataSet.get(listPosition).getYourwishplace());
            holder.orernotxt.setText(mcontext.getString(R.string.ordernumber)+" : DOV"+dataSet.get(listPosition).getM_oid()+"-"+dataSet.get(listPosition).getOrderId());

        } else {

            holder.nametxt.setText(""+dataSet.get(listPosition).getMosquename());
            holder.orernotxt.setText(mcontext.getString(R.string.ordernumber)+" : DOVM"+dataSet.get(listPosition).getM_oid()+"-"+dataSet.get(listPosition).getOrderId());

        }
        holder.productnametxt.setText(""+dataSet.get(listPosition).getProductname());
        holder.quantitytext.setText(""+dataSet.get(listPosition).getCartoncount()+mcontext.getString(R.string.quantity));
//        Log.d(TAG, "milesTokm: "+StaticInfo.milesTokm(dataSet.get(listPosition).getDistance()));

        holder.kmtxt.setText(""+dataSet.get(listPosition).getDistance()+"KM" );
//       holder.progresscard.setOnClickListener(new DoubleClick(new DoubleClickListener() {
//           @Override
//           public void onSingleClick(View view) {
//               Intent intent = new Intent(mcontext, OrderDetailsActivity.class);
//               intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//               intent.putExtra("orderData",dataSet.get(listPosition).getData());
//               mcontext.startActivity(intent);
//           }
//
//           @Override
//           public void onDoubleClick(View view) {
//               Intent intent = new Intent(mcontext, OrderDetailsActivity.class);
//               intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//               intent.putExtra("orderData",dataSet.get(listPosition).getData());
//               mcontext.startActivity(intent);
//           }
//       },400));

        holder.progresscard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
              Intent intent = new Intent(mcontext, OrderDetailsActivity.class);
               intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
               intent.putExtra("orderData",dataSet.get(listPosition).getData());
               mcontext.startActivity(intent);
            }
        });


        holder.linearaddress.setVisibility(View.GONE);
        holder.linearhome.setVisibility(View.GONE);
        holder.linearwater.setVisibility(View.VISIBLE);
        holder.orderdate.setText(""+dataSet.get(listPosition).getOrderdate());
        holder.totalcarton.setText(""+dataSet.get(listPosition).getCartoncount()+" "+ dashboardActivity.getString(R.string.pack));
        //holder.paytype.setText(""+dataSet.get(listPosition).paytype);
        if(dataSet.get(listPosition).getPaytype().equalsIgnoreCase("cod")){
            holder.paytype.setText(""+dashboardActivity.getString(R.string.cashondelivery));
        }
        else if(dataSet.get(listPosition).getPaytype().equalsIgnoreCase("wallet")){
            holder.paytype.setText(""+dashboardActivity.getString(R.string.doverwallet));
        }
        else if(dataSet.get(listPosition).getPaytype().equalsIgnoreCase("card")){
            holder.paytype.setText(""+dashboardActivity.getString(R.string.onlinecard));
        }
        else if(dataSet.get(listPosition).getPaytype().equalsIgnoreCase("swipemachine")){
            holder.paytype.setText(""+dashboardActivity.getString(R.string.swipemachine));
        }
        else if(dataSet.get(listPosition).getPaytype().equalsIgnoreCase("iban")){
            holder.paytype.setText(""+dashboardActivity.getString(R.string.iban));
        }
        else {
            holder.paytype.setText(""+dataSet.get(listPosition).getPaytype());
        }
        holder.amount.setText(""+dataSet.get(listPosition).amount+" SAR");
        if(dataSet.get(listPosition).getIswish() == 1){
            holder.orderno.setText("DOVM"+dataSet.get(listPosition).getM_oid()+"-"+dataSet.get(listPosition).getOrderId());
            holder.suborderno.setText("DOVM"+dataSet.get(listPosition).getM_oid()+"-"+dataSet.get(listPosition).getOrderId());
                holder.suborderdate.setText(""+dataSet.get(listPosition).getOrderdate());
                holder.subtotalcarton.setText(""+dataSet.get(listPosition).getCartoncount()+" "+ dashboardActivity.getString(R.string.pack));
                holder.ismecca.setText(""+dataSet.get(listPosition).getIsmecca());
                holder.mosquename.setText(""+dataSet.get(listPosition).getMosquename());
                holder.subtotalcarton2.setText(""+dataSet.get(listPosition).getPname()+"-"+dataSet.get(listPosition).getCartoncount()+" "+ dashboardActivity.getString(R.string.pack));
              //  Log.d("wfscvbd","ert"+dataSet.get(listPosition).getReceivermno());
                if(dataSet.get(listPosition).getReceivermno().equals("0") || dataSet.get(listPosition).getReceivermno().equals("") || dataSet.get(listPosition).getReceivermno().isEmpty()){
                    holder.receivermno.setText("");
                }
                else {
                    holder.receivermno.setText(""+dataSet.get(listPosition).getReceivermno());
                }
                holder.receivername.setText(""+dataSet.get(listPosition).getReceivername());
                holder.linearimg1.setVisibility(View.VISIBLE);
                holder.linearimg2.setVisibility(View.VISIBLE);
                holder.linearimg3.setVisibility(View.VISIBLE);
                holder.linearimg4.setVisibility(View.VISIBLE);
                if(!dataSet.get(listPosition).getImgpath1().isEmpty()){
                    Picasso.get().load(dataSet.get(listPosition).getImgpath1()).placeholder(R.drawable.defaultimg).error(R.drawable.ic_baseline_error_24).into(holder.imageView1);
                }
                if(!dataSet.get(listPosition).getImgpath2().isEmpty()){
                    Picasso.get().load(dataSet.get(listPosition).getImgpath2()).placeholder(R.drawable.defaultimg).error(R.drawable.ic_baseline_error_24).into(holder.imageView2);

               /* RequestOptions options = new RequestOptions()
                        .centerCrop()
                        .placeholder(R.mipmap.ic_launcher_round)
                        .error(R.mipmap.ic_launcher_round);

                Glide.with(homeActivity).load(dataSet.get(listPosition).getImgpath2()).apply(options).into(holder.imageView2);*/
                }
                if(!dataSet.get(listPosition).getImgpath3().isEmpty()){
                    Picasso.get().load(dataSet.get(listPosition).getImgpath3()).placeholder(R.drawable.defaultimg).error(R.drawable.ic_baseline_error_24).into(holder.imageView3);
                }
                if(!dataSet.get(listPosition).getImgpath4().isEmpty()){
                    Picasso.get().load(dataSet.get(listPosition).getImgpath4()).placeholder(R.drawable.defaultimg).error(R.drawable.ic_baseline_error_24).into(holder.imageView4);
                }
            }
        else {
            holder.orderno.setText("DOV"+dataSet.get(listPosition).getM_oid()+"-"+dataSet.get(listPosition).getOrderId());
            holder.suborderno.setText("DOV"+dataSet.get(listPosition).getM_oid()+"-"+dataSet.get(listPosition).getOrderId());
                holder.suborderdate.setText(""+dataSet.get(listPosition).getOrderdate());
                holder.subtotalcarton.setText(""+dataSet.get(listPosition).getCartoncount()+" "+ dashboardActivity.getString(R.string.pack));
                holder.ismecca.setText(""+dataSet.get(listPosition).getPlace()+"-"+dataSet.get(listPosition).getBranchname());
                holder.mosquename.setText(""+dataSet.get(listPosition).getYourwishplace());
                holder.subtotalcarton2.setText(""+dataSet.get(listPosition).getPname()+"-"+dataSet.get(listPosition).getCartoncount()+" "+ dashboardActivity.getString(R.string.pack));
                holder.receivername.setText(""+dataSet.get(listPosition).getReceivername());
              //  Log.d("wfscvbd","ert"+dataSet.get(listPosition).getReceivermno());
                if(dataSet.get(listPosition).getReceivermno().equals("0") || dataSet.get(listPosition).getReceivermno().equals("") || dataSet.get(listPosition).getReceivermno().isEmpty()){
                    holder.receivermno.setText("");
                }
                else {
                    holder.receivermno.setText(""+dataSet.get(listPosition).getReceivermno());
                }
                holder.linearimg1.setVisibility(View.VISIBLE);
                holder.linearimg2.setVisibility(View.VISIBLE);
                holder.linearimg3.setVisibility(View.VISIBLE);
                holder.linearimg4.setVisibility(View.VISIBLE);
                holder.map.setVisibility(View.VISIBLE);
                if(!dataSet.get(listPosition).getImgpath1().isEmpty()){
                    Picasso.get().load(dataSet.get(listPosition).getImgpath1()).placeholder(R.drawable.defaultimg).error(R.drawable.ic_baseline_error_24).into(holder.imageView1);
                }
           /* if(!dataSet.get(listPosition).getImgpath2().isEmpty()){
                Picasso.get().load(dataSet.get(listPosition).getImgpath2()).into(holder.imageView2);
            }
            if(!dataSet.get(listPosition).getImgpath3().isEmpty()){
                Picasso.get().load(dataSet.get(listPosition).getImgpath3()).into(holder.imageView3);
            }
            if(!dataSet.get(listPosition).getImgpath4().isEmpty()){
                Picasso.get().load(dataSet.get(listPosition).getImgpath4()).into(holder.imageView4);
            }*/
            }

        holder.subcard.setVisibility(View.GONE);
        holder.linear1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                holder.linear1.setVisibility(View.GONE);
                holder.linear2.setVisibility(View.VISIBLE);
                holder.subcard.setVisibility(View.VISIBLE);
            }
        });
        holder.minimize.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                holder.linear2.setVisibility(View.GONE);
                holder.linear1.setVisibility(View.VISIBLE);
                holder.subcard.setVisibility(View.GONE);
            }
        });
        holder.map.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                Double latitude = dataSet.get(listPosition).getLat();
                Double longitude = dataSet.get(listPosition).getLng();
                Log.d("lat long",""+latitude);
                Log.d("lat long",""+longitude);
                String uri = String.format(Locale.ENGLISH, "google.navigation:q="+latitude+","+longitude);
               /* Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(uri));
                homeActivity.startActivity(intent);*/
                Log.d("uri",uri);
                Uri gmmIntentUri = Uri.parse(uri);
                Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
                mapIntent.setPackage("com.google.android.apps.maps");
                dashboardActivity.startActivity(mapIntent);
            }
        });
        holder.receivermno.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!holder.receivermno.getText().equals("")) {
                    try {
                        String text = "";// Replace with your message.
                        String toNumber = ""+holder.receivermno.getText(); // Replace with mobile phone number without +Sign or leading zeros, but with country code
                        //Suppose your country is India and your phone number is “xxxxxxxxxx”, then you need to send “91xxxxxxxxxx”.
                        Intent intent = new Intent(Intent.ACTION_VIEW);
                        intent.setData(Uri.parse("http://api.whatsapp.com/send?phone=" + toNumber + "&text=" + text));
                        dashboardActivity.startActivity(intent);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                else {
                    if(!dataSet.get(listPosition).getCustomer_ccode().equalsIgnoreCase("") && !dataSet.get(listPosition).getCmno().equalsIgnoreCase("")){
                        try {
                            String text = "";// Replace with your message.
                            String toNumber = ""+dataSet.get(listPosition).getCustomer_ccode()+dataSet.get(listPosition).getCmno(); // Replace with mobile phone number without +Sign or leading zeros, but with country code
                            //Suppose your country is India and your phone number is “xxxxxxxxxx”, then you need to send “91xxxxxxxxxx”.
                            Intent intent = new Intent(Intent.ACTION_VIEW);
                            intent.setData(Uri.parse("http://api.whatsapp.com/send?phone=" + toNumber + "&text=" + text));
                            dashboardActivity.startActivity(intent);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
        });
        holder.phone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!holder.receivermno.getText().equals("")) {
                    try {
                        String text = "";// Replace with your message.
                        String toNumber = ""+holder.receivermno.getText(); // Replace with mobile phone number without +Sign or leading zeros, but with country code
                        //Suppose your country is India and your phone number is “xxxxxxxxxx”, then you need to send “91xxxxxxxxxx”.
                        Intent intent = new Intent(Intent.ACTION_VIEW);
                        intent.setData(Uri.parse("http://api.whatsapp.com/send?phone=" + toNumber + "&text=" + text));
                        dashboardActivity.startActivity(intent);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                else {
                    if (!dataSet.get(listPosition).getCustomer_ccode().equalsIgnoreCase("") && !dataSet.get(listPosition).getCmno().equalsIgnoreCase("")) {
                        try {
                            String text = "";// Replace with your message.
                            String toNumber = ""+dataSet.get(listPosition).getCustomer_ccode()+dataSet.get(listPosition).getCmno(); // Replace with mobile phone number without +Sign or leading zeros, but with country code
                            //Suppose your country is India and your phone number is “xxxxxxxxxx”, then you need to send “91xxxxxxxxxx”.
                            Intent intent = new Intent(Intent.ACTION_VIEW);
                            intent.setData(Uri.parse("http://api.whatsapp.com/send?phone=" + toNumber + "&text=" + text));
                            dashboardActivity.startActivity(intent);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
        });
        holder.imageView1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                dashboardActivity.showPictureDialog(dataSet.get(listPosition).getOrderId(),"image1",holder.imageView1);
            }
        });
        holder.imageView2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                dashboardActivity.showPictureDialog(dataSet.get(listPosition).getOrderId(),"image2",holder.imageView2);
            }
        });
        holder.imageView3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                dashboardActivity.showPictureDialog(dataSet.get(listPosition).getOrderId(),"image3",holder.imageView3);
            }
        });
        holder.imageView4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                dashboardActivity.showPictureDialog(dataSet.get(listPosition).getOrderId(),"image4",holder.imageView4);
            }
        });
        holder.completeorder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                JSONObject requestbody1 = new JSONObject();
                try {
                    dashboardActivity.session.progressdialogshow();
                    requestbody1.put("id", dashboardActivity.session.getUserId());
                    requestbody1.put("lat", dashboardActivity.session.getlat());
                    requestbody1.put("lng", dashboardActivity.session.getlng());
                    assignedorderrequestJSON(requestbody1,dataSet.get(listPosition).getOrderId());
                } catch (JSONException e) {
                    e.printStackTrace();
                }
              /*  if(dataSet.get(listPosition).getIswish() == 1 && !dataSet.get(listPosition).getImgpath1().isEmpty() && !dataSet.get(listPosition).getImgpath2().isEmpty() && !dataSet.get(listPosition).getImgpath3().isEmpty() && !dataSet.get(listPosition).getImgpath4().isEmpty()){
                    Snackbar.make(view, R.string.pleasecompleteorder, Snackbar.LENGTH_LONG)
                            .setTextColor(ContextCompat.getColor(homeActivity,R.color.design_default_color_error))
                            .setBackgroundTint(ContextCompat.getColor(homeActivity,R.color.yellow_update))
                            .setDuration(BaseTransientBottomBar.LENGTH_LONG)
                            .setAnimationMode(BaseTransientBottomBar.ANIMATION_MODE_SLIDE)
                            .setAction(R.string.ok, new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    homeActivity.progressDialog.show();
                                    JSONObject requestbody = new JSONObject();
                                    try {
                                        requestbody.put("orderdetId",""+dataSet.get(listPosition).getId());
                                        requestbody.put("status","completed");
                                        orderstatusrequestJSON(requestbody);
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                            }
                            })
                            .setActionTextColor(Color.BLACK)
                            .show();
                }
                else {
                    if(!dataSet.get(listPosition).getImgpath1().isEmpty() && dataSet.get(listPosition).getIswish() == 0){
                        Snackbar.make(view, R.string.pleasecompleteorder, Snackbar.LENGTH_LONG)
                                .setTextColor(ContextCompat.getColor(homeActivity,R.color.design_default_color_error))
                                .setBackgroundTint(ContextCompat.getColor(homeActivity,R.color.white))
                                .setDuration(BaseTransientBottomBar.LENGTH_LONG)
                                .setAnimationMode(BaseTransientBottomBar.ANIMATION_MODE_SLIDE)
                                .setAction(R.string.ok, new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                        JSONObject requestbody = new JSONObject();
                                        try {
                                            requestbody.put("orderdetId",""+dataSet.get(listPosition).getId());
                                            requestbody.put("status","completed");
                                            orderstatusrequestJSON(requestbody);
                                        } catch (JSONException e) {
                                            e.printStackTrace();
                                        }
                                    }
                                })
                                .setActionTextColor(Color.BLACK)
                                .show();
                    }
                    else {
                        Snackbar.make(view, R.string.pleaseuploadphoto, Snackbar.LENGTH_SHORT)
                                .setTextColor(ContextCompat.getColor(mcontext,R.color.design_default_color_error))
                                .setBackgroundTint(ContextCompat.getColor(mcontext,R.color.white))
                                .setDuration(BaseTransientBottomBar.LENGTH_LONG)
                                .setAnimationMode(BaseTransientBottomBar.ANIMATION_MODE_SLIDE).show();
                    }
                }*/
            }
        });
    }

    private void assignedorderrequestJSON(JSONObject response,int orderid) {
        RequestQueue requestQueue = Volley.newRequestQueue(mcontext);
        final String requestBody = response.toString();
        Log.d("request234",String.valueOf(response));
        StringRequest stringRequest = new StringRequest(Request.Method.POST, StaticInfo.assigned_orders, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d("assignedorderresponse", ">>" + response);
                dashboardActivity.session.progressdialogdismiss();
                try{
                    JSONObject obj = new JSONObject(response);
                    if(obj.getBoolean("error") == false){
                        JSONObject json = obj.getJSONObject("data");
                        JSONArray obj1 = new JSONArray(json.getString("water"));
                        View view = dashboardActivity.recyclerwater;
                        for (int i=0; i<obj1.length(); i++){
                            JSONObject jsonObject = obj1.getJSONObject(i);
                            if(jsonObject.getInt("Id") == orderid){
                                if(jsonObject.getInt("iswish") == 1){
                                    if(jsonObject.getString("image_path1").equals("") || jsonObject.getString("image_path2").equals("") || jsonObject.getString("image_path3").equals("") || jsonObject.getString("image_path4").equals("")){
                                            Snackbar.make(view, R.string.pleaseuploadphoto, Snackbar.LENGTH_SHORT)
                                                    .setTextColor(ContextCompat.getColor(mcontext, R.color.design_default_color_error))
                                                    .setBackgroundTint(ContextCompat.getColor(mcontext, R.color.yellow_update))
                                                    .setDuration(BaseTransientBottomBar.LENGTH_LONG)
                                                    .setAnimationMode(BaseTransientBottomBar.ANIMATION_MODE_SLIDE).show();
                                    }
                                    else {
                                        //completedialog(orderid,"water");
                                        JSONObject requestbody = new JSONObject();
                                        try {
                                            requestbody.put("orderdetId",""+orderid);
                                            requestbody.put("status","completed");
                                            requestbody.put("orderfor", "water");
                                            orderstatusrequestJSON(requestbody);
                                            dashboardActivity.session.progressdialogshow();
                                        } catch (JSONException e) {
                                            e.printStackTrace();
                                        }
                                    }

                                }
                                else if(jsonObject.getInt("iswish") == 0){
                                    if(jsonObject.getString("image_path1").equals("") || jsonObject.getString("image_path2").equals("") || jsonObject.getString("image_path3").equals("") || jsonObject.getString("image_path4").equals("")){
                                            Snackbar.make(view, R.string.pleaseuploadphoto, Snackbar.LENGTH_SHORT)
                                                    .setTextColor(ContextCompat.getColor(mcontext, R.color.design_default_color_error))
                                                    .setBackgroundTint(ContextCompat.getColor(mcontext, R.color.yellow_update))
                                                    .setDuration(BaseTransientBottomBar.LENGTH_LONG)
                                                    .setAnimationMode(BaseTransientBottomBar.ANIMATION_MODE_SLIDE).show();
                                    }
                                    else {
                                        //completedialog(orderid,"water");
                                        JSONObject requestbody = new JSONObject();
                                        try {
                                            requestbody.put("orderdetId",""+orderid);
                                            requestbody.put("status","completed");
                                            requestbody.put("orderfor", "water");
                                            orderstatusrequestJSON(requestbody);
                                            dashboardActivity.session.progressdialogshow();
                                        } catch (JSONException e) {
                                            e.printStackTrace();
                                        }
                                    }
                                }
                            }
                        }
                    }
                    else {

                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                dashboardActivity.session.progressdialogdismiss();
                dashboardActivity.session.volleyerror(error);
                Log.e("VOLLEY", error.toString());
            }
        }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                int id = dashboardActivity.session.getUserId();
                Map<String, String> params = new HashMap<String, String>();
                //params.put("Content-Type","application/json" );
                params.put("Authorization", "Bearer "+ dashboardActivity.session.gettoken());
                Log.d("param",""+params);
                Log.d("id",""+id);
                return params;
            }

            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }

            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return requestBody == null ? null : requestBody.getBytes("utf-8");
                } catch (UnsupportedEncodingException uee) {
                    VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                    return null;
                }
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(10000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        Log.d("asdsdjkxdk",""+stringRequest);
        requestQueue.add(stringRequest);

    }

    private void orderstatusrequestJSON(JSONObject response) {
        RequestQueue requestQueue = Volley.newRequestQueue(mcontext);
        final String requestBody = response.toString();
        Log.d("request234",String.valueOf(response));
        StringRequest stringRequest = new StringRequest(Request.Method.POST, StaticInfo.orderstatusupdate, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                dashboardActivity.session.progressdialogdismiss();
                Log.d("assignedorderresponse", ">>" + response);
                try{
                    JSONObject obj = new JSONObject(response);
                    if(obj.getBoolean("error") == false){
                        dashboardActivity.finish();
                        Intent intent1 = new Intent(dashboardActivity, DashboardActivity.class);
                        dashboardActivity.startActivity(intent1);
                    }
                    else {
                        dashboardActivity.session.snackbarToast(obj.getString("message"),dashboardActivity.view);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                dashboardActivity.session.progressdialogdismiss();
                Log.e("VOLLEY", error.toString());
            }
        }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                String token = dashboardActivity.session.gettoken();
                Map<String, String> params = new HashMap<String, String>();
//                params.put("Content-Type","application/json" );
                params.put("Authorization", "Bearer "+token);
                Log.d("param",""+params);
                return params;
            }

            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }

            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return requestBody == null ? null : requestBody.getBytes("utf-8");
                } catch (UnsupportedEncodingException uee) {
                    VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                    return null;
                }
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(10000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        Log.d("asdsdjkxdk",""+stringRequest);
        requestQueue.add(stringRequest);

    }


    public void completedialog(int orderid,String orderfor) {
        // Initializing a new alert dialog
        AlertDialog.Builder builder = new AlertDialog.Builder(dashboardActivity);

        // Set a title for alert dialog
        //builder.setTitle("Say Hello!");

        // Show a message on alert dialog
        builder.setMessage(dashboardActivity.getString(R.string.pleasemakesuretocompletetheorder));

        // Set the positive button
        builder.setPositiveButton(dashboardActivity.getString(R.string.yes), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
                JSONObject requestbody = new JSONObject();
                try {
                    requestbody.put("orderdetId", ""+orderid);
                    requestbody.put("status", "completed");
                    requestbody.put("orderfor", orderfor);
                    orderstatusrequestJSON(requestbody);
                    dashboardActivity.session.progressdialogshow();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });

        // Set the negative button
        builder.setNegativeButton(dashboardActivity.getString(R.string.no),  new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        // Set the neutral button
        //builder.setNeutralButton("Cancel", null);

        // Create the alert dialog
        AlertDialog dialog = builder.create();

        // Finally, display the alert dialog
        dialog.show();

        // Get the alert dialog buttons reference
        Button positiveButton = dialog.getButton(AlertDialog.BUTTON_POSITIVE);
        Button negativeButton = dialog.getButton(AlertDialog.BUTTON_NEGATIVE);
        //Button neutralButton = dialog.getButton(AlertDialog.BUTTON_NEUTRAL);

        // Change the alert dialog buttons text and background color
        positiveButton.setTextColor(dashboardActivity.getResources().getColorStateList(R.color.black));
        //positiveButton.setBackgroundColor(Color.parseColor("#FFE1FCEA"));

        negativeButton.setTextColor(dashboardActivity.getResources().getColorStateList(R.color.black));
        //negativeButton.setBackgroundColor(Color.parseColor("#FFFCB9B7"));

        //neutralButton.setTextColor(Color.parseColor("#FF1B5AAC"));
        //neutralButton.setBackgroundColor(Color.parseColor("#FFD9E9FF"));
    }

    @Override
    public int getItemCount() {
        return dataSet.size();
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }
}