package com.lia.rohadriver;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.app.Dialog;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.media.AudioAttributes;
import android.media.RingtoneManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.bumptech.glide.load.resource.bitmap.FitCenter;
import com.squareup.picasso.Picasso;

import java.util.Locale;

public class SplashActivity extends AppCompatActivity implements Animation.AnimationListener {
    Animation animFadeIn;
    RelativeLayout relativeLayout;
    ImageView imageView;
    private Session session;
    View view;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        view = (RelativeLayout) findViewById(R.id.relativeLayout);

        session = new Session(this,this,view);
        session.setIsVersion(false);
        imageView = (ImageView) findViewById(R.id.splach_image);

        if (Build.VERSION.SDK_INT < 16) {
            getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                    WindowManager.LayoutParams.FLAG_FULLSCREEN);

        } else {
            View decorView = getWindow().getDecorView();
            // Hide the status bar.
            int uiOptions = View.SYSTEM_UI_FLAG_FULLSCREEN;
            decorView.setSystemUiVisibility(uiOptions);
            // Remember that you should never show the action bar if the
            // status bar is hidden, so hide that too if necessary.
        }
        // load the animation
        animFadeIn = AnimationUtils.loadAnimation(getApplicationContext(),
                R.anim.animation_fade_in);
        // set animation listener
        animFadeIn.setAnimationListener(this);
        // animation for image
        relativeLayout = (RelativeLayout) findViewById(R.id.relativeLayout);
        // start the animation
        relativeLayout.setVisibility(View.VISIBLE);
        relativeLayout.startAnimation(animFadeIn);
        if (session.getlang().equals("ar")) {
            session.setAppLocale("ar");
        }
        else{
            session.setAppLocale("en");
        }
        createNotificationChannels();
    }

    @Override
    public void onBackPressed() {
        this.finish();
        super.onBackPressed();
    }

    @Override
    public void onAnimationStart(Animation animation) {
        //under Implementation
    }

    public void onAnimationEnd(Animation animation) {
        // Start Main Screen
        if (session.isNetworkEnabled() == true){
            Intent i = new Intent(SplashActivity.this, LanguageActivity.class);
            startActivity(i);
            this.finish();
        }
        else {
            session.noNetworkDialog(this);
        }
    }

    @Override
    public void onAnimationRepeat(Animation animation) {
        //under Implementation
    }

    private void createNotificationChannels() {
        Uri uri= RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        AudioAttributes attributes = new AudioAttributes.Builder().setUsage(AudioAttributes.USAGE_NOTIFICATION).build();
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            NotificationManager notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
            NotificationChannel chatGroupHangUp = new NotificationChannel(
                    "Notification",
                    "Notification",
                    NotificationManager.IMPORTANCE_HIGH
            );
            chatGroupHangUp.enableVibration(true);
            chatGroupHangUp.setSound(uri,attributes);
            notificationManager.createNotificationChannel(chatGroupHangUp);

           /* Uri sound = Uri.parse(ContentResolver.SCHEME_ANDROID_RESOURCE + "://" + this.getPackageName() + "/" + R.raw.driver_notification);  //Here is FILE_NAME is the name of file that you want to play
            AudioAttributes attributes = new AudioAttributes.Builder()
                    .setUsage(AudioAttributes.USAGE_NOTIFICATION)
                    .build();
            NotificationChannel channel_all = new NotificationChannel("Notification_Sound", "Notification_Sound", NotificationManager.IMPORTANCE_HIGH);
            channel_all.enableVibration(true);
            channel_all.setSound(sound, attributes); // This is IMPORTANT
            notificationManager.createNotificationChannel(channel_all);*/
        }

    }

}
