package com.lia.rohadriver;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.material.snackbar.BaseTransientBottomBar;
import com.google.android.material.snackbar.Snackbar;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;

public class ResetPasswordActivity extends AppCompatActivity implements View.OnClickListener {

    EditText cpass,npass,cnpass;
    Button reset;
    private Session session;
    ImageView back;
    View view;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reset_password);
        view = (RelativeLayout) findViewById(R.id.root);
        session = new Session(this,this,view);
        if (!session.isNetworkEnabled() == true){
            session.noNetworkDialog(this);
        }
        cpass = (EditText) findViewById(R.id.cpass);
        npass = (EditText) findViewById(R.id.npass);
        cnpass = (EditText) findViewById(R.id.cnpass);
        reset = (Button) findViewById(R.id.reset);
        reset.setOnClickListener(this);
        back = (ImageView) findViewById(R.id.back);
        back.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        if (!session.isNetworkEnabled() == true){
            session.noNetworkDialog(this);
        }
        else {
            if (view == reset) {
                String specialcharreg = ".*[@#!$%^&+=].*";
                String uppercasereg = ".*[A-Z].*";
                String numberreg = ".*[0-9].*";
                String cpassword = String.valueOf(cpass.getText().toString());
                String npassword = String.valueOf(npass.getText().toString());
                String cnpassword = String.valueOf(cnpass.getText().toString());
                if (!npassword.equals(cnpassword)) {
                    npass.setError(getString(R.string.enterpasscorrectly));
                    cnpass.setError(getString(R.string.enterpasscorrectly));
                    snackbarToast(getString(R.string.passwordandconfirmpasswordmismatch));
                } else if (cpass.getText().toString().trim().length() == 0 || npass.getText().toString().trim().length() == 0 || cnpass.getText().toString().trim().length() == 0) {
                    cpass.setError(getString(R.string.enterallfields));
                    npass.setError(getString(R.string.enterallfields));
                    cnpass.setError(getString(R.string.enterallfields));
                    snackbarToast(getString(R.string.enterallfields));
                }
            /*else if ((npassword.length() < 8) || (!npassword.matches(specialcharreg)) || (!npassword.matches(uppercasereg)) || (!npassword.matches(numberreg))) {
                npass.setError(getString(R.string.passmusthavesymbol));
                cnpass.setError(getString(R.string.passmusthavesymbol));
                //Snackbar.make(view,"Password must have 8 Characters,One UpperCase,Numeric,Special Symbol",Snackbar.LENGTH_SHORT).show();
                snackbarToast(getString(R.string.passmusthavesymbol));
            }*/
                else if (cpassword.equals(npassword)) {
                    snackbarToast(getString(R.string.currentpasswordandnewpasswordissame));
                    npass.setError(getString(R.string.enterpasscorrectly));
                    cnpass.setError(getString(R.string.enterpasscorrectly));
                } else {
                    JSONObject requestbody = new JSONObject();
                    try {
                        requestbody.put("userid", session.getUserId());
                        requestbody.put("oldpassword", cpassword);
                        requestbody.put("password", npassword);
                        resetrequestJSON(requestbody);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            } else if (view == back) {
               /* Intent intent = new Intent(ResetPasswordActivity.this, HomeActivity.class);
                startActivity(intent);*/
                onBackPressed();
            }
        }
    }

    private void resetrequestJSON(JSONObject response) {
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        final String requestBody = response.toString();
        Log.d("request",String.valueOf(response));
        StringRequest stringRequest = new StringRequest(Request.Method.POST, StaticInfo.resetpassword, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d("passwordresetresponse", ">>" + response);
                try{
                    JSONObject obj = new JSONObject(response);
                    if(obj.getBoolean("error") == true){
                        String msg = obj.getString("message");
                        snackbarToast(getString(R.string.currentpasswordisincorrect));
                        cpass.setError(getString(R.string.enterpasscorrectly));
                    }
                    else {
                        String msg = obj.getString("message");
                        snackbarToast(String.valueOf(R.string.passupdatesucess));
                        View view = findViewById(R.id.reset);
                        Snackbar.make(view, msg, Snackbar.LENGTH_LONG)
                                .setTextColor(ContextCompat.getColor(ResetPasswordActivity.this,R.color.design_default_color_error))
                                .setBackgroundTint(ContextCompat.getColor(ResetPasswordActivity.this,R.color.white))
                                .setDuration(BaseTransientBottomBar.LENGTH_LONG)
                                .setAnimationMode(BaseTransientBottomBar.ANIMATION_MODE_SLIDE)
                                .setAction("Home", new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                        Intent intent = new Intent(ResetPasswordActivity.this, DashboardActivity.class);
                                        startActivity(intent);
                                    }
                                })
                                .setActionTextColor(Color.BLACK)
                                .show();
                        Intent intent = new Intent(ResetPasswordActivity.this, DashboardActivity.class);
                        startActivity(intent);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("VOLLEY", error.toString());
            }
        }){
            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }

            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return requestBody == null ? null : requestBody.getBytes("utf-8");
                } catch (UnsupportedEncodingException uee) {
                    VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                    return null;
                }
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(10000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        Log.d("asdsdjkxdk",""+stringRequest);
        requestQueue.add(stringRequest);
    }

    private void snackbarToast(String msg){
        Snackbar snackbar ;
        //contextView = findViewById(R.id.contextview);
        View view = findViewById(R.id.reset);
        Snackbar.make(view, msg, Snackbar.LENGTH_SHORT)
                .setTextColor(ContextCompat.getColor(ResetPasswordActivity.this,R.color.design_default_color_error))
                .setBackgroundTint(ContextCompat.getColor(ResetPasswordActivity.this,R.color.white))
                .setDuration(BaseTransientBottomBar.LENGTH_LONG)
                .setAnimationMode(BaseTransientBottomBar.ANIMATION_MODE_SLIDE)
                .show();
    }

   /* @Override
    public void onBackPressed() {
        *//*Intent intent = new Intent(ResetPasswordActivity.this, HomeActivity.class);
        startActivity(intent);*//*
        onBackPressed();
    }*/
}