package com.lia.rohadriver;

import static android.content.ContentValues.TAG;

import android.app.Dialog;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.facebook.drawee.view.SimpleDraweeView;
import com.squareup.picasso.Picasso;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class WaterCompletedAdapter extends RecyclerView.Adapter<WaterCompletedAdapter.MyViewHolder> {
    private Context mcontext;
    private List<WaterCompletedDataModel> dataSet;
    private DashboardActivity dashboardActivity;
    public static class MyViewHolder extends RecyclerView.ViewHolder {
        CardView mainview,subcard;
        TextView orderno,map;
        TextView orderdate,totalcarton,completeorder,text,nametxt,quantitytext,productnametxt,orernotxt,kmtxt;
        ImageView imageView1,imageView2,imageView3,imageView4;
        LinearLayout minimize,linear1,linear2,linearimg1,linearimg2,linearimg3,linearimg4,receive,linearhome,linearwater,linearaddress;
        TextView suborderno,suborderdate,subtotalcarton,ismecca,mosquename,subtotalcarton2,receivername,receivermno,paytype,amount;


        public MyViewHolder(View itemView) {
            super(itemView);

            this.nametxt =itemView.findViewById(R.id.nametxt);
            this.quantitytext = itemView.findViewById(R.id.quantitytext);
            this.productnametxt = itemView.findViewById(R.id.productnametxt);
            this.orernotxt = itemView.findViewById(R.id.orernotxt);
            this.kmtxt = itemView.findViewById(R.id.kmtxt);

            this.subcard = (CardView) itemView.findViewById(R.id.sub_card);
            this.mainview = (CardView) itemView.findViewById(R.id.progresscard);
            this.receive =(LinearLayout) itemView.findViewById(R.id.receive);
            this.map = (TextView) itemView.findViewById(R.id.map);
            this.minimize = (LinearLayout) itemView.findViewById(R.id.minimize);
            this.paytype = (TextView) itemView.findViewById(R.id.paymenttype);
            this.amount = (TextView) itemView.findViewById(R.id.amount);
            this.orderno = (TextView) itemView.findViewById(R.id.order_no);
            this.orderdate = (TextView) itemView.findViewById(R.id.order_date);
            this.totalcarton = (TextView) itemView.findViewById(R.id.total_carton);
            this.suborderno = (TextView) itemView.findViewById(R.id.sub_order_no);
            this.suborderdate = (TextView) itemView.findViewById(R.id.sub_order_date);
            this.subtotalcarton = (TextView) itemView.findViewById(R.id.sub_total_cartons);
            this.ismecca = (TextView) itemView.findViewById(R.id.is_mecca);
            this.mosquename = (TextView) itemView.findViewById(R.id.mosque_name);
            this.subtotalcarton2 = (TextView) itemView.findViewById(R.id.sub_total_cartons2);
            this.receivername = (TextView) itemView.findViewById(R.id.receiver_name);
            this.receivermno = (TextView) itemView.findViewById(R.id.receiver_mno);
            this.linear1 = (LinearLayout) itemView.findViewById(R.id.linear1);
            this.linear2 = (LinearLayout) itemView.findViewById(R.id.linear2);
            linear2.setVisibility(View.GONE);

            linearimg1 = (LinearLayout) itemView.findViewById(R.id.linear_img1);
            linearimg2 = (LinearLayout) itemView.findViewById(R.id.linear_img2);
            linearimg3 = (LinearLayout) itemView.findViewById(R.id.linear_img3);
            linearimg4 = (LinearLayout) itemView.findViewById(R.id.linear_img4);

            this.imageView1 = (ImageView)itemView.findViewById(R.id.imageView1);
            this.imageView2 = (ImageView)itemView.findViewById(R.id.imageView2);
            this.imageView3 = (ImageView)itemView.findViewById(R.id.imageView3);
            this.imageView4 = (ImageView)itemView.findViewById(R.id.imageView4);
            this.completeorder = (TextView) itemView.findViewById(R.id.complete_order);
            completeorder.setVisibility(View.GONE);
            this.text = (TextView) itemView.findViewById(R.id.text);
            text.setVisibility(View.GONE);
            map.setVisibility(View.GONE);
            this.linearhome = (LinearLayout) itemView.findViewById(R.id.linear_home);
            this.linearwater = (LinearLayout) itemView.findViewById(R.id.linear_water);
            this.linearaddress = (LinearLayout) itemView.findViewById(R.id.linear_address);

            linearaddress.setVisibility(View.GONE);
            linearhome.setVisibility(View.GONE);
            linearwater.setVisibility(View.VISIBLE);
        }
    }

    public WaterCompletedAdapter(Context context, List<WaterCompletedDataModel> data, DashboardActivity dashboardActivity) {
        this.dataSet = data;
        mcontext = context;
        this.dashboardActivity = dashboardActivity;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent,
                                           int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.inprogress_card, parent, false);

        MyViewHolder myViewHolder = new MyViewHolder(view);
        return myViewHolder;
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int listPosition) {
        Log.d(TAG, "onBindViewHolder:pos "+listPosition+dataSet.get(listPosition).getM_oid());
        holder.orderdate.setText(""+dataSet.get(listPosition).getDeliverydate());
        holder.totalcarton.setText(""+dataSet.get(listPosition).getCartoncount()+" Cartons");

        if (dataSet.get(listPosition).getIswish() == 0 ) {
            holder.nametxt.setText(""+dataSet.get(listPosition).getYourwishplace());
            holder.orernotxt.setText(mcontext.getString(R.string.ordernumber)+" : DOV"+dataSet.get(listPosition).getM_oid()+"-"+dataSet.get(listPosition).getOrderId());

        } else {

            holder.nametxt.setText(""+dataSet.get(listPosition).getMosquename());
            holder.orernotxt.setText(mcontext.getString(R.string.ordernumber)+" : DOVM"+dataSet.get(listPosition).getM_oid()+"-"+dataSet.get(listPosition).getOrderId());

        }

        holder.productnametxt.setText(""+dataSet.get(listPosition).getProductname());
        holder.quantitytext.setText(""+dataSet.get(listPosition).getCartoncount()+mcontext.getString(R.string.quantity));
        holder.kmtxt.setText(""+mcontext.getString(R.string.deliveredtime)+":"+ dataSet.get(listPosition).getDeliverydate());


       // holder.paytype.setText(""+dataSet.get(listPosition).paytype);
        if(dataSet.get(listPosition).getPaytype().equalsIgnoreCase("cod")){
            holder.paytype.setText(""+dashboardActivity.getString(R.string.cashondelivery));
        }
        else if(dataSet.get(listPosition).getPaytype().equalsIgnoreCase("wallet")){
            holder.paytype.setText(""+dashboardActivity.getString(R.string.doverwallet));
        }
        else if(dataSet.get(listPosition).getPaytype().equalsIgnoreCase("card")){
            holder.paytype.setText(""+dashboardActivity.getString(R.string.onlinecard));
        }
        else if(dataSet.get(listPosition).getPaytype().equalsIgnoreCase("swipemachine")){
            holder.paytype.setText(""+dashboardActivity.getString(R.string.swipemachine));
        }
        else if(dataSet.get(listPosition).getPaytype().equalsIgnoreCase("iban")){
            holder.paytype.setText(""+dashboardActivity.getString(R.string.iban));
        }
        else {
            holder.paytype.setText(""+dataSet.get(listPosition).getPaytype());
        }
        holder.amount.setText(""+dataSet.get(listPosition).amount+" SAR");

        if(dataSet.get(listPosition).getIswish() == 1){
            holder.orderno.setText("DOVM"+dataSet.get(listPosition).getM_oid()+"-"+dataSet.get(listPosition).getOrderId());
            holder.suborderno.setText("DOVM"+dataSet.get(listPosition).getM_oid()+"-"+dataSet.get(listPosition).getOrderId());
            holder.suborderdate.setText(""+dataSet.get(listPosition).getOrderdate());
            holder.subtotalcarton.setText(""+dataSet.get(listPosition).getCartoncount()+" "+ dashboardActivity.getString(R.string.pack));
            holder.ismecca.setText(""+dataSet.get(listPosition).getIsmecca());
            holder.mosquename.setText(""+dataSet.get(listPosition).getMosquename());
            holder.subtotalcarton2.setText(""+dataSet.get(listPosition).getPname()+"-"+dataSet.get(listPosition).getCartoncount()+" "+ dashboardActivity.getString(R.string.pack));
            holder.receivermno.setText(""+dataSet.get(listPosition).getReceivermno());
            Log.d("wfscvbd","ert"+dataSet.get(listPosition).getReceivermno());
            if(dataSet.get(listPosition).getReceivermno().equals("0") || dataSet.get(listPosition).getReceivermno().equals("null")){
                Log.d("wfscvbd","ert"+dataSet.get(listPosition).getReceivermno());
                holder.receivermno.setText("");
            }
            holder.receivername.setText(""+dataSet.get(listPosition).getReceivername());
            Log.d("ghj","photo4");
            holder.linearimg1.setVisibility(View.VISIBLE);
            holder.linearimg2.setVisibility(View.VISIBLE);
            holder.linearimg3.setVisibility(View.VISIBLE);
            holder.linearimg4.setVisibility(View.VISIBLE);
            if(!dataSet.get(listPosition).getImgpath1().isEmpty()){
                Picasso.get().load(dataSet.get(listPosition).getImgpath1()).placeholder(R.drawable.defaultimg).error(R.drawable.ic_baseline_error_24).into(holder.imageView1);
            }
            if(!dataSet.get(listPosition).getImgpath2().isEmpty()){
                Picasso.get().load(dataSet.get(listPosition).getImgpath2()).placeholder(R.drawable.defaultimg).error(R.drawable.ic_baseline_error_24).into(holder.imageView2);

               /* RequestOptions options = new RequestOptions()
                        .centerCrop()
                        .placeholder(R.mipmap.ic_launcher_round)
                        .error(R.mipmap.ic_launcher_round);

                Glide.with(homeActivity).load(dataSet.get(listPosition).getImgpath2()).apply(options).into(holder.imageView2);*/
            }
            if(!dataSet.get(listPosition).getImgpath3().isEmpty()){
                Picasso.get().load(dataSet.get(listPosition).getImgpath3()).placeholder(R.drawable.defaultimg).error(R.drawable.ic_baseline_error_24).into(holder.imageView3);
            }
            if(!dataSet.get(listPosition).getImgpath4().isEmpty()){
                Picasso.get().load(dataSet.get(listPosition).getImgpath4()).placeholder(R.drawable.defaultimg).error(R.drawable.ic_baseline_error_24).into(holder.imageView4);
            }
        }
        else {
            holder.orderno.setText("DOV"+dataSet.get(listPosition).getM_oid()+"-"+dataSet.get(listPosition).getOrderId());
            holder.suborderno.setText("DOV"+dataSet.get(listPosition).getM_oid()+"-"+dataSet.get(listPosition).getOrderId());
            holder.suborderdate.setText(""+dataSet.get(listPosition).getOrderdate());
            holder.subtotalcarton.setText(""+dataSet.get(listPosition).getCartoncount()+" "+ dashboardActivity.getString(R.string.pack));
            holder.ismecca.setText(""+dataSet.get(listPosition).getPlace()+"-"+dataSet.get(listPosition).getBranchname());
            holder.mosquename.setText(""+dataSet.get(listPosition).getYourwishplace());
            holder.subtotalcarton2.setText(""+dataSet.get(listPosition).getPname()+"-"+dataSet.get(listPosition).getCartoncount()+" "+ dashboardActivity.getString(R.string.pack));
            holder.receivername.setText(""+dataSet.get(listPosition).getReceivername());
            holder.receivermno.setText(""+dataSet.get(listPosition).getReceivermno());
            Log.d("wfscvbd","ert"+dataSet.get(listPosition).getReceivermno());
            if(dataSet.get(listPosition).getReceivermno().equals("0") || dataSet.get(listPosition).getReceivermno().equals("null")){
                Log.d("wfscvbd","ert"+dataSet.get(listPosition).getReceivermno());
                holder.receivermno.setText("");
            }
            Log.d("ghj","photo1");
            holder.linearimg1.setVisibility(View.VISIBLE);
            holder.linearimg2.setVisibility(View.VISIBLE);
            holder.linearimg3.setVisibility(View.VISIBLE);
            holder.linearimg4.setVisibility(View.VISIBLE);
            if(!dataSet.get(listPosition).getImgpath1().isEmpty()){
                Picasso.get().load(dataSet.get(listPosition).getImgpath1()).placeholder(R.drawable.defaultimg).error(R.drawable.ic_baseline_error_24).into(holder.imageView1);
            }
            if(!dataSet.get(listPosition).getImgpath2().isEmpty()){
                Picasso.get().load(dataSet.get(listPosition).getImgpath2()).placeholder(R.drawable.defaultimg).into(holder.imageView2);
            }
            if(!dataSet.get(listPosition).getImgpath3().isEmpty()){
                Picasso.get().load(dataSet.get(listPosition).getImgpath3()).placeholder(R.drawable.defaultimg).into(holder.imageView3);
            }
            if(!dataSet.get(listPosition).getImgpath4().isEmpty()){
                Picasso.get().load(dataSet.get(listPosition).getImgpath4()).placeholder(R.drawable.defaultimg).into(holder.imageView4);
            }
        }
        holder.subcard.setVisibility(View.GONE);
        holder.linear1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                holder.linear1.setVisibility(View.GONE);
                holder.linear2.setVisibility(View.VISIBLE);
                holder.subcard.setVisibility(View.VISIBLE);
            }
        });
        holder.minimize.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                holder.linear2.setVisibility(View.GONE);
                holder.linear1.setVisibility(View.VISIBLE);
                holder.subcard.setVisibility(View.GONE);
            }
        });

        holder.imageView1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!dataSet.get(listPosition).getImgpath1().isEmpty()) {
                    openimage(dataSet.get(listPosition).getImgpath1());
                }
            }
        });
        holder.imageView2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!dataSet.get(listPosition).getImgpath2().isEmpty()) {
                    openimage(dataSet.get(listPosition).getImgpath2());
                }
            }
        });
        holder.imageView3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!dataSet.get(listPosition).getImgpath3().isEmpty()) {
                    openimage(dataSet.get(listPosition).getImgpath3());
                }
            }
        });
        holder.imageView4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!dataSet.get(listPosition).getImgpath4().isEmpty()) {
                    openimage(dataSet.get(listPosition).getImgpath4());
                }
            }
        });
    }


    public void openimage(String url){
        final Dialog nagDialog = new Dialog(dashboardActivity);
        nagDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        nagDialog.setCancelable(true);
        nagDialog.setContentView(R.layout.preview_image);
        WindowManager.LayoutParams layoutParams = new WindowManager.LayoutParams();
        layoutParams.copyFrom(nagDialog.getWindow().getAttributes());
        layoutParams.width =  WindowManager.LayoutParams.MATCH_PARENT;
        layoutParams.height =  WindowManager.LayoutParams.FIRST_SUB_WINDOW;
        //layoutParams.height = 700;
        //nagDialog.getWindow().setBackgroundDrawable(null);
        nagDialog.getWindow().setAttributes(layoutParams);
        ImageView btnClose = (ImageView)nagDialog.findViewById(R.id.btnIvClose);
        ImageView ivPreview = (ImageView)nagDialog.findViewById(R.id.iv_preview_image);
        //ivPreview.setBackgroundDrawable(dd);
        Picasso.get().load(url).placeholder(R.drawable.defaultimg).error(R.drawable.ic_baseline_error_24).into(ivPreview);
        btnClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                nagDialog.dismiss();
            }
        });
        nagDialog.show();
    }

    @Override
    public int getItemCount() {
        Log.d(TAG, "getItemCount: "+dataSet.size());
        return dataSet.size();
    }
}