package com.lia.rohadriver;

import org.json.JSONArray;

class HomeCompletedDataModel {
    String totalprice,order_for,deliverydate,customer_name,customer_mno,payment_type,order_date,day_id,time_from,time_to,session,branch_name,address;
    int total_cartcount,order_id;
    double lat,lng;
    JSONArray details;

    public HomeCompletedDataModel(String deliverydate,String order_for, int order_id, String customer_name, String customer_mno, String payment_type, int total_cartcount, String totalprice, String order_date, String day_id, String time_from, String time_to, String session, String branch_name, String address, double lat, double lng, JSONArray details) {
        this.order_for = order_for;
        this.deliverydate = deliverydate;
        this.order_id = order_id;
        this.customer_name = customer_name;
        this.customer_mno = customer_mno;
        this.payment_type = payment_type;
        this.order_date = order_date;
        this.day_id = day_id;
        this.time_from = time_from;
        this.time_to = time_to;
        this.session = session;
        this.branch_name = branch_name;
        this.address = address;
        this.total_cartcount = total_cartcount;
        this.totalprice = totalprice;
        this.lat = lat;
        this.lng = lng;
        this.details = details;
    }

    public String getDeliverydate(){
        return deliverydate;
    }
    public String getOrder_for(){
        return order_for;
    }

    public String getCustomer_name(){
        return customer_name;
    }

    public String getCustomer_mno(){
        return customer_mno;
    }

    public String getPayment_type(){
        return payment_type;
    }

    public String getOrder_date(){
        return order_date;
    }

    public String getDay_id(){
        return day_id;
    }

    public String getTime_from(){
        return time_from;
    }

    public String getTime_to(){
        return time_to;
    }

    public String getSession(){
        return session;
    }

    public String getBranch_name(){
        return branch_name;
    }

    public String getAddress(){
        return address;
    }

    public String getTotalprice(){
        return totalprice;
    }

    public double getLat(){
        return lat;
    }

    public double getLng(){
        return lng;
    }

    public int getTotal_cartcount(){
        return total_cartcount;
    }

    public int getOrder_id(){
        return order_id;
    }

    public JSONArray getDetails(){
        return details;
    }
}
