package com.lia.rohadriver;

import static android.content.ContentValues.TAG;

import androidx.activity.result.ActivityResultLauncher;
import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.cardview.widget.CardView;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.content.FileProvider;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.Manifest;
import android.content.ContentResolver;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.ColorStateList;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.webkit.MimeTypeMap;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.material.navigation.NavigationView;
import com.google.android.material.snackbar.BaseTransientBottomBar;
import com.google.android.material.snackbar.Snackbar;

import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class DashboardActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener, View.OnClickListener, SwipeRefreshLayout.OnRefreshListener {
    RecyclerView.Adapter adapter1, adapter2, adapter3, adapter4;
    RecyclerView.LayoutManager layoutManager;
    RecyclerView recyclerwater, recyclerhome, recyclerwatercompleted, recyclerhomecompleted;
    ArrayList<WaterInprogressDataModel> data;
    ArrayList<HomeInProgressDataModel> data3;
    ArrayList<WaterCompletedDataModel> data1;
    ArrayList<HomeCompletedDataModel> data4;
    public TextView uname, umailid, umobileno;
    public int CAMERA = 2;
    private TextView version;
    private LinearLayout inprogress, completed;
    private Button  txt_completed, txt_inprogress;
    TextView inprogress_txt, completed_txt;
    String currentPhotoPath = null;
    File currentPhotoFile = null;
    Uri currentPhotoPathFileUri = null;
    private Bitmap bitmap;
    public Session session;
    public ImageView img;
    public NavigationView navigationView;
    public View headerview;
    public LinearLayout navheader;
    int profilecount = 2;
    TextView update_profile_txt,order_count,cartoon_count;

    private static final int REQUEST_PERMISSIONS = 100;
    private static final int PICK_IMAGE_REQUEST = 1;
    public int orderid = 0;
    public String imgvalue = "";
    ImageView nav_close;
    TextView editprofile;
    SwipeRefreshLayout mSwipeRefreshLayout;
    VolleyMultipartRequest.DataPart imagedata;
    View view;
    ActivityResultLauncher<Intent> someActivityResultLauncher;
    int orderCount,cartonCount;
    CardView progresscard;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // Fresco.initialize(this);
        setContentView(R.layout.activity_dashboard);
        view = (DrawerLayout) findViewById(R.id.drawer_layout);
        session = new Session(this, this, view);
        if (!session.isNetworkEnabled() == true) {
            session.noNetworkDialog(this);
        } else {
            if (session.isPermissionsEnabled() == false) {
                session.permissionsEnableRequest();
            } else if (session.isVersion() == true) {
                session.versiondialog();
            }
        }
//
//        mSwipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipeToRefresh);
//        mSwipeRefreshLayout.setColorSchemeResources(R.color.yellow_update);
//        mSwipeRefreshLayout.setOnRefreshListener(this);
//        mSwipeRefreshLayout.setEnabled(false);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        //toolbar.setLogo(R.drawable.ic_burger);
        toolbar.setTitle("");
        toolbar.setSubtitle("");

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
        toolbar.setNavigationIcon(R.drawable.navigation_icon);

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        //navigationView.setCheckedItem(R.id.nav_home);
        navheader();
        headerview = navigationView.getHeaderView(0);
        navheader = (LinearLayout) headerview.findViewById(R.id.nav_header);

        nav_close = (ImageView) navheader.findViewById(R.id.nav_close);
        nav_close.setOnClickListener(this);
        editprofile = (TextView) navheader.findViewById(R.id.edit_profile_txt);
        editprofile.setOnClickListener(this);
        update_profile_txt = (TextView) navheader.findViewById(R.id.update_profile_txt);
        update_profile_txt.setOnClickListener(this);
        order_count =findViewById(R.id.order_count);
        cartoon_count =findViewById(R.id.cartoon_count);


        version = (TextView) findViewById(R.id.version);
        version.setOnClickListener(this);
        // version.setText("Version : "+ BuildConfig.VERSION_NAME);
        try {
            version.setText("" + getString(R.string.version) + " " + getPackageManager().getPackageInfo(getPackageName(), 0).versionName);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }

        TextView uname = (TextView) navheader.findViewById(R.id.uname);
        TextView umailid = (TextView) navheader.findViewById(R.id.umailid);
        TextView umobileno = (TextView) navheader.findViewById(R.id.umobileno);
        uname.setText(session.getusername());
        umailid.setText(session.getusermail());
        umobileno.setText(session.getuserphoneno());

        inprogress_txt = (TextView) findViewById(R.id.inprogress_txt);
        completed_txt = (TextView) findViewById(R.id.complete_txt);

        txt_inprogress =  findViewById(R.id.txt_inprogress);
        txt_inprogress.setOnClickListener(this);

        txt_completed = findViewById(R.id.txt_completed);
        txt_completed.setOnClickListener(this);
        recyclerwater = (RecyclerView) findViewById(R.id.recycler_water);
        recyclerwater.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(this);
        recyclerwater.setLayoutManager(layoutManager);
        recyclerwater.setItemAnimator(new DefaultItemAnimator());

        recyclerhome = (RecyclerView) findViewById(R.id.recycler_home);
        recyclerhome.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(this);
        recyclerhome.setLayoutManager(layoutManager);
        recyclerhome.setItemAnimator(new DefaultItemAnimator());

        recyclerwatercompleted = (RecyclerView) findViewById(R.id.recycler_water_completed);
        recyclerwatercompleted.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(this);
        recyclerwatercompleted.setLayoutManager(layoutManager);
        recyclerwatercompleted.setItemAnimator(new DefaultItemAnimator());

        recyclerhomecompleted = (RecyclerView) findViewById(R.id.recycler_home_completed);
        recyclerhomecompleted.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(this);
        recyclerhomecompleted.setLayoutManager(layoutManager);
        recyclerhomecompleted.setItemAnimator(new DefaultItemAnimator());

        inprogress = (LinearLayout) findViewById(R.id.inprogress);

        recyclerwater.setOnTouchListener(new OnSwipeTouchListener(DashboardActivity.this) {
            @Override
            public void onSwipeLeft() {
                super.onSwipeLeft();
                completed_txt.setTypeface(null, Typeface.BOLD);
                inprogress_txt.setTypeface(null, Typeface.NORMAL);
                inprogress = (LinearLayout) findViewById(R.id.inprogress);
                completed = (LinearLayout) findViewById(R.id.completed);
                inprogress.setVisibility(View.GONE);
                completed.setVisibility(View.VISIBLE);
                View view_inprogress = (View) findViewById(R.id.view_inprogress);
                view_inprogress.setVisibility(View.VISIBLE);
                View view_completed = (View) findViewById(R.id.view_completed);
                view_completed.setVisibility(View.VISIBLE);
                txt_inprogress.setBackgroundTintList(ColorStateList.valueOf(getResources().getColor(R.color.newgrey)));
                txt_inprogress.setTextColor(getColor(R.color.white));
                txt_completed.setBackgroundTintList(ColorStateList.valueOf(getResources().getColor(R.color.newyellow)));
                txt_completed.setTextColor(getColor(R.color.newgrey));
            }

            @Override
            public void onSwipeRight() {
                super.onSwipeRight();
            }
        });

        completed = (LinearLayout) findViewById(R.id.completed);
        recyclerwatercompleted.setOnTouchListener(new OnSwipeTouchListener(DashboardActivity.this) {
            @Override
            public void onSwipeLeft() {
                super.onSwipeLeft();
            }

            @Override
            public void onSwipeRight() {
                super.onSwipeRight();
                completed_txt.setTypeface(null, Typeface.NORMAL);
                inprogress_txt.setTypeface(null, Typeface.BOLD);
                inprogress = (LinearLayout) findViewById(R.id.inprogress);
                completed = (LinearLayout) findViewById(R.id.completed);
                inprogress.setVisibility(View.VISIBLE);
                completed.setVisibility(View.GONE);
                View view_inprogress = (View) findViewById(R.id.view_inprogress);
                view_inprogress.setVisibility(View.VISIBLE);
                View view_completed = (View) findViewById(R.id.view_completed);
                view_completed.setVisibility(View.GONE);
                txt_completed.setBackgroundTintList(ColorStateList.valueOf(getResources().getColor(R.color.newgrey)));
                txt_completed.setTextColor(getColor(R.color.white));
                txt_inprogress.setBackgroundTintList(ColorStateList.valueOf(getResources().getColor(R.color.newyellow)));
                txt_inprogress.setTextColor(getColor(R.color.newgrey));
            }
        });

        if ((ContextCompat.checkSelfPermission(getApplicationContext(),
                Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) && (ContextCompat.checkSelfPermission(getApplicationContext(),
                Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED)) {
            if ((ActivityCompat.shouldShowRequestPermissionRationale(DashboardActivity.this,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE)) && (ActivityCompat.shouldShowRequestPermissionRationale(DashboardActivity.this,
                    Manifest.permission.READ_EXTERNAL_STORAGE))) {

            } else {
                ActivityCompat.requestPermissions(DashboardActivity.this,
                        new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE},
                        REQUEST_PERMISSIONS);
            }
        } else {
            Log.e("camera", "granted");
        }





        JSONObject requestbody1 = new JSONObject();
        try {
            requestbody1.put("id", session.getUserId());
            requestbody1.put("lat", session.getlat());
            requestbody1.put("lng", session.getlng());
            assignedorderrequestJSON(requestbody1);
            session.progressdialogshow();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        JSONObject requestbody2 = new JSONObject();
        try {
            requestbody2.put("id", session.getUserId());
            completedorderrequestJSON(requestbody2);
            session.progressdialogshow();
        } catch (JSONException e) {
            e.printStackTrace();
        }
       /* someActivityResultLauncher = registerForActivityResult(
                new ActivityResultContracts.StartActivityForResult(),
                new ActivityResultCallback<ActivityResult>() {
                    @Override
                    public void onActivityResult(ActivityResult result) {
                        Log.d("jgvdv",""+result.getResultCode()+"dv"+result.getData());
                        if (result.getResultCode() == Activity.RESULT_OK) {
                            // There are no request codes
                            if(result.getData() != null){
                                // Get Extra from the intent
                                Bundle extras = result.getData().getExtras();
                                // Get the returned image from extra
                                bitmap = (Bitmap) extras.get("data");
                                img.setImageBitmap(bitmap);
                                long imagename = System.currentTimeMillis();
                                imagedata = new VolleyMultipartRequest.DataPart(imagename + ".jpg", getFileDataFromDrawable(bitmap));
                                if (imagedata != null) {
                                    session.progressdialogshow();
                                    uploadBitmap(img, bitmap);
                                }
                            }
                            else {
                                Intent data = result.getData();
                                String filePath = getImageFilePath(data);
                                if (filePath != null) {
                                    Log.d("filePath", String.valueOf(filePath));
                                    // Decode it for real
                                    BitmapFactory.Options bmpFactoryOptions = new BitmapFactory.Options();
                                    bmpFactoryOptions.inJustDecodeBounds = false;
                                    bitmap = BitmapFactory.decodeFile(filePath,bmpFactoryOptions);
                                    //bitmap = BitmapFactory.decodeFile(filePath);
                                    img.setImageBitmap(bitmap);
                                    imagedata = new VolleyMultipartRequest.DataPart(System.currentTimeMillis()+".jpg", getFileDataFromDrawable(bitmap));
                                    if(imagedata != null){
                                        session.progressdialogshow();
                                        uploadBitmap(img,bitmap);
                                    }
                                }
                            }
                        }
                    }
                });*/
    }

    private void assignedorderrequestJSON(JSONObject response) {
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        final String requestBody = response.toString();
        Log.d("request234", String.valueOf(response));
        StringRequest stringRequest = new StringRequest(Request.Method.POST, StaticInfo.assigned_orders, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d("assignedorderresponse", ">>" + response);
                cartonCount = 0;
                session.progressdialogdismiss();
                try {
                    JSONObject obj = new JSONObject(response);
                    if (obj.getBoolean("error") == false) {
                        data = new ArrayList<WaterInprogressDataModel>();
                        data3 = new ArrayList<HomeInProgressDataModel>();
                        JSONObject json = obj.getJSONObject("data");
                        JSONArray obj1 = new JSONArray(json.getString("water"));
                        JSONArray obj2 = new JSONArray(json.getString("home"));
                        orderCount = obj1.length() + obj2.length();
//
                        for (int i = 0; i < obj1.length(); i++) {
                            JSONObject jsonObject = obj1.getJSONObject(i);
                            WaterInprogressDataModel datamodel = new WaterInprogressDataModel(jsonObject.getString("branch_name"), jsonObject.getString("yourwish_spec_place"), jsonObject.getString("receiverdesc"), jsonObject.getString("product_name"), jsonObject.getString("customer_name"), jsonObject.getString("customer_mno"), jsonObject.getString("countrycode"), jsonObject.getString("amount"), jsonObject.getString("payment_type"), jsonObject.getString("orderFor"), Integer.parseInt(jsonObject.getString("Id")), Integer.parseInt(jsonObject.getString("order_id")), Double.parseDouble(jsonObject.getString("latitude")), Double.parseDouble(jsonObject.getString("lng")), Integer.parseInt(jsonObject.getString("cartcount")), jsonObject.getString("order_date"), Integer.parseInt(jsonObject.getString("ismecca")), Integer.parseInt(jsonObject.getString("iswish")), jsonObject.getString("place"), jsonObject.getString("mosquename"), jsonObject.getString("receivername"), jsonObject.getString("receivermno"), jsonObject.getString("image_path1"), jsonObject.getString("image_path2"), jsonObject.getString("image_path3"), jsonObject.getString("image_path4"),jsonObject.getDouble("distance"),jsonObject.getString("product_name"),String.valueOf(jsonObject));
                            data.add(datamodel);
                            cartonCount += Integer.parseInt(jsonObject.getString("cartcount"));
                        }
                        for (int j = 0; j < obj2.length(); j++) {
                            JSONObject jsonObject2 = obj2.getJSONObject(j);
                            HomeInProgressDataModel dataModel = new HomeInProgressDataModel(jsonObject2.getString("order_for"), jsonObject2.getInt("order_id"), jsonObject2.getString("customer_name"), jsonObject2.getString("customer_mno"), jsonObject2.getString("countrycode"), jsonObject2.getString("payment_type"), jsonObject2.getInt("total_cartcount"), jsonObject2.getString("totalprice"), jsonObject2.getString("order_date"), jsonObject2.getString("day_id"), jsonObject2.getString("time_from"), jsonObject2.getString("time_to"), jsonObject2.getString("session"), jsonObject2.getString("branch_name"), jsonObject2.getString("address"), jsonObject2.getDouble("lat"), jsonObject2.getDouble("lng"), jsonObject2.getJSONArray("details"));
                            data3.add(dataModel);
                            cartonCount += jsonObject2.getInt("total_cartcount");

                            //  jsonObject.getInt("product_id"),jsonObject.getString("product_name"),jsonObject.getString("product_image"),jsonObject.getInt("product_quantity"),jsonObject.getDouble("product_price")
                        }
                        order_count.setText(""+orderCount);
                        cartoon_count.setText(""+cartonCount);
                        adapter1 = new WaterInprogressAdapter(getApplicationContext(), data, DashboardActivity.this);
                        recyclerwater.setAdapter(adapter1);
                        adapter3 = new HomeInprogressAdapter(getApplicationContext(), data3, DashboardActivity.this);
                        recyclerhome.setAdapter(adapter3);
                    } else {

                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                session.progressdialogdismiss();
                session.volleyerror(error);
                Log.e("VOLLEY", error.toString());
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                int id = session.getUserId();
                Map<String, String> params = new HashMap<String, String>();
                //params.put("Content-Type","application/json" );
                params.put("Authorization", "Bearer " + session.gettoken());
                Log.d("param", "" + params);
                Log.d("id", "" + id);
                return params;
            }

            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }

            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return requestBody == null ? null : requestBody.getBytes("utf-8");
                } catch (UnsupportedEncodingException uee) {
                    VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                    return null;
                }
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(10000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        Log.d("asdsdjkxdk", "" + stringRequest);
        requestQueue.add(stringRequest);

    }

    private void completedorderrequestJSON(JSONObject response) {
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        final String requestBody = response.toString();
        Log.d("request234", String.valueOf(response));
        StringRequest stringRequest = new StringRequest(Request.Method.POST, StaticInfo.completedorders, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d("completedorderresponse", ">>" + response);
                session.progressdialogdismiss();
                try {
                    JSONObject obj = new JSONObject(response);
                    if (obj.getBoolean("error") == false) {
                        data1 = new ArrayList<WaterCompletedDataModel>();
                        data4 = new ArrayList<HomeCompletedDataModel>();
                        JSONObject json = obj.getJSONObject("data");
                        JSONArray obj1 = new JSONArray(json.getString("water"));
                        JSONArray obj2 = new JSONArray(json.getString("home"));
                        for (int i = 0; i < obj1.length(); i++) {
                            JSONObject jsonObject = obj1.getJSONObject(i);
                            WaterCompletedDataModel datamodel = new WaterCompletedDataModel(jsonObject.getString("delivered_date"), jsonObject.getString("branch_name"), jsonObject.getString("yourwish_spec_place"), jsonObject.getString("receiverdesc"), jsonObject.getString("product_name"), jsonObject.getString("customer_name"), jsonObject.getString("customer_mno"), jsonObject.getString("amount"), jsonObject.getString("payment_type"), jsonObject.getString("orderFor"), Integer.parseInt(jsonObject.getString("Id")), Integer.parseInt(jsonObject.getString("order_id")), Double.parseDouble(jsonObject.getString("latitude")), Double.parseDouble(jsonObject.getString("lng")), Integer.parseInt(jsonObject.getString("cartcount")), jsonObject.getString("order_date"), Integer.parseInt(jsonObject.getString("ismecca")), Integer.parseInt(jsonObject.getString("iswish")), jsonObject.getString("place"), jsonObject.getString("mosquename"), jsonObject.getString("receivername"), jsonObject.getString("receivermno"), jsonObject.getString("image_path1"), jsonObject.getString("image_path2"), jsonObject.getString("image_path3"), jsonObject.getString("image_path4"),jsonObject.getString("product_name"));
                            data1.add(datamodel);
                        }
                        for (int j = 0; j < obj2.length(); j++) {
                            JSONObject jsonObject2 = obj2.getJSONObject(j);
                            HomeCompletedDataModel dataModel = new HomeCompletedDataModel(jsonObject2.getString("delivered_date"), jsonObject2.getString("order_for"), jsonObject2.getInt("order_id"), jsonObject2.getString("customer_name"), jsonObject2.getString("customer_mno"), jsonObject2.getString("payment_type"), jsonObject2.getInt("total_cartcount"), jsonObject2.getString("totalprice"), jsonObject2.getString("order_date"), jsonObject2.getString("day_id"), jsonObject2.getString("time_from"), jsonObject2.getString("time_to"), jsonObject2.getString("session"), jsonObject2.getString("branch_name"), jsonObject2.getString("address"), jsonObject2.getDouble("lat"), jsonObject2.getDouble("lng"), jsonObject2.getJSONArray("details"));
                            data4.add(dataModel);
                            //  jsonObject.getInt("product_id"),jsonObject.getString("product_name"),jsonObject.getString("product_image"),jsonObject.getInt("product_quantity"),jsonObject.getDouble("product_price")
                        }
                        Log.d(TAG, "onResponseco: "+  data1.size());
                        adapter2 = new WaterCompletedAdapter(getApplicationContext(), data1, DashboardActivity.this);
                        recyclerwatercompleted.setAdapter(adapter2);
                        adapter4 = new HomeCompletedAdapter(getApplicationContext(), data4, DashboardActivity.this);
                        recyclerhomecompleted.setAdapter(adapter4);
                    } else {

                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                session.progressdialogdismiss();
                session.volleyerror(error);
                Log.e("VOLLEY", error.toString());
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                int id = session.getUserId();
                Map<String, String> params = new HashMap<String, String>();
                //params.put("Content-Type","application/json" );
                params.put("Authorization", "Bearer " + session.gettoken());
                Log.d("param", "" + params);
                Log.d("id", "" + id);
                return params;
            }

            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }

            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return requestBody == null ? null : requestBody.getBytes("utf-8");
                } catch (UnsupportedEncodingException uee) {
                    VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                    return null;
                }
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(10000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        Log.d("asdsdjkxdk", "" + stringRequest);
        requestQueue.add(stringRequest);

    }

    public void navheader() {
        navigationView = (NavigationView) findViewById(R.id.nav_view);
        headerview = navigationView.getHeaderView(0);
        navheader = (LinearLayout) headerview.findViewById(R.id.nav_header);
        //uname = (TextView) navheader.findViewById(R.id.uname);
        //umailid = (TextView) navheader.findViewById(R.id.umailid);
        //umobileno = (TextView) navheader.findViewById(R.id.umobileno);
        JSONObject requestbody = new JSONObject();
        try {
            requestbody.put("id", session.getUserId());
            profileviewrequestJSON(requestbody);
            session.progressdialogshow();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void profileviewrequestJSON(JSONObject response) {
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        final String requestBody = response.toString();
        Log.d("reques34", String.valueOf(response));
        StringRequest stringRequest = new StringRequest(Request.Method.POST, StaticInfo.viewprofile, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                session.progressdialogdismiss();
                Log.d("viewprofileresponse", ">>" + response);
                try {
                    JSONObject obj = new JSONObject(response);
                    if (obj.getBoolean("error") == true) {
                        String msg = obj.getString("message");
                        Log.d("loginerror", "" + msg);
                    } else {
                        uname = (TextView) navheader.findViewById(R.id.uname);
                        umailid = (TextView) navheader.findViewById(R.id.umailid);
                        umobileno = (TextView) navheader.findViewById(R.id.umobileno);
                        String msg = obj.getString("data");
                        JSONObject tok = new JSONObject(msg);
                        session.setusername(tok.getString("name"));
                        session.setusermail(tok.getString("email"));
                        session.setuserphoneno(tok.getString("phone"));
                        session.setDeltotcarton(tok.getInt("total_soldcarton"));
                        session.setRewardedcarton(tok.getInt("settled_carton"));
                        session.setGalaccess(tok.getString("gallery_access"));
                        uname.setText(session.getusername());
                        umailid.setText(session.getusermail());
                        umobileno.setText(session.getuserphoneno());
                        Log.d(TAG, "onResponse: "+tok.getString("gallery_access"));
                        /*Snackbar snackbar;
                        Snackbar.make(mapView, "Profile Updated", Snackbar.LENGTH_SHORT)
                                .setTextColor(ContextCompat.getColor(getApplicationContext(),R.color.design_default_color_error))
                                .setBackgroundTint(ContextCompat.getColor(getApplicationContext(),R.color.white))
                                .setDuration(BaseTransientBottomBar.LENGTH_LONG)
                                .setAnimationMode(BaseTransientBottomBar.ANIMATION_MODE_SLIDE)
                                .show();*/
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                session.progressdialogdismiss();
                session.volleyerror(error);
                Log.e("VOLLEY", error.toString());
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                int id = session.getUserId();
                Map<String, String> params = new HashMap<String, String>();
                //params.put("Content-Type","application/json" );
                params.put("Authorization", "Bearer " + session.gettoken());
                Log.d("param", "" + params);
                Log.d("id", "" + id);
                return params;
            }

            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }

            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return requestBody == null ? null : requestBody.getBytes("utf-8");
                } catch (UnsupportedEncodingException uee) {
                    VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                    return null;
                }
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(10000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        Log.d("asdsdjkxdk", "" + stringRequest);
        requestQueue.add(stringRequest);
    }

    private void profileupdaterequestJSON(JSONObject response) {
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        final String requestBody = response.toString();
        Log.d("reques234", String.valueOf(response));
        StringRequest stringRequest = new StringRequest(Request.Method.POST, StaticInfo.updateprofile, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d("updateprofileresponse", ">>" + response);
                try {
                    JSONObject obj = new JSONObject(response);
                    if (obj.getBoolean("error") == true) {
                        String msg = obj.getString("message");
                        Log.d("loginerror", "" + msg);
                    } else {
                        navheader();
                        Snackbar snackbar;
                        View view = findViewById(R.id.update_profile_txt);
                        Snackbar.make(view, R.string.profileupdated, Snackbar.LENGTH_SHORT)
                                .setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.design_default_color_error))
                                .setBackgroundTint(ContextCompat.getColor(getApplicationContext(), R.color.white))
                                .setDuration(BaseTransientBottomBar.LENGTH_LONG)
                                .setAnimationMode(BaseTransientBottomBar.ANIMATION_MODE_SLIDE)
                                .show();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("VOLLEY", error.toString());
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                int id = session.getUserId();
                Map<String, String> params = new HashMap<String, String>();
                //params.put("Content-Type","application/json" );
                params.put("Authorization", "Bearer " + session.gettoken());
                Log.d("param", "" + params);
                Log.d("id", "" + id);
                return params;
            }

            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }

            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return requestBody == null ? null : requestBody.getBytes("utf-8");
                } catch (UnsupportedEncodingException uee) {
                    VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                    return null;
                }
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(10000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        Log.d("asdsdjkxdk", "" + stringRequest);
        requestQueue.add(stringRequest);
    }


    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            //super.onBackPressed();
           /* exitcount = exitcount+1;
            if(exitcount==1) {
                Toast.makeText(this, "Press again back button to exit", Toast.LENGTH_SHORT).show();
            }
            else if(exitcount>=2){
                exitcount=0;
                Intent intent = new Intent(Intent.ACTION_MAIN);
                intent.addCategory(Intent.CATEGORY_HOME);
                startActivity(intent);
            }*/
            finish();
            Intent intent = new Intent(DashboardActivity.this, MapActivity.class);
            startActivity(intent);
        }
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        if (!session.isNetworkEnabled() == true) {
            session.noNetworkDialog(this);
        } else {
            int id = item.getItemId();
            //Fragment fragment = null;
            switch (id) {
                case R.id.home_nav:
                    Intent intent3 = new Intent(DashboardActivity.this, DashboardActivity.class);
                    startActivity(intent3);
                    break;
                case R.id.reset:
                    Intent intent1 = new Intent(DashboardActivity.this, ResetPasswordActivity.class);
                    startActivity(intent1);
                    break;
                case R.id.more:

                    break;
                case R.id.language:
                    Intent n5 = new Intent(DashboardActivity.this, LanguageActivity.class);
                    startActivity(n5);
                    break;
                case R.id.earnings:
                    Log.d(TAG, "onNavigationItemSelected: clickedearnis");
                    Intent eintent = new Intent(DashboardActivity.this, EarningsActivity.class);
                    startActivity(eintent);
                    break;
                case R.id.signoff:
                    /*FirebaseAuth.getInstance().signOut();
                     */
                    session.settoken("");
                    session.setusername("");
                    session.setusermail("");
                    session.setUserId(0);
                    session.setuserphoneno("");
                    Intent intent = new Intent(DashboardActivity.this, LoginActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent);
                    break;
                default:
                    break;
            }
        }
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return false;
    }

    @Override
    public void onClick(View view) {
        if (!session.isNetworkEnabled() == true) {
            session.noNetworkDialog(this);
        } else {
            if (view == txt_inprogress) {
                JSONObject requestbody1 = new JSONObject();
                try {
                    requestbody1.put("id", session.getUserId());
                    requestbody1.put("lat", session.getlat());
                    requestbody1.put("lng", session.getlng());
                    assignedorderrequestJSON(requestbody1);
                    session.progressdialogshow();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                completed_txt.setTypeface(null, Typeface.NORMAL);
                inprogress_txt.setTypeface(null, Typeface.BOLD);
                inprogress = (LinearLayout) findViewById(R.id.inprogress);
                inprogress.setVisibility(View.VISIBLE);
                completed = (LinearLayout) findViewById(R.id.completed);
                completed.setVisibility(View.GONE);
                View view_inprogress = (View) findViewById(R.id.view_inprogress);
                view_inprogress.setVisibility(View.VISIBLE);
                View view_completed = (View) findViewById(R.id.view_completed);
                view_completed.setVisibility(View.GONE);
                txt_completed.setBackgroundTintList(ColorStateList.valueOf(getResources().getColor(R.color.newgrey)));
                txt_completed.setTextColor(getColor(R.color.white));
                txt_inprogress.setBackgroundTintList(ColorStateList.valueOf(getResources().getColor(R.color.newyellow)));
                txt_inprogress.setTextColor(getColor(R.color.newgrey));
            } else if (view == txt_completed) {
                completed_txt.setTypeface(null, Typeface.BOLD);
                inprogress_txt.setTypeface(null, Typeface.NORMAL);
                completed = (LinearLayout) findViewById(R.id.completed);
                completed.setVisibility(View.VISIBLE);
                inprogress = (LinearLayout) findViewById(R.id.inprogress);
                inprogress.setVisibility(View.GONE);
                View view_inprogress = (View) findViewById(R.id.view_inprogress);
                view_inprogress.setVisibility(View.VISIBLE);
                View view_completed = (View) findViewById(R.id.view_completed);
                view_completed.setVisibility(View.VISIBLE);
                txt_inprogress.setBackgroundTintList(ColorStateList.valueOf(getResources().getColor(R.color.newgrey)));
                txt_inprogress.setTextColor(getColor(R.color.white));
                txt_completed.setBackgroundTintList(ColorStateList.valueOf(getResources().getColor(R.color.newyellow)));
                txt_completed.setTextColor(getColor(R.color.newgrey));
            } else if (view == editprofile) {
                EditText uname = (EditText) navheader.findViewById(R.id.username);
                EditText umobile = (EditText) navheader.findViewById(R.id.mobile);
                uname.setText(session.getusername());
                umobile.setText(session.getuserphoneno());
                LinearLayout profile = (LinearLayout) navheader.findViewById(R.id.profile);
                LinearLayout profile_update = (LinearLayout) navheader.findViewById(R.id.profile_update);
                if (profilecount % 2 == 0) {
                    profile.setVisibility(View.GONE);
                    profile_update.setVisibility(View.VISIBLE);
                    profilecount++;
                } else {
                    profile.setVisibility(View.VISIBLE);
                    profile_update.setVisibility(View.GONE);
                    profilecount++;
                }
            } else if (view == update_profile_txt) {
                EditText uname = (EditText) navheader.findViewById(R.id.username);
                EditText umobile = (EditText) navheader.findViewById(R.id.mobile);
                String unameStr = String.valueOf(uname.getText().toString());
                String mobileStr = String.valueOf(umobile.getText().toString());
                Log.d("sd4dfg", "" + unameStr);
                if (!unameStr.equals("") && !mobileStr.equals("")) {
                    JSONObject requestbody = new JSONObject();
                    try {
                        requestbody.put("id", session.getUserId());
                        requestbody.put("name", unameStr);
                        requestbody.put("email", session.getusermail());
                        requestbody.put("phone", mobileStr);
                        profileupdaterequestJSON(requestbody);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    LinearLayout profile = (LinearLayout) navheader.findViewById(R.id.profile);
                    LinearLayout profile_update = (LinearLayout) navheader.findViewById(R.id.profile_update);
                    profile_update.setVisibility(View.GONE);
                    profile.setVisibility(View.VISIBLE);
                } else {
                    Snackbar snackbar;
                    Snackbar.make(update_profile_txt, R.string.enterallfields, Snackbar.LENGTH_SHORT)
                            .setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.design_default_color_error))
                            .setBackgroundTint(ContextCompat.getColor(getApplicationContext(), R.color.white))
                            .setDuration(BaseTransientBottomBar.LENGTH_LONG)
                            .setAnimationMode(BaseTransientBottomBar.ANIMATION_MODE_SLIDE)
                            .show();
                }
            } else if (view == nav_close) {
                DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
                drawer.closeDrawer(GravityCompat.START);
            } else if (view == version) {
                session.version();
            }
        }
    }


    public void showPictureDialog(int norderid, String imagename, ImageView imageView) {
        orderid = norderid;
        imgvalue = imagename;
        img = imageView;
        AlertDialog.Builder pictureDialog = new AlertDialog.Builder(this);
        pictureDialog.setCancelable(true);
        pictureDialog.setTitle(getString(R.string.selectaction));
        pictureDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialogInterface) {
                imgvalue = "";
                orderid = 0;
            }
        });
        //String[] pictureDialogItems = {getString(R.string.photocamera),getString(R.string.photogallery)};
        String[] pictureDialogItems = {getString(R.string.capturephotofromcamera)};
        pictureDialog.setItems(pictureDialogItems,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        switch (which) {
                            case 0:
                                dialog.dismiss();
                                takePhotoFromCamera();
                                break;
                            case 1:
                                dialog.dismiss();
                                showFileChooser();
                                break;
                        }
                    }
                });
        pictureDialog.show();
    }

    private void showFileChooser() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_IMAGE_REQUEST);
    }

    private void takePhotoFromCamera() {
       /* Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
         intent.putExtra(MediaStore.EXTRA_OUTPUT, outputFileUri);
        someActivityResultLauncher.launch(intent);*/

        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (intent.resolveActivity(getPackageManager()) != null) {
            // Create the File where the photo should go
            Uri outputFileUri = null;
            try {
                outputFileUri = createImageFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
            // Continue only if the File was successfully created
            if (outputFileUri != null) {
                intent.putExtra(MediaStore.EXTRA_OUTPUT, outputFileUri);
                startActivityForResult(intent, CAMERA);

               /* try {
                    startActivityForResult(intent, CAMERA);
                } catch (ActivityNotFoundException e) {
                    // display error state to the user
                }*/
            }
        }

    }

    private Uri createImageFile() throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );

        // Save a file: path for use with ACTION_VIEW intents
        if (image != null) {
            currentPhotoFile = image;
            currentPhotoPath = image.getAbsolutePath();
            currentPhotoPathFileUri = FileProvider.getUriForFile(this,
                    "com.lia.rohadriver.android.fileprovider",
                    image);
        }
        return currentPhotoPathFileUri;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.d("requestcode", "" + requestCode + ".." + resultCode + ".." + data);
        if (requestCode == PICK_IMAGE_REQUEST && resultCode == RESULT_OK && data != null && data.getData() != null) {
            Uri contentUri = data.getData();
            String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
            String imageFilePath = "JPEG_" + timeStamp + "." + getFileExt(contentUri);
            String RealimageFilePath = getImageFilePath(data);
            img.setImageURI(contentUri);
            try {
                //bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), contentUri);
                byte[] byteArray = getBytes(getContentResolver().openInputStream(contentUri));
                bitmap = BitmapFactory.decodeByteArray(byteArray, 0, byteArray.length);
                imagedata = new VolleyMultipartRequest.DataPart(System.currentTimeMillis() + ".jpg", getFileDataFromDrawable(bitmap));
                if (imagedata != null) {
                    uploadBitmap(img);
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else if (requestCode == CAMERA && resultCode == RESULT_OK) {
           /* Bundle extras = data.getExtras();
            bitmap = (Bitmap) extras.get("data");*/

            String filePath = getImageFilePath(data);
            if (filePath != null) {
                Log.d("filePath", String.valueOf(filePath));
                // Decode it for real
                BitmapFactory.Options bmpFactoryOptions = new BitmapFactory.Options();
                bmpFactoryOptions.inJustDecodeBounds = false;
                //bitmap = BitmapFactory.decodeFile(filePath);
                //bitmap = BitmapFactory.decodeFile(filePath,bmpFactoryOptions);
                bitmap = compressBitMapImage1(filePath, 800, 600);
                //bitmap = BitmapFactory.decodeFile(compressBitMapImage3(filePath));
                //amazons3();
                try {
                   bitmap = imagerotate(filePath);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                //img.setImageBitmap(bitmap);
                imagedata = new VolleyMultipartRequest.DataPart(System.currentTimeMillis() + ".jpg", getFileDataFromDrawable(bitmap));
                if (imagedata != null) {
                    session.progressdialogshow();
                    uploadBitmap(img);
                }
            }
        }
    }

    private Bitmap imagerotate(String photoPath) throws IOException {
        ExifInterface ei = new ExifInterface(photoPath);
        int orientation = ei.getAttributeInt(ExifInterface.TAG_ORIENTATION,
                ExifInterface.ORIENTATION_UNDEFINED);

        Bitmap rotatedBitmap = null;
        switch (orientation) {

            case ExifInterface.ORIENTATION_ROTATE_90:
                rotatedBitmap = rotateImage(bitmap, 90);
                break;

            case ExifInterface.ORIENTATION_ROTATE_180:
                rotatedBitmap = rotateImage(bitmap, 180);
                break;

            case ExifInterface.ORIENTATION_ROTATE_270:
                rotatedBitmap = rotateImage(bitmap, 270);
                break;

            case ExifInterface.ORIENTATION_NORMAL:
            default:
                rotatedBitmap = bitmap;
        }
        return rotatedBitmap;
    }

    public static Bitmap rotateImage(Bitmap source, float angle) {
        Matrix matrix = new Matrix();
        matrix.postRotate(angle);
        return Bitmap.createBitmap(source, 0, 0, source.getWidth(), source.getHeight(),
                matrix, true);
    }

    private String getImageFilePath(Intent data) {
        boolean isCamera = data == null || data.getData() == null;
        if (isCamera) return currentPhotoPath;
        else return getPathFromURI(data.getData());
    }

    public String getPathFromURI(Uri contentUri) {
        String res = null;
        String[] proj = {MediaStore.Images.Media.DATA};
        Cursor cursor = getContentResolver().query(contentUri, proj, null, null, null);
        if (cursor.moveToFirst()) {
            int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            res = cursor.getString(column_index);
        }
        cursor.close();
        return res;
    }

    public byte[] getBytes(InputStream inputStream) throws IOException {
        ByteArrayOutputStream byteBuffer = new ByteArrayOutputStream();
        int bufferSize = 1024;
        byte[] buffer = new byte[bufferSize];

        int len = 0;
        while ((len = inputStream.read(buffer)) != -1) {
            byteBuffer.write(buffer, 0, len);
        }
        return byteBuffer.toByteArray();
    }

    private String getFileExt(Uri contentUri) {
        ContentResolver c = getContentResolver();
        MimeTypeMap mime = MimeTypeMap.getSingleton();
        return mime.getExtensionFromMimeType(c.getType(contentUri));
    }

    private void uploadBitmap(ImageView imageView) {
        VolleyMultipartRequest volleyMultipartRequest = new VolleyMultipartRequest(Request.Method.POST, StaticInfo.uploadimage,
                new Response.Listener<NetworkResponse>() {
                    @Override
                    public void onResponse(NetworkResponse response) {
                        session.progressdialogdismiss();
                        //imageView.setImageBitmap(bitmap);
                        try {
                           // Log.d("dfgdh12dsbbhj",""+response);
                           // Log.d("dfgdh12",""+response.data);
                           // Log.d("dfgdh",""+new String(response.data));
                            JSONObject obj = new JSONObject(new String(response.data));
                            Log.d("xjhjvhchx", "" + obj);
                            if(obj.getBoolean("error") == false){
                                session.snackbarToast(getString(R.string.photouploadsuccess), view);
                                JSONObject jsonObject = obj.getJSONObject("data");
                                Picasso.get().load(jsonObject.getString("url")).placeholder(R.drawable.defaultimg).error(R.drawable.ic_baseline_error_24).into(imageView);
                            }
                            else {
                                session.snackbarToast(obj.getString("message"), view);
                            }
                        } catch (JSONException e) {
                            session.snackbarToast("Backend Side Issue", view);
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        session.volleyerror(error);
                        session.progressdialogdismiss();
                    }
                }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                //params.put("Content-Type","application/json" );
                params.put("Authorization", "Bearer " + session.gettoken());
                Log.d("param", "" + params);
                return params;
            }

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("orderid", "" + orderid);
                Log.d("param order", "" + params);
                return params;
            }

            @Override
            protected Map<String, DataPart> getByteData() {
                Map<String, DataPart> params = new HashMap<>();
                if (imagedata != null) {
                    params.put(imgvalue, imagedata);
                }
                Log.d("Params", "" + params);
                return params;
            }
        };
        //adding the request to volley
        Volley.newRequestQueue(this).add(volleyMultipartRequest);
    }

    public byte[] getFileDataFromDrawable(Bitmap bitmap) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, byteArrayOutputStream);
        return byteArrayOutputStream.toByteArray();
    }

    @Override
    public void onRefresh() {
        Intent intent3 = new Intent(DashboardActivity.this, DashboardActivity.class);
        startActivity(intent3);
//        mSwipeRefreshLayout.setRefreshing(false);
    }


    public static Bitmap compressBitMapImage1(String filePath, int targetWidth, int targetHeight) {
        Bitmap bitMapImage = null;
        try {
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inJustDecodeBounds = true;
            BitmapFactory.decodeFile(filePath, options);
            double sampleSize = 0;
            Boolean scaleByHeight = Math.abs(options.outHeight - targetHeight) >= Math.abs(options.outWidth
                    - targetWidth);
            if (options.outHeight * options.outWidth * 2 >= 1638) {
                sampleSize = scaleByHeight ? options.outHeight / targetHeight : options.outWidth / targetWidth;
                sampleSize = (int) Math.pow(2d, Math.floor(Math.log(sampleSize) / Math.log(2d)));
            }
            options.inJustDecodeBounds = false;
            options.inTempStorage = new byte[128];
            while (true) {
                try {
                    options.inSampleSize = (int) sampleSize;
                    bitMapImage = BitmapFactory.decodeFile(filePath, options);
                    break;
                } catch (Exception ex) {
                    try {
                        sampleSize = sampleSize * 2;
                    } catch (Exception ex1) {

                    }
                }
            }
        } catch (Exception ex) {

        }
        return bitMapImage;
    }

    public static Bitmap compressBitMapImage2() {
        Bitmap bitmapImage = BitmapFactory.decodeFile("Your path");
        int nh = (int) (bitmapImage.getHeight() * (512.0 / bitmapImage.getWidth()));
        Bitmap scaled = Bitmap.createScaledBitmap(bitmapImage, 512, nh, true);
        return scaled;
    }

    public String compressBitMapImage3(String imageUri) {

       // String filePath = getRealPathFromURI(imageUri);
        String filePath = imageUri;
        Bitmap scaledBitmap = null;

        BitmapFactory.Options options = new BitmapFactory.Options();

//      by setting this field as true, the actual bitmap pixels are not loaded in the memory. Just the bounds are loaded. If
//      you try the use the bitmap here, you will get null.
        options.inJustDecodeBounds = true;
        Bitmap bmp = BitmapFactory.decodeFile(filePath, options);

        int actualHeight = options.outHeight;
        int actualWidth = options.outWidth;

//      max Height and width values of the compressed image is taken as 816x612

        float maxHeight = 816.0f;
        float maxWidth = 612.0f;
        float imgRatio = actualWidth / actualHeight;
        float maxRatio = maxWidth / maxHeight;

//      width and height values are set maintaining the aspect ratio of the image

        if (actualHeight > maxHeight || actualWidth > maxWidth) {
            if (imgRatio < maxRatio) {
                imgRatio = maxHeight / actualHeight;
                actualWidth = (int) (imgRatio * actualWidth);
                actualHeight = (int) maxHeight;
            } else if (imgRatio > maxRatio) {
                imgRatio = maxWidth / actualWidth;
                actualHeight = (int) (imgRatio * actualHeight);
                actualWidth = (int) maxWidth;
            } else {
                actualHeight = (int) maxHeight;
                actualWidth = (int) maxWidth;

            }
        }

//      setting inSampleSize value allows to load a scaled down version of the original image

        options.inSampleSize = calculateInSampleSize(options, actualWidth, actualHeight);

//      inJustDecodeBounds set to false to load the actual bitmap
        options.inJustDecodeBounds = false;

//      this options allow android to claim the bitmap memory if it runs low on memory
        options.inPurgeable = true;
        options.inInputShareable = true;
        options.inTempStorage = new byte[16 * 1024];

        try {
//          load the bitmap from its path
            bmp = BitmapFactory.decodeFile(filePath, options);
        } catch (OutOfMemoryError exception) {
            exception.printStackTrace();

        }
        try {
            scaledBitmap = Bitmap.createBitmap(actualWidth, actualHeight, Bitmap.Config.ARGB_8888);
        } catch (OutOfMemoryError exception) {
            exception.printStackTrace();
        }

        float ratioX = actualWidth / (float) options.outWidth;
        float ratioY = actualHeight / (float) options.outHeight;
        float middleX = actualWidth / 2.0f;
        float middleY = actualHeight / 2.0f;

        Matrix scaleMatrix = new Matrix();
        scaleMatrix.setScale(ratioX, ratioY, middleX, middleY);

        Canvas canvas = new Canvas(scaledBitmap);
        canvas.setMatrix(scaleMatrix);
        canvas.drawBitmap(bmp, middleX - bmp.getWidth() / 2, middleY - bmp.getHeight() / 2, new Paint(Paint.FILTER_BITMAP_FLAG));

//      check the rotation of the image and display it properly
        ExifInterface exif;
        try {
            exif = new ExifInterface(filePath);

            int orientation = exif.getAttributeInt(
                    ExifInterface.TAG_ORIENTATION, 0);
            Log.d("EXIF", "Exif: " + orientation);
            Matrix matrix = new Matrix();
            if (orientation == 6) {
                matrix.postRotate(90);
                Log.d("EXIF", "Exif: " + orientation);
            } else if (orientation == 3) {
                matrix.postRotate(180);
                Log.d("EXIF", "Exif: " + orientation);
            } else if (orientation == 8) {
                matrix.postRotate(270);
                Log.d("EXIF", "Exif: " + orientation);
            }
            scaledBitmap = Bitmap.createBitmap(scaledBitmap, 0, 0,
                    scaledBitmap.getWidth(), scaledBitmap.getHeight(), matrix,
                    true);
        } catch (IOException e) {
            e.printStackTrace();
        }

        FileOutputStream out = null;
        String filename = getFilename();
        try {
            out = new FileOutputStream(filename);

//          write the compressed bitmap at the destination specified by filename.
            scaledBitmap.compress(Bitmap.CompressFormat.JPEG, 80, out);

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        return filename;

    }

    public String getFilename() {
        File file = new File(Environment.getExternalStorageDirectory().getPath(), "MyFolder/Images");
        if (!file.exists()) {
            file.mkdirs();
        }
        String uriSting = (file.getAbsolutePath() + "/" + System.currentTimeMillis() + ".jpg");
        return uriSting;

    }

    public int calculateInSampleSize(BitmapFactory.Options options, int reqWidth, int reqHeight) {
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {
            final int heightRatio = Math.round((float) height / (float) reqHeight);
            final int widthRatio = Math.round((float) width / (float) reqWidth);
            inSampleSize = heightRatio < widthRatio ? heightRatio : widthRatio;
        }
        final float totalPixels = width * height;
        final float totalReqPixelsCap = reqWidth * reqHeight * 2;
        while (totalPixels / (inSampleSize * inSampleSize) > totalReqPixelsCap) {
            inSampleSize++;
        }

        return inSampleSize;
    }


}