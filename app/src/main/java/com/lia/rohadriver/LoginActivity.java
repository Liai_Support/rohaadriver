package com.lia.rohadriver;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ScrollView;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.material.snackbar.BaseTransientBottomBar;
import com.google.android.material.snackbar.Snackbar;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener {
    EditText mobileno,password;
    private Session session;
    TextView forget;
    Button signin;
    ProgressDialog progressDialog;
    View view;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        view = (ScrollView) findViewById(R.id.scroll);
        session = new Session(this,this,view);
        if (!session.isNetworkEnabled() == true){
            session.noNetworkDialog(this);
        }
        if (!session.gettoken().equals("") && session.isNetworkEnabled() == true){
            if(session.isVersion() == true){
                session.versiondialog();
            }
            else {
                finish();
                Intent intent = new Intent(LoginActivity.this, MapActivity.class);
                startActivity(intent);
            }
        }
        mobileno = (EditText)findViewById(R.id.edit_mobileno);
        password = (EditText)findViewById(R.id.edit_password);
        signin = (Button)findViewById(R.id.btn_signin);
        signin.setOnClickListener(this);
        forget = (TextView) findViewById(R.id.txt_forget);
        forget.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        if (!session.isNetworkEnabled() == true){
            session.noNetworkDialog(this);
        }
        else {
            if (view == signin) {
                String phonestr = String.valueOf(mobileno.getText().toString());
                String passstr = String.valueOf(password.getText().toString());
                if (mobileno.getText().toString().trim().length() == 0 || password.getText().toString().trim().length() == 0) {
                    mobileno.setError(getString(R.string.enteryourmailid));
                    password.setError(getString(R.string.enterpassword));
                    //Snackbar.make(view, "Please Enter Username and Password", Snackbar.LENGTH_SHORT).show();
                    snackbarToast(getString(R.string.pleaseentermailidandpassword));
                } else {
                    JSONObject requestbody = new JSONObject();
                    try {
                        requestbody.put("email", phonestr);
                        requestbody.put("password", passstr);
                        loginrequestJSON(requestbody);
                        progressDialog = new ProgressDialog(LoginActivity.this);
                        progressDialog.setTitle("");
                        progressDialog.setMessage(getString(R.string.pleasewait));
                        progressDialog.setCancelable(false);
                        progressDialog.show();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            } else if (view == forget) {
                Intent intent = new Intent(LoginActivity.this, ForgetPasswordActivity.class);
                startActivity(intent);
            }
        }
    }

    private void loginrequestJSON(JSONObject response) {
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        final String requestBody = response.toString();
        Log.d("request234",String.valueOf(response));
        StringRequest stringRequest = new StringRequest(Request.Method.POST, StaticInfo.driverlogincopy , new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d("loginresponse", ">>" + response);
                try{
                    progressDialog.dismiss();
                    JSONObject obj = new JSONObject(response);
                    if(obj.getBoolean("error") == true){
                        String msg = obj.getString("message");
                        snackbarToast(""+msg);
                        Log.d("loginerror",""+msg);
                    }
                    else {
                        String msg = obj.getString("data");
                        JSONObject tok = new JSONObject(msg);
                        String token = tok.getString("access_token");
                        Log.d("token",""+token);
                        if(tok.getString("status").equalsIgnoreCase("active")) {
                            session.settoken(token);
                            session.setUserId(tok.getInt("user_id"));
                            session.setusername(tok.getString("user_name"));
                            session.setuserphoneno(tok.getString("user_mobile"));
                            session.setusermail(tok.getString("user_mail"));
                            snackbarToast(getString(R.string.loginsuccess));
                            Intent intent = new Intent(LoginActivity.this, MapActivity.class);
                            startActivity(intent);
                            finish();
                        }
                        else {
                            snackbarToast(getString(R.string.youaredisablefromadminpleasecontactadmin));
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.dismiss();
                Log.e("VOLLEY", error.toString());
            }
        }){
            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }

            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return requestBody == null ? null : requestBody.getBytes("utf-8");
                } catch (UnsupportedEncodingException uee) {
                    VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                    return null;
                }
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(10000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        Log.d("asdsdjkxdk",""+stringRequest);
        requestQueue.add(stringRequest);
    }

    private void snackbarToast(String msg){
        Snackbar snackbar ;
        View view = findViewById(R.id.btn_signin);
        Snackbar.make(view, msg, Snackbar.LENGTH_SHORT)
                .setTextColor(ContextCompat.getColor(LoginActivity.this,R.color.design_default_color_error))
                .setBackgroundTint(ContextCompat.getColor(LoginActivity.this,R.color.white))
                .setDuration(BaseTransientBottomBar.LENGTH_LONG)
                .setAnimationMode(BaseTransientBottomBar.ANIMATION_MODE_SLIDE)
                .show();
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(LoginActivity.this, LanguageActivity.class);
        startActivity(intent);
    }
}