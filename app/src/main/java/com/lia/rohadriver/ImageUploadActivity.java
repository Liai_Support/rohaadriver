package com.lia.rohadriver;

import static android.content.ContentValues.TAG;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.content.FileProvider;
import androidx.lifecycle.Observer;

import android.Manifest;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Parcelable;
import android.provider.MediaStore;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.material.button.MaterialButton;

import com.lia.rohadriver.aws.S3Uploader;
import com.lia.rohadriver.aws.S3Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class ImageUploadActivity extends AppCompatActivity  implements View.OnClickListener {

    ImageView img1,img2,img3,img4,del1,del2,del3,del4;
    MaterialButton completedbtn;
    View view;


    private static final int REQUEST_PERMISSIONS = 100;
    static final int REQUEST_IMAGE_CAPTURE = 1;

    String imagedata1,imagedata2,imagedata3,imagedata4;
    public String currentPhotoPath = "";
    private Bitmap mImageBitmap,mImageBitmap2,mImageBitmap3,mImageBitmap4;
    int click = 0;

    public Session session;
    String orderId = "0";
    boolean completed = false;


    S3Uploader s3uploaderObj;
    String urlFromS3 = null;
    AlertDialog.Builder builder;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_image_upload);
        s3uploaderObj = new S3Uploader(ImageUploadActivity.this);
        img1 =findViewById(R.id.img1);
        img1 =findViewById(R.id.img1);
        img2 =findViewById(R.id.img2);
        img3 =findViewById(R.id.img3);
        img4 =findViewById(R.id.img4);
        del1 =findViewById(R.id.delete1);
        del2 =findViewById(R.id.delete2);
        del3 =findViewById(R.id.delete3);
        del4 =findViewById(R.id.delete4);
        completedbtn =findViewById(R.id.complete_btn);
        img1.setOnClickListener(this);
        img2.setOnClickListener(this);
        img3.setOnClickListener(this);
        img4.setOnClickListener(this);
        del1.setOnClickListener(this);
        del2.setOnClickListener(this);
        del3.setOnClickListener(this);
        del4.setOnClickListener(this);
        builder = new AlertDialog.Builder(this);
        completedbtn.setOnClickListener(this);
        orderId = getIntent().getStringExtra("orderId");
        Log.d(TAG, "onCreate: "+orderId);
        session = new Session(this, this,img1);
        if (!session.isNetworkEnabled()) {
            session.noNetworkDialog(this);
        } else {
            if (!session.isPermissionsEnabled()) {
                session.permissionsEnableRequest();
            } else if (session.isVersion()) {
                session.versiondialog();
            }
        }

        if ((ContextCompat.checkSelfPermission(getApplicationContext(),
                Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) && (ContextCompat.checkSelfPermission(getApplicationContext(),
                Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED)) {
            if ((ActivityCompat.shouldShowRequestPermissionRationale(ImageUploadActivity.this,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE)) && (ActivityCompat.shouldShowRequestPermissionRationale(ImageUploadActivity.this,
                    Manifest.permission.READ_EXTERNAL_STORAGE))) {

            } else {
                ActivityCompat.requestPermissions(ImageUploadActivity.this,
                        new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE},
                        REQUEST_PERMISSIONS);
            }
        } else {
            Log.e("camera", "granted");
        }

    }



    private void dispatchTakePictureIntent() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        // Ensure that there's a camera activity to handle the intent
        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
            // Create the File where the photo should go
            File photoFile = null;
            try {
                photoFile = createImageFile();
            } catch (IOException ex) {
                // Error occurred while creating the File
            }
            // Continue only if the File was successfully created
            if (photoFile != null) {
                Uri photoURI = FileProvider.getUriForFile(this,
                        "com.lia.rohadriver.android.fileprovider",
                        photoFile);
                Log.d(TAG, "dispatchTakePictureIntent: "+photoFile);
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
//                takePictureIntent.putExtra("imgId", data);

            }
            startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);

        }
    }
    public static void addImageToGallery(final String filePath, final Context context) {

        ContentValues values = new ContentValues();

        values.put(MediaStore.Images.Media.DATE_TAKEN, System.currentTimeMillis());
        values.put(MediaStore.Images.Media.MIME_TYPE, "image/jpeg");
        values.put(MediaStore.MediaColumns.DATA, filePath);

        context.getContentResolver().insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.d(TAG, "onActivityResult: "+requestCode +click);
        if(currentPhotoPath.isEmpty()){
            currentPhotoPath = ImagePicker.getImagePath(this,resultCode,data);
            addImageToGallery(currentPhotoPath,this);

        }else if (data != null){
            currentPhotoPath = ImagePicker.getImagePath(this,resultCode,data);
            addImageToGallery(currentPhotoPath,this);

        }
        Log.d(TAG, "onActivityResult: path "+currentPhotoPath);
        if (requestCode == REQUEST_IMAGE_CAPTURE || requestCode == 6 && resultCode == RESULT_OK) {
            try {
            if (click ==1){
                if (data!=null) {
                    mImageBitmap = ImagePicker.getImageFromResult(this, resultCode, data);
                }else{
                    mImageBitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), Uri.fromFile(new File(currentPhotoPath)));
                }
                imagedata1 = currentPhotoPath;
                img1.setImageBitmap(mImageBitmap);
                btnEnable();
            }else if (click ==2) {
                if (data!=null) {
                    mImageBitmap2 = ImagePicker.getImageFromResult(this, resultCode, data);
                }else {
                     mImageBitmap2 = MediaStore.Images.Media.getBitmap(this.getContentResolver(), Uri.fromFile(new File(currentPhotoPath)));

                }
                imagedata2 = currentPhotoPath;
                img2.setImageBitmap(mImageBitmap2);
                btnEnable();
            }else if (click ==3) {
                if (data!=null) {
                    mImageBitmap3 = ImagePicker.getImageFromResult(this, resultCode, data);
                }else {
                    mImageBitmap3 = MediaStore.Images.Media.getBitmap(this.getContentResolver(), Uri.fromFile(new File(currentPhotoPath)));
                }
                imagedata3 = currentPhotoPath;
                img3.setImageBitmap(mImageBitmap3);
                btnEnable();
            }else if (click ==4) {
                if (data!=null) {
                    mImageBitmap4 = ImagePicker.getImageFromResult(this, resultCode, data);
                }else{
                    mImageBitmap4 = MediaStore.Images.Media.getBitmap(this.getContentResolver(), Uri.fromFile(new File(currentPhotoPath)));

                }
             imagedata4 = currentPhotoPath;
                img4.setImageBitmap(mImageBitmap4);
                btnEnable();
            }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }



    }

    private void uploadImageTos3(String currentPhotoPath ,int curentUpload) {
        final String path = currentPhotoPath;
        if (path != null) {
            s3uploaderObj.initUpload(path);
            s3uploaderObj.setOns3UploadDone(new S3Uploader.S3UploadInterface() {
                @Override
                public void onUploadSuccess(String response) {
                    if (response.equalsIgnoreCase("Success")) {
                        urlFromS3 = S3Utils.generates3ShareUrl(getApplicationContext(), path);
                        if(!TextUtils.isEmpty(urlFromS3)) {
                            Toast.makeText(ImageUploadActivity.this, "Uploaded Successfully!!", Toast.LENGTH_SHORT).show();

                        if (curentUpload == 4){
                            JSONArray imagearray = new JSONArray();
                            JSONObject imageobject = new JSONObject();
                            try {
                                imageobject.put("image1",""+imagedata1.substring(imagedata1.lastIndexOf("/")+1));
                                imageobject.put("image2",""+imagedata2.substring(imagedata2.lastIndexOf("/")+1));
                                imageobject.put("image3",""+imagedata3.substring(imagedata3.lastIndexOf("/")+1));
                                imageobject.put("image4",""+imagedata4.substring(imagedata4.lastIndexOf("/")+1));
                                imagearray.put(imageobject);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                            JSONObject requestbody = new JSONObject();
                            try {
                                requestbody.put("orderid", ""+orderId);
                                requestbody.put("driverid", ""+session.getUserId());
                                requestbody.put("images", imagearray);

                                    orderstatusrequestJSON(requestbody);


                                session.progressdialogshow();
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }
//                            session.progressdialogshow();


                        }

                    }
                }

                @Override
                public void onUploadError(String response) {
                    Log.e(TAG, "Error Uploading"+response);

                }
            });
        }else{
            Toast.makeText(this, "Null Path", Toast.LENGTH_SHORT).show();
        }
    }





    private void openPickerDialog() {

       JSONObject jsonObject = ImagePicker.getPickImageIntent(ImageUploadActivity.this);
        Intent chooseImageIntent = null;
        try {
            chooseImageIntent = (Intent) jsonObject.get("intent");
            if (!jsonObject.getString("path").isEmpty()){
               currentPhotoPath =  jsonObject.getString("path");
                Log.d(TAG, "openPickerDialog: "+currentPhotoPath);
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
        startActivityForResult(chooseImageIntent, 6);
    }

    public  File createImageFile() throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        Log.d(TAG, "createImageFile: "+storageDir);
        File image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );

        // Save a file: path for use with ACTION_VIEW intents
        currentPhotoPath = image.getAbsolutePath();
        return image;
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()){
            case R.id.img1:
                click=1;
                if (session.getGalaccess().equals("A") ){
                    openPickerDialog();
                }else{
                    dispatchTakePictureIntent();
                }

                break;
            case R.id.img2:
                click=2;
                if (session.getGalaccess().equals("A")){
                    openPickerDialog();
                }else{
                    dispatchTakePictureIntent();
                }
                break;
            case R.id.img3:
                click = 3;
                if (session.getGalaccess().equals("A")){
                    openPickerDialog();
                }else{
                    dispatchTakePictureIntent();
                }
                break;
            case R.id.img4:
                click = 4;
                if (session.getGalaccess().equals("A")){
                    openPickerDialog();
                }else{
                    dispatchTakePictureIntent();
                }
                break;
            case R.id.complete_btn:

            session.progressdialogshow();
                for (int i=1;i<=5;i++){

                    Log.d(TAG, "onClick: "+i);
                    if (i==1){
                        uploadImageTos3(imagedata1,1);
                    }else if (i == 2){
                        uploadImageTos3(imagedata2,2);

                    }else if (i == 3){
                        uploadImageTos3(imagedata3,3);

                    }else if (i == 4){
                        uploadImageTos3(imagedata4,4);


                    }else if (i == 5){

                    }


                }


                break;
            case R.id.delete1:
                imagedata1 ="";
                img1.setImageDrawable(getDrawable(R.drawable.defaultimg));
                completedbtn.setEnabled(false);
                break;
            case R.id.delete2:
                imagedata2 ="";
                img2.setImageDrawable(getDrawable(R.drawable.defaultimg));
                completedbtn.setEnabled(false);

                break;
            case R.id.delete3:
                imagedata3 ="";
                img3.setImageDrawable(getDrawable(R.drawable.defaultimg));
                completedbtn.setEnabled(false);

                break;
            case R.id.delete4:
                imagedata4 ="";
                img4.setImageDrawable(getDrawable(R.drawable.defaultimg));
                completedbtn.setEnabled(false);

                break;
        }

    }
    private void btnEnable(){
        if (mImageBitmap != null &&mImageBitmap2 != null&&mImageBitmap3 != null && mImageBitmap4 != null){

            completedbtn.setEnabled(true);
        }
    }


    private void orderstatusrequestJSON(JSONObject response) {
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        final String requestBody = response.toString();
        Log.d("request234",requestBody);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, StaticInfo.uploadimage, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                session.progressdialogdismiss();
                Log.d("assignedorderresponsesd", ">>" + response);
                try{
                    JSONObject obj = new JSONObject(response);
                    if(obj.getBoolean("error") == false){
                        completed =true;
                        builder.setMessage(R.string.ordercompleted) .setTitle(R.string.orderstatus);
                            builder.setCancelable(false)
                                .setPositiveButton(getString(R.string.yes), new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        Log.d("assignedorderresponse", "jhk");
                                        Intent intent1 = new Intent(ImageUploadActivity.this, DashboardActivity.class);
                                        startActivity(intent1);
                                        finish();

                                    }
                                });
                        AlertDialog alert = builder.create();
                        alert.show();
                    }
                    else {
                        Log.d("assignedorderresponse", "hjk");
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
               session.volleyerror(error);
             session.progressdialogdismiss();
                Log.e("VOLLEY", error.toString());
            }
        }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                String token = session.gettoken();
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type","application/json" );
                params.put("Authorization", "Bearer "+token);
                Log.d("param",""+params);
                return params;
            }

            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }

            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return requestBody == null ? null : requestBody.getBytes("utf-8");
                } catch (UnsupportedEncodingException uee) {
                    VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                    return null;
                }
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(100000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        Log.d("asdsdjkxdk",""+stringRequest);
        requestQueue.add(stringRequest);

    }
    @Override
    public void onDestroy(){
        super.onDestroy();

            session.progressdialogdismiss();

    }





}